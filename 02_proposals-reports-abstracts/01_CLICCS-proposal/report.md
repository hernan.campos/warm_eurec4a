###### Experiments performed successfully at project account bm1183: 
We successfully performed several tests for different climate change forcings. These tests were crucial in developing a suitable setup for a longer global warming scenario simulation. 
###### Scientific results of project bm1183: 
Realistic high resolution regional LES with idealised climate change forcings are a new approach to investigate feedbacks. We explored the partially unforeseen interdependencies of variables in the boundary conditions and constructed a consistent climate change forcing signal.
###### Publications in 2023 that use data of project bm1183: 
Campos, R.H., Kluft, L., Naumann, A.K., Stevens, B. (2023). Constraining Shallow Cumulus Cloud Feedback Using Large-Domain LES [Poster Presentation] 2023 CFMIP/GASS, Paris, France, https://sites.google.com/view/cfmip2023/abstracts
###### Data Management of project bm1183: 
We use no data storage from the project. Data from the test runs might be saved in highly compressed from.