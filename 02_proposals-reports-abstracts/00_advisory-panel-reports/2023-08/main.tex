\documentclass{article}

\usepackage[utf8]{inputenc}
\usepackage{comment} % multi-line comment
\usepackage{pdfpages} % include a pdf

\usepackage[]{todonotes} 
% \usepackage[disable]{todonotes} 
\usepackage[normalem]{ulem} % strike through text via \sout{}
\usepackage{hyperref}

\usepackage{geometry} % less bordering white space
    % normal:
    \geometry{a4paper, top=25mm, left=20mm, right=20mm, bottom=40mm, headsep=10mm, footskip=20mm}
\usepackage{pgfgantt} % time table aka. Gantt Chart

% for rotated gant chart
% \usepackage{pdflscape} % this does also rotate the pdf page back to make it more readable
\usepackage{lscape} % this does not
\usepackage{rotating}

\usepackage{csquotes} % \enquote{}
\usepackage{caption} % subfigures
    \usepackage{subcaption} 
\newcommand{\capwidth}{0.9\columnwidth} % used for setting the width of captions
\usepackage{enumitem,amssymb} % for todo lists
% https://tex.stackexchange.com/questions/247681/how-to-create-checkbox-todo-list
\newlist{todolist}{itemize}{2}
\setlist[todolist]{label=$\square$}
\usepackage[ % Für Benutzung von Citation-kürzeln wie natbib
    authordate,
    bibencoding=auto,
    strict,
    backend=biber,
    maxcitenames=2,
    natbib
    ]{biblatex-chicago}
\addbibresource{../bib.bib}
% make colored hyperref links for citations
\usepackage{hyperref}
\usepackage{xcolor}
\hypersetup{
    colorlinks,
    linkcolor={black!50!black},
    citecolor={blue!50!black},
    urlcolor={blue!80!black}
}
% keine schusterjungen, plz :sob:
% https://tex.stackexchange.com/questions/4152/how-do-i-prevent-widow-orphan-lines
\widowpenalty10000
\clubpenalty10000



\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}


\usepackage{placeins} % for \FloatBarrier

\title{4. Advisory Panel Meeting on 28.08.2023 \\ Status Report and Outline}
\begin{document}



\vspace*{3cm}
\section*{4. Advisory Panel Meeting on 28.08.2023 \\ Status Report and Outline}
\vfill{}
\begin{tabular}{ l l }
    PhD topic : & \textit{Tropical low cloud feedbacks in global storm resolving models} \\
    PhD student : & Hernan Campos \\
    Panel Members : & Bjorn Stevens, Ann-Kristin Naumann, Peter Korn \\
\end{tabular}



\newpage
\section*{Thesis synopsis}
An accurate estimate of the impact of climate change depends on an accurate estimate of climate sensitivity. Cloud feedback is an important factor for climate sensitivity. Poor understanding of the behaviour of clouds in a warming world is a major contribution to the total uncertainty in climate sensitivity \citep{bretherton2015insights,ipcc2021}. 

The difficulty in assessing cloud feedback has its basis in the complex nature of clouds. Both small scale processes as turbulence, and large scale processes as self organisation play an important role for clouds, their behaviour and their response to forcing. But investigating both small and large scales in the same simulation is difficult, because computational resources put a limit on the model setup. This results in studies focusing either on fine resolutions on small domains, or coarse resolutions on large domains. Especially shallow cumulus with average individual sizes of less than 1000\,m are difficult to grasp in models with resolutions of several kilometers.

%The new generation of global \textit{storm-resolving} simulations is under constant development and -- with the aid of new generation computing infrastructure -- may close this gap. 

This project is aimed at improving the quantification of cloud feedback. Our starting point is the Large Eddy Simulation (LES) setup that was designed to accompany the EUREC4A field campaign in the South-Western Atlantic trade wind region. We will introduce different global warming signals to the experiment via its boundary conditions, ranging from simple conceptual \citep{vallis2015response} to complex general circulation model (GCM) derived perturbations \citep{kroner2017separating, li2019high}. The complex perturbation represents the most realistic case, yielding the most accurate quantitative estimate of cloud feedback. The conceptual forcings aim to disentangle the effect of global warming into their dynamical and thermodynamical effect as two different pathways on which clouds are affected by climate change \citep{bony2004dynamic}.


\newpage
\section*{Status Report}

At the time of my last Panel report I had to deal with numerous technical issues that slowed down my progress. The reproducibility issues have been resolved. While there still is no decision on a final setup for the production run, I have explored the be behaviour of the warming run and designed a second run that ensures mass conservation. 

The computing resources we applied for within the CLICCS project are already used up. We applied for 12,000 node hours. This number was a faulty estimate based on the conducted test runs. We will apply for new resources in the upcoming CLICCS proposal.

I participated in the CFMIP/GASS meeting in July 2023. I presented a poster and talked to a lot of people working on topics related to the EUREC4A project, both with measurements and with simulations. This community showed a lot of interest in my work. A group of researchers working on simulations of the EUREC4A time and domain (like myself) organise in a \href{https://eurec4a.eu/set-up-large-eddy-simulations}{EUREC4A-MIP project}. Most of the people working on this project are based in Delft. Bjorn suggested I could visit the Delft group to benefit from their know-how for my work on the simulation setup or a specific analysis.


\subsection*{The warming signal}

The basic idea of this experiment is to look at the effect of warming as a simple increase in temperature, without the dynamical effects. This temperature increase should be sufficiently strong to cause a change in the model behaviour. A common convention is to use 4 kelvin surface warming which roughly reflects equilibrium climate sensitivity (ca. 2-5 kelvin according to \citep{ipcc2021}). The open boundaries our model force us to think about the translation of this uniform surface warming into a warming of the air column. With warmer surface temperatures the lapse rate will also change, leading to more warming aloft (warming is roughly double in 10 km height compared to the surface). To construct our perturbation of the temperature profile ($\Delta{}T_h$) we calculate the moist adiabatic profiles over two surface temperatures with the set difference of 4 degree kelvin. The difference between these two profiles ($\Delta{}T_h$) is added onto the air temperature in the existing boundary conditions (Fig.~\ref{fig:delta}). 

Where the amount of water vapour is controlled by the Clausius-Clapeyron relationship, the increasing temperature leads to an increase in specific humidity, keeping relative humidity roughly constant \citep{held2006robust}. Because we constantly force our model over the entire run time we should try to anticipate how it tends to equilibrate to reduce spin up effects on the boundaries. Thus we increase specific humidity in our boundaries to keep the relative humidity fixed (Fig.~\ref{fig:latbc_variability}). 



\begin{figure}[htb]
 \centering
 \captionsetup{format=plain, width=\capwidth}
 \begin{subfigure}[b]{0.24\textwidth}
     \centering
     \includegraphics[width=\textwidth]{2023-08/img/fig_delta_from_moist_adiabats.png}
     \caption{}
     \label{fig:delta}
 \end{subfigure}
 \hfill
 \begin{subfigure}[b]{0.7\textwidth}
     \centering
     \includegraphics[width=\textwidth]{2023-08/img/fig_boundary_conditions.png}
     \caption{}
     \label{fig:latbc_variability}
 \end{subfigure}
    \caption{\small \textbf{a}: Construction of the $\Delta$ (right) as difference between two moist adiabats with 4 kelvin difference in surface temperature (left); \textbf{b}: mean and variability over time and space (shading = interquartile range) for lateral boundary conditions in the control (\textcolor{blue}{blue}) and warming run (\textcolor{red}{red}). }
    \label{fig:warming_forcing}
\end{figure}



\FloatBarrier
\subsection*{Pressure and mass loss}


ICON reads the boundary conditions in their diagnostic form, as pressure and temperature (\texttt{pres} and \texttt{temp}). Internally ICON works on the prognostic variables density and virtual potential temperature (\texttt{rho} and \texttt{theta\_v}). After passing the boundary conditions to ICON, it will calculate the prognostic variables on the basis of temperature (increased), specific humidity (increased) and pressure (unchanged).

\begin{equation}
\rho = \frac{\epsilon p\,(1+w)}{R_dT\,(w+\epsilon)}
\end{equation}

with $R_d$ being the specific gas constant of dry air, $\epsilon$ the molecular weight ratio between water vapor and dry air ($\epsilon\approx0.622$) and $w$ the mixing ratio ($w = q (1-q)^{-1}$).

\begin{equation}
\theta_v = T (P_0 / P)^\kappa \frac{{w} + \epsilon}{\epsilon\,(1 + {w})}
\end{equation}

with $\kappa$ being the Poisson constant and $P_0$ a reference pressure.



This way the increase in temperature leads to a reduction in air density, unless it is compensated by a decrease in pressure. In our first experiment (named \textbf{p\_fix} hereafter) we do nothing with the pressure in the boundary conditions, thus implicitly we decrease the air density. This lead to an unforeseen drop in surface pressure. Not changing pressure at the boundaries decreases pressure inside the domain. This seems counter intuitive at first glance. But as mentioned above ICON only uses the pressure information to calculate density. Providing less dense air at the boundaries leads to mass loss inside the domain and the reduced air mass leads to reduced surface pressure (surface pressure is a measure for the mass in the air column).

To inhibit mass loss we tried to fix the density in our second experiment (named \textbf{rho\_fix} hereafter). We save the information on air density, then manipulate the boundary conditions (increase temperature and specific humidity) and afterwards restore the density by decreasing pressure following the relationship given by the hydrostatic equation:

$$ p = \rho \cdot R_{\text{specific}} \cdot T $$

This indeed does prevent the drop in surface pressure, but has its own side effects. When we decrease pressure to compensate for the increase in temperature, we produce a a pressure change that (inversely) follows our applied temperature delta with the most change at around 10 km height and no change in higher layers. The adjustment to the this change results in vigorous circulation and strong winds during spinup (Fig.~\ref{fig:winds}).



\begin{figure}[htb]
  \centering
  \captionsetup{format=plain, width=\capwidth}
  \includegraphics[width=0.95\textwidth]{2023-08/img/fig_domain_means_winds(1).png}
  \caption{\small Spatial mean and variability (shading; interquartile range) for pressure and winds as a time series (hours after startup on x-axis) in the control (\textcolor{blue}{blue}) run, the \textit{fixed pressure} warming run (\textcolor{orange}{orange}) and the fixed density warming run (\textcolor{red}{red}).}
  \label{fig:winds}
\end{figure}



\clearpage
\FloatBarrier
\subsection*{Outrageous surface fluxes}

Surface fluxes are a result the gradient at the surface, the airs capacity, turbulence (and thus wind), and a coefficient that can be dependent on temperature, pressure or salinity. A typical parametrisation can look like this:

\begin{equation}
Q_{LH} = \rho \cdot L_e \cdot c_e \cdot W \cdot (q_s - q_a)
\end{equation}

\begin{equation}
Q_{SH} = \rho \cdot c_p \cdot c_h \cdot W \cdot (T_s - T_a)
\end{equation}

where $L_e$ is the latent heat of vaporisation (a function of sea surface temperature), $c_p$ is the specific heat capacity of air, $c$ are turbulent exchange coefficients \citep{yu2009sea}.
% Where the amount of water vapour is controlled by the Clausius-Clapeyron relationship, the increasing temperature leads to an increase in specific humidity. A purely thermodynamic scaling based on a saturated troposphere gives a rate of increase in specific humidity of around 6 to 15 percent per degree kelvin (depending on the temperature with stronger increase for lower base temperatures; $\approx$6 for 300 K; \cite{ogorman2010closely}). 

The increase in temperature that we apply to our model should lead to higher surface fluxes. But the fluxes we see in the model are much higher than anticipated (latent heat flux almost doubles). While relative humidity seems to have opposing trends (higher than control for \texttt{p\_fix}, lower than control for \texttt{rho\_fix}), their surface fluxes are suprisingly close to each other (Fig.~\ref{fig:fluxes}).

\begin{figure}[htb]
  \centering
  \captionsetup{format=plain, width=\capwidth}
  \includegraphics[width=0.95\textwidth]{2023-08/img/fig_domain_means_fluxes(2).png}
  \caption{\small Spatial mean and variability (shading; interquartile range) for surface fluxes and near surface relative humidity as a time series (hours after startup on x-axis) in the control (\textcolor{blue}{blue}) run, the \textit{fixed pressure} warming run (\textcolor{orange}{orange}) and the fixed density warming run (\textcolor{red}{red}).}
  \label{fig:fluxes}
\end{figure}



Comparison to another set of simulations with a similar forcing (4 kelvin surface warming) reveals how extreme the change in latent heat flux is (Fig.~\ref{fig:heatfluxhistogram}). Although the aquaplanet simulation (Fig.~\ref{fig:latentheat_aquaplanet}) is global and thus should represent more variability, the range of encountered values for surface latent heat fluxes shifts much less than it does in the EUREC4A-LES simulations (Fig.~\ref{fig:latentheat_les}).



\begin{figure}[htb]
 \centering
 \captionsetup{format=plain, width=\capwidth}
 \begin{subfigure}[b]{0.4\textwidth}
     \centering
     \includegraphics[width=\textwidth]{2023-08/img/fig_hfls_histogram_LES.png}
     \caption{}
     \label{fig:latentheat_les}
 \end{subfigure}
 \hspace{1cm}
 \begin{subfigure}[b]{0.4\textwidth}
     \centering
     \includegraphics[width=\textwidth]{2023-08/img/fig_hfls_histogram_aquaplanet.png}
     \caption{}
     \label{fig:latentheat_aquaplanet}
 \end{subfigure}
    \caption{\small Histograms of latent heat fluxes over space and time in aquaplanet (\textbf{a}) and EUREC4A-LES simulations (\textbf{b}), for warming and control runs. Warming simulations (\textcolor{red}{\textbf{red}}) have a similar design with surface temperatures raised by 4 kelvin.}
    \label{fig:heatfluxhistogram}
\end{figure}




% \subsection*{Vertical motion}


% \begin{figure}[htb]
%  \centering
%  \captionsetup{format=plain, width=\capwidth}
%  \begin{subfigure}[b]{0.48\textwidth}
%      \centering
%      \includegraphics[width=\textwidth]{2023-08/img/fig_profiles_RH_w.png}
%      \caption{}
%      \label{fig:RH_w}
%  \end{subfigure}
%  \hfill
%  \begin{subfigure}[b]{0.48\textwidth}
%      \centering
%      \includegraphics[width=\textwidth]{2023-08/img/fig_profiles_u_v.png}
%      \caption{}
%      \label{fig:u_v}
%  \end{subfigure}
%     \caption{\small Domain mean vertical profiles at 8 hours after startup for relative humidity, vertical velocity (\textbf{a}), meridional and zonal winds (\textbf{b}). The shading is the interquartile range.}
%     \label{fig:profiles}
% \end{figure}







\clearpage
\section*{Outlook}

\subsubsection*{Upcoming Tasks}

\begin{comment}
improve picture
- RH calculation (why is it strange near the surface?)
- how is latent heat flux calculated? why is it so high? (look into source code?)
- redo last part of p\_fix run
\end{comment}

It is more pressing than ever to start the production run and produce results that can form the basis of my analysis. The surface fluxes are much higher than expected, a problem that seems worth exploring before more computing resources are invested into a production run. Thus this problem has to be fixed.

\begin{enumerate}
    \item Investigate unusually high surface fluxes
    \begin{enumerate}
        \item How are fluxes calculated (in ICON)?
        \item Why are they so high?
        \item Why are they similar in both warming runs?
    \end{enumerate}
    \item Think about an alternative approach
    \begin{enumerate}
        \item If the thermodynamic warming does not yields promising results, how do we proceed?
        \item Design another forcing
        \item Move to PGW?
    \end{enumerate}
    \item Look into circulations at different scales (small to meso)
    \begin{enumerate}
        \item Are circulations or circulation scales different under warming?
        \item Does this (partially) explain higher fluxes?
    \end{enumerate}
    \item Connect circulation analysis to cloud radiative effect
    \item Write a manuscript
    \item Postprocess the Data
\end{enumerate}






% \begin{enumerate}
%     \item Apply at the \href{https://sites.google.com/view/cfmip2023/}{CFMIP/GASS conference}
%     \item Prepare a talk for the AES seminar
%     \item Improve confidence in rerun
%     \begin{enumerate}
%         \item Compare diagnotic to non-diagnostic (e.g. \texttt{tqv\_dia} to \texttt{tqv})
%         \item Try effect of no defined \texttt{dt\_conv}
%         \item Try effect of very large \texttt{dt\_conv}
%     \end{enumerate}
%     \item Improve confidence in warming experiment setup
%     % \todo[inline]{Bist du überzeugt, dass der Lauf tut, was wir erwarten? Bjorns "Convince ourselves that the perturbed simulations are reasonable". Wenn ja, wäre es gut aus (oben) genauer auszuführen. Wenn nicht, solltest du es hier als Todo aufnehmen. Oder meinst du das mit (b)?}
%     \begin{enumerate}
%         \item Restart the simulation and produce a longer run
%         % \todo[inline]{Was meinst du hier? Eine längere Simulation im Sinne eines Produktionslaufs? Oder mehr Outputvariablen?}
%         \item Is the observed behaviour just spin-up? (Fig.~\ref{fig:meteogram})
%         % \todo[inline]{Was meinst du hier genau?}
%         \item Why/How is the domain cooling down? 
%     \end{enumerate}
%     \item Move towards a production run for the warming experiment
%     \begin{enumerate}
%         \item Apply CRE ananlysis 
%         \item Decide on desired simulation length
%     \end{enumerate}
%     \item Start writing a draft for publication ({A Paper outline is given below})
%     \item Outline second experiment
% %    \todo[inline]{\textbf{AKN}: Heißt dass du würdest ein PGW Experiment gerne auch schon für dein erstes Paper (3.) auswerten? In dem Paper Outline kommt es nicht vor, oder? \break \break \textbf{HC}:Am liebsten würde ich das schon in mein erstes Paper aufnehmen. ich. habe nur sorge, dass das etwas ambitioniert ist. Auf jeden Fall finde ich es ist Zeit Kontakt mit der Gruppe aufzunehmen und mal anzufragen, ob ich deren Deltas erben kann oder ob ich vielleicht Deltas nach dem gleichen Rezept erstelle.}
%     \begin{enumerate}
%         \item Talk to swiss group about deltas (EUREC4A MIP)
%     \end{enumerate}
    
    
    
    






\clearpage
\begin{landscape}


% \vspace{5mm}
\begin{ganttchart}[
    expand chart=0.97\linewidth,
    % expand chart=0.97\textwidth,
    y unit title=0.5cm,
    y unit chart=0.5cm,
    title/.style={fill=teal, draw=none},
    title label font=\color{white}\bfseries,
    vgrid,
%    inline,
    milestone inline label node/.append style={left=5mm},
    time slot format=isodate-yearmonth,
    time slot unit=month
    ]
    {2021-08}{2025-10}
    \gantttitlecalendar{year, month} \\

    % before start
    \ganttbar[bar/.append style={fill=green!15}]{Work for CIMD}{2021-08}{2021-12} \\
    \ganttmilestone{Start PhD}{2021-08} \ganttnewline
    \ganttbar[bar/.append style={fill=black!50}]{Sick leave}{2022-01}{2022-01} 
    \ganttbar[bar/.append style={fill=black!50}]{}{2023-05}{2023-06} \\
    \ganttbar[bar/.append style={fill=black!50}]{Vacation}{2022-03}{2022-03} 
    \ganttbar[bar/.append style={fill=black!50}]{}{2022-08}{2022-08} 
    \ganttbar[bar/.append style={fill=black!50}]{}{2023-03}{2023-03} 
    \ganttbar[bar/.append style={fill=black!50}]{}{2023-09}{2023-09} \\

    % first year
    \ganttbar[bar/.append style={fill=yellow!50}]{Literature research}{2021-09}{2021-12} 
    \ganttbar[bar/.append style={fill=yellow!50}]{}{2022-02}{2022-02} \\
    \ganttbar[bar/.append style={fill=red!50}]{NextGEMS}{2021-10}{2021-10} \\
    \ganttbar[bar/.append style={fill=red!50}]{Simulation setup}{2022-04}{2022-07} 
    \ganttbar[bar/.append style={fill=red!50}]{}{2022-09}{2022-10} \\
    \ganttbar[bar/.append style={fill=red!50}]{Data analysis}{2022-07}{2022-07}
    \ganttbar[bar/.append style={fill=red!50}]{}{2022-09}{2022-10} \\ 

    % second year
    \ganttbar[bar/.append style={fill=red!50}]{Setup warming experiment}{2022-09}{2023-01} \\
    \ganttbar[bar/.append style={fill=red!50}]{Debug and validate rerun}{2022-11}{2023-04} \\
    \ganttbar[bar/.append style={fill=red!50}]{Debug and validate warming run}{2023-02}{2023-03} 
    \ganttbar[bar/.append style={fill=red!50}]{}{2023-07}{2023-08} 
    \ganttbar[bar/.append style={fill=red!10}]{}{2023-10}{2023-11} \\
    \ganttbar[bar/.append style={fill=red!50}]{CRE and circulation analysis}{2023-10}{2024-01} \\
    \ganttbar[bar/.append style={fill=green!50}]{Paper manuscript}{2024-01}{2024-4} \\
    \ganttbar[bar/.append style={fill=yellow!50}]{Plan PGW experiment}{2024-05}{2024-07} \\
    
    % yearly marker
    \ganttvrule{\rotatebox{270}{start}}{2021-08}
    \ganttvrule{\rotatebox{270}{1 year}}{2022-08}
    \ganttvrule{\rotatebox{270}{2 years}}{2023-08}
    \ganttvrule{\rotatebox{270}{3 years}}{2024-08}
    \ganttvrule{\rotatebox{270}{4 years}}{2025-08}
    % \ganttvrule{}{2021-08}
    % \ganttvrule{}{2022-08}
    % \ganttvrule{}{2023-08}
    % \ganttvrule{}{2024-08}
    
    % panel meetings
    \ganttvrule[vrule/.append style={red,solid}, vrule offset=.5]{\rotatebox{270}{APM-1}}{2021-10}
    \ganttvrule[vrule/.append style={red,solid}, vrule offset=.5]{\rotatebox{270}{APM-2.1}}{2022-04}
    \ganttvrule[vrule/.append style={red,solid}, vrule offset=.5]{\rotatebox{270}{APM-2.2}}{2022-06}
    \ganttvrule[vrule/.append style={red,solid}, vrule offset=.5]{\rotatebox{270}{APM-3.1}}{2023-01}
    \ganttvrule[vrule/.append style={red,solid}, vrule offset=.5]{\rotatebox{270}{APM-3.2}}{2023-03}
    \ganttvrule[vrule/.append style={red,solid}, vrule offset=.5]{\rotatebox{270}{APM-4}}{2023-08}
    \ganttvrule[vrule/.append style={red,solid}, vrule offset=.5]{\rotatebox{270}{APM-5(?)}}{2024-05}
    
\end{ganttchart}
\end{landscape}



\clearpage
\printbibliography


\end{document}
