\documentclass{article}

\usepackage[utf8]{inputenc}
\usepackage{comment} % multi-line comment
\usepackage{pdfpages} % include a pdf

\usepackage[]{todonotes} 
% \usepackage[disable]{todonotes} 
\usepackage[normalem]{ulem} % strike through text via \sout{}
\usepackage{hyperref}

\usepackage{geometry} % less bordering white space
    % normal:
    \geometry{a4paper, top=25mm, left=20mm, right=20mm, bottom=40mm, headsep=10mm, footskip=20mm}
\usepackage{pgfgantt} % time table aka. Gantt Chart

% for rotated gant chart
% \usepackage{pdflscape} % this does also rotate the pdf page back to make it more readable
\usepackage{lscape} % this does not
\usepackage{rotating}

\usepackage{csquotes} % \enquote{}
\usepackage{caption} % subfigures
    \usepackage{subcaption} 
\newcommand{\capwidth}{0.9\columnwidth} % used for setting the width of captions
\usepackage{enumitem,amssymb} % for todo lists
% https://tex.stackexchange.com/questions/247681/how-to-create-checkbox-todo-list
\newlist{todolist}{itemize}{2}
\setlist[todolist]{label=$\square$}
\usepackage[ % Für Benutzung von Citation-kürzeln wie natbib
    authordate,
    bibencoding=auto,
    strict,
    backend=biber,
    maxcitenames=2,
    natbib
    ]{biblatex-chicago}
\addbibresource{../bib.bib}
% make colored hyperref links for citations
\usepackage{hyperref}
\usepackage{xcolor}
\hypersetup{
    colorlinks,
    linkcolor={black!50!black},
    citecolor={blue!50!black},
    urlcolor={blue!80!black}
}
% keine schusterjungen, plz :sob:
% https://tex.stackexchange.com/questions/4152/how-do-i-prevent-widow-orphan-lines
\widowpenalty10000
\clubpenalty10000






\title{3. Advisory Panel Meeting on 20.01.2023 \\ Status Report and Outline}
\begin{document}



\vspace*{3cm}
\section*{3. Advisory Panel Meeting, part II (10.03.2023) \\ Status Report and Outline}
\vfill{}
\begin{tabular}{ l l }
    PhD topic : & \textit{Tropical low cloud feedbacks in global storm resolving models} \\
    PhD student : & Hernan Campos \\
    Panel Members : & Bjorn Stevens, Ann-Kristin Naumann, Peter Korn \\
\end{tabular}



\newpage
\section*{Thesis synopsis}
An accurate estimate of the impact of climate change depends on an accurate estimate of climate sensitivity. Cloud feedback is an important factor for climate sensitivity. Poor understanding of the behaviour of clouds in a warming world is a major contribution to the total uncertainty in climate sensitivity \citep{bretherton2015insights,ipcc2021}. 

The difficulty in assessing cloud feedback has its basis in the complex nature of clouds. Both small scale processes as turbulence, and large scale processes as self organisation play an important role for clouds, their behaviour and their response to forcing. But investigating both small and large scales in the same simulation is difficult, because computational resources put a limit on the model setup. This results in studies focusing either on fine resolutions on small domains, or coarse resolutions on large domains. Especially shallow cumulus with average individual sizes of less than 1000\,m are difficult to grasp in models with resolutions of several kilometers.

%The new generation of global \textit{storm-resolving} simulations is under constant development and -- with the aid of new generation computing infrastructure -- may close this gap. 

This project is aimed at improving the quantification of cloud feedback. Our starting point is the Large Eddy Simulation (LES) setup that was designed to accompany the EUREC4A field campaign in the South-Western Atlantic trade wind region. We will introduce different flavours of global warming signals to the experiment via its boundary conditions, ranging from simple conceptual \citep{vallis2015response} to complex general circulation model (GCM) derived perturbations \citep{kroner2017separating, li2019high}. 


There are several directions the project could take in later stages.
We could move to larger domains, with the aim of closing the gap to global storm-resolving simulations. We could look at smaller domains, but with increased resolution. And we could exploit the existing setup and add different flavours of global warming to explore the role of different processes.
%Storm-resolving models try to harness the power, latest computing infrastructure has to offer, to directly resolve convection, making the model more independent from assumptions and idealisations, which found their way into the climate models as parametrisations. A global storm-resolving model with a global warming scenario could cover both large and small domains and has the potential to give a significantly more accurate picture of the behaviour of clouds in a warming climate and giving a much better estimate of cloud feedback.



\newpage
\section*{Status Report}

In my second Panel Report I identified three main tasks for the upcoming months: to familiarize myself with the LES setup, to generate boundary conditions for the global warming experiment, and to analyse the existing control simulation. The first two tasks have been completed. The analysis of the control simulation has been neglected in favor of advancing the status of the warming simulations. 

In the last few weeks I started a test run for the warming simulation and proof the reproducibility of my experiment by rerunning parts of the control simulation. In both cases, the work is still in progress, but the test runs seem promising. We applied for 12,000 node hours of computing time via the CLICCS project (for four separate simulations) and have been granted the requested resources.


\section*{LES Setup}

Our experiment setup is based on the ICON-LES experiment, Hauke Schulz created to accompany the EUREC4A field campaign \citep{bony2017eurec4a}. The EUREC4A field campaign was designed to study shallow cumulus clouds and the ICON-LES simulations reproduce these clouds and their mesoscale organisation \citep{schulz2023representation}. This makes them a solid basis for the study of shallow cumulus under realistic global warming conditions.

The experiment setup comprises two nested domains, with the outer domain (\texttt{DOM01}) covering an 10\textdegree by 15\textdegree rectangular area over the Northern Atlantic trade wind region (Fig.~\ref{fig:domains}). The outer domain is initialised via an Initial field and continuously forced via the lateral boundaries and the sea surface (lower boundary). The inner domain (\texttt{DOM02}) is forced at the lateral boundaries by the outer domain, and is initialised later, after the outer domain had time to spin-up. Lateral boundary conditions and sea surface temperatures are updated hourly. \texttt{DOM01} consists of 4.5 million cells with a 600\,m resolution (equivalent surface square edge length), \texttt{DOM02} of 12 million cells with a 300\,m resolution (subsets of R2B12 and R2B13 respectively). A third computing domain with 150\,m resolution has been implemented, but was only sparsely used for the control simulations because of its high computational costs.

\begin{figure}[htb]
  \centering
  \captionsetup{format=plain, width=\capwidth}
  \includegraphics[width=0.9\textwidth]{2023-01/img/eureca_domain.png}
  \caption{\small The study region: position of the EUREC4A LES simulations computing domains \texttt{DOM01} and \texttt{DOM02} (\textcolor{blue}{blue}) in the Atlantic trade wind region.}
  \label{fig:domains}
\end{figure}

The control simulations (done by Hauke Schulz) are completed and the data is publicly available via the DKRZ hosted OpenStack swift object storage and can be loaded via a intake catalog. This greatly facilitates the use of the data.


\clearpage
\subsection*{Reproducibility}

My research will use the already completed EUREC4A simulations as a control run and modify the setup for the perturbation experiments. The technical setup we use has changed over the past two years. The DKRZ replaced its computing cluster and Hauke Schulz ran his experiment on the new cluster during its first months of operation. Since then the software setup has changed and the original binary does not work anymore. The code had to be recompiled and I want to be sure my new binary produces identical output. 

I reran a few hours of the original control simulation setup and compared it to the output of the first run. Original and rerun are in good overall agreement. This is visible in the spatial mean time series of most surface variables (Fig.~\ref{fig:mean_timeseries}). The diagnostic output variables (\texttt{tqv\_dia}, \texttt{tqc\_dia} and \texttt{tqi\_dia}) show periodic outliers and subsequent regression to the values of the control simulation. This itself is a hint, that it could be an output artifact rather than an irregularity during computation.

\begin{figure}[htb]
  \centering
  \captionsetup{format=plain, width=\capwidth}
  \includegraphics[width=0.99\textwidth]{2023-03/img/domain_mean_timeseries.png}
  \caption{\small Domain mean time series for the 2D variables in the control simulation (blue) and tis rerun (orange). Outliers are visible in the output of some variables (\texttt{tqv\_dia}, \texttt{tqc\_dia} and \texttt{tqi\_dia}).}
  \label{fig:mean_timeseries}
\end{figure}

Further analysis shows that the frequency of these outliers matches the convective parametrisation timestep (\texttt{dt\_conv}; Fig~\ref{fig:uhf_outlier}). The convective parametrisation has been disabled via the namelist parameter \texttt{inwp\_convection} ($=0$). This is another indicator that the problem is purely in the output, and would not be visible during runtime. Faulty 2D output would be inconvenient as it limits comparability to the control simulation. This remains unresolved.

\begin{figure}[htb]
  \centering
  \captionsetup{format=plain, width=\capwidth}
  \includegraphics[width=0.99\textwidth]{2023-03/img/tqvtqitqc_timeseries_uhf-rerun.png}
  \caption{\small Frequency of output outliers matches \texttt{dt\_conv} as defined in the runscript ($= timestep \cdot 7$). 
    }
  \label{fig:uhf_outlier}
\end{figure}


\clearpage
\section*{Boundary conditions}

Unlike other LES models (\textit{large eddy simulation}), the simulation we are using is not idealised. It is meant to represent a real domain and time frame, much like a hindcast. In our case time and domain are modelled to match the EUREC4A field campaign in 2020 \citep{bony2017eurec4a}. 
The control simulation uses a combination of reanalysis data (ERA5 sea surface temperatures) and output from storm-resolving ICON simulations \citep{klocke2017rediscovery} as boundary conditions. We reuse these realistic boundary conditions as a basis and add a signal on top that is meant to represent climate change. 
This approach gives is often referred to as a \textit{storyline} (e.g. \cite{shepherd2019storyline}) as it answers the question: \textit{How would this event have looked under global warming?}

Because we are looking at a small scale phenomenon, we want to compute a high resolution experiment and thus are constrained to a limited domain (Fig.~\ref{fig:domains}). There are different approaches to translating the effects of global warming into a small domain. In our first experiment, we decided for an increase in temperature by 4\,K at the surface and a warming profile at the lateral boundaries that reflects a moist adiabat derived from this 4\,K surface warming (Fig.~\ref{fig:deltas}, middle column). 


\begin{figure}[htb]
  \centering
  \captionsetup{format=plain, width=\capwidth}
  \includegraphics[width=0.75\textwidth]{2023-01/img/deltas.png}
  \caption{\small Deltas (warming - control) derived from global high resolution ICON simulations (\textit{GSRM}), analytically assuming a moist adiabatic temperature profile (\textit{Moist adiabat}) and using the single column radiative-convective equilibrium model \textit{konrad} (\textit{RCE}). The GSRM delta is the difference between a control run and a SSP585 emission scenario run. To enhance comparability the surface temperature of the \textit{moist adiabat} and \textit{RCE} plot was chosen to mimic the one found in GSRM. For their use as boundary conditions the changes will be scaled up to 4\,K surface warming.}
  \label{fig:deltas}
\end{figure}


Although this idealized forcing captures the first-order response of the thermodynamic state to global warming, more subtle changes are not be represented. Therefore, in a next step, we will follow a more elaborate treatment of boundary conditions, which is also able to capture changes in their spatial pattern: pseudo global warming (PGW). 
%This forcing is one of the simpler, more idealised approaches to represent global warming in a \textit{LES}. For a later experiment I aim to apply the most realistic global warming signal we can produce: pseudo global warming (PGW). 
This approach subtracts GCM output from a control simulation from the output of a global warming simulation. The resulting fields of differences (\textit{deltas}) are then added on top of our original boundary conditions \citep{brogli2022pseudo}.
%. This approach yields the most detailed representation of our current understanding of global warming and combines it with the real scenario boundary conditions \citep{brogli2022pseudo}. 
% Lukas: Although this idealized forcing captures the first-order response of the thermodynamic state to global warming, more subtle changes are not be represented. Therefore, in a next step, we will follow a more elaborate treatment of boundary conditions, which is also able to capture changes in their spatial pattern: pseudo global warming (PGW). [...]
How will does our conceptual warming compare to this PGW warming signal? Is there added value and what does PGW capture that the simple thermodynamic approach does not? Whether or not this experiment should be included in the first paper should depend on the complexity of the answer to these questions.
%The comparison of this PGW warming to our conceptual warming signal different levels of complexity in our LES forcing will also enable us to attribute parts of the warming signal to different processes. Having a separate experiment with increased temperatures for example allows us to assess its effect on cloud radiative effect separately. A comparison to the effect of the holistic PWG signal will allow quantitative analysis.

%Like the comparison of the simple moist adiabat with the result of the RCE in Figure~\ref{fig:deltas} enables us to separate the effects of radiation and convection,
%\todo[inline]{AKN: inwiefern?}
%The comparison of different levels of complexity in our LES forcing will also enable us to attribute parts of the warming signal to different processes. Having a separate experiment with increased temperatures for example allows us to assess its effect on cloud radiative effect separately. A comparison to the effect of the holistic PWG signal will allow quantitative analysis.
%The comparison of the results of holistic PGW experiment and the low complexity moist adiabat experiment will enable us to quantitatively separate the effects of different processes
%\todo[inline]{\textbf{AKN}: \break mir ist immer noch nicht klar, was genau du dir aus dem Vergleich zwischen idealisiertem Warming und PGW erwartest. Was ist der physikalische Unterschied im Forcing (methodisch ist klar, denke ich) und wie erwartest du, dass es sich auf das Wolkenfeedback auswirkt? Welche Frage kannst du mit dem Vergleich beantworten? Das muss nicht in diesem Bericht geklärt werden, sollte aber Grundlage dafür sein, ob du es in ein Manuskript aufnehmen möchtest. Im Paperoutline sprichst du von warming experiment im Singular. Das könntest du anpassen oder als offene Frage formulieren.}
%\todo[inline]{\textbf{LK}:\break deutlicher machen was die Unterschied PGW-TGW sind (z.b. boundary layer, feuchteprofil). was sind die untershiede die ich mir erhoffe (z.b. vertical profil feuchte, in pgw zb feucht nicht exact konstant) ob subsidence darstellbar ist (ausser als ererbte änderung im feuchteprofil) ist zweifelhaft Taktisch am wichtigsten analyse von 4k warming, alles andere sollte darauf aufbauen}

\section*{Adiabatic warming experiment}

I launched a test run of the warming experiment and produced 4 hours of model output. The spin-up seems to be in a time frame that is comparable to the control simulation (Fig.~\ref{fig:t2m}). Compared to the control simulation there is a stronger cooling of the near surface layers during the spin-up. The air temperature cools roughly 1\,degree at 2\,m height.

\begin{figure}[htb]
  \centering
  \captionsetup{format=plain, width=\capwidth}
  \includegraphics[width=0.5\textwidth]{2023-01/img/t2m_timeseries.png}
  \caption{\small Evolution of the domain mean air temperature at 2\,m after model start-up for the control simulation (\textcolor{blue}{\textbf{blue}}) and adiabatic warming (by 4\,K; \textcolor{red}{\textbf{red}}).}
  \label{fig:t2m}
\end{figure}

The evolution of a vertical column over the same time frame (BCO meteogram, Fig.~\ref{fig:meteogram}), shows the same tendency: near surface levels to cool much stronger during spin-up, than they do in the control simulation. At the same time the stratosphere is warming, which can be interpreted as an adjustment to the relatively crude changes we imposed on the initial conditions (Compare \textit{RCE} and \textit{Moist adiabat} in Fig.~\ref{fig:deltas}).


\begin{figure}[htb]
  \centering
  \captionsetup{format=plain, width=\capwidth}
  \includegraphics[width=0.99\textwidth]{2023-03/img/dual_bco_meteogram_ylabel.png}
  \caption{\small Spin-up in the meteogram (output in a fixed vertical column) temperature record at the Barbados cloud observatory. Plots show difference to initial field over time, with the control simulation (\textbf{left}) 
  versus simulation with adiabatically warmed boundary conditions (and initial field; \textbf{right}).}
  \label{fig:meteogram}
\end{figure}

The warmed domain is much cloudier than the control run, with an increase in cloud cover by 15\,percent (relative to total area, averaged over time and space). When we adjust the temperature in our boundary conditions, we increase specific humidity to keep relative humidity fixed, as is predicted for a warmer world \citep{romps2014analytical}. If the air is effectively cooled by roughly 1\,degree\,K as it enters the domain, this would cause condensation of excess humidity (Fig.~\ref{fig:clct}).
% \todo[inline]{do you understand why this is happening}
The cause of this cooling and whether is is a bug or a feature or the imposed perturbation has yet to be determined. 

\begin{figure}[htb]
  \centering
  \captionsetup{format=plain, width=\capwidth}
  \includegraphics[width=0.99\textwidth]{2023-01/img/clct_compare.png}
  \caption{\small Comparison of the cloud field after 4h simulated time. Left and right show the control and the warming scenario respectively. They depict computing domain \texttt{DOM01}, which includes the island Barbados (green outline). An animated plot for the first 4h is provided under \href{https://owncloud.gwdg.de/index.php/s/8hIIsQGXMJxrXXi}{\url{s.gwdg.de/kOISfY}}
    }
  \label{fig:clct}
\end{figure}


\clearpage
\section*{Outlook}

\subsubsection*{Upcoming Tasks}

During the next weeks I will focus on finishing solving the problem of outliers in the rerun and on producing more output for the warming experiment. At the end of march I want to have clarity on how to handle the output outliers in the rerun. After this is done I will resume work on longer warming simulation and start with the analysis of cloud radiative effect CRE variability evolution over the simulated time span. The CRE variablity will be the main indicator I will use for deciding on how long I want to run the model.

\begin{enumerate}
    \item Apply at the \href{https://sites.google.com/view/cfmip2023/}{CFMIP/GASS conference}
    \item Prepare a talk for the AES seminar
    \item Improve confidence in rerun
    \begin{enumerate}
        \item Compare diagnotic to non-diagnostic (e.g. \texttt{tqv\_dia} to \texttt{tqv})
        \item Try effect of no defined \texttt{dt\_conv}
        \item Try effect of very large \texttt{dt\_conv}
    \end{enumerate}
    \item Improve confidence in warming experiment setup
    % \todo[inline]{Bist du überzeugt, dass der Lauf tut, was wir erwarten? Bjorns "Convince ourselves that the perturbed simulations are reasonable". Wenn ja, wäre es gut aus (oben) genauer auszuführen. Wenn nicht, solltest du es hier als Todo aufnehmen. Oder meinst du das mit (b)?}
    \begin{enumerate}
        \item Restart the simulation and produce a longer run
        % \todo[inline]{Was meinst du hier? Eine längere Simulation im Sinne eines Produktionslaufs? Oder mehr Outputvariablen?}
        \item Is the observed behaviour just spin-up? (Fig.~\ref{fig:meteogram})
        % \todo[inline]{Was meinst du hier genau?}
        \item Why/How is the domain cooling down? 
    \end{enumerate}
    \item Move towards a production run for the warming experiment
    \begin{enumerate}
        \item Apply CRE ananlysis 
        \item Decide on desired simulation length
    \end{enumerate}
    \item Start writing a draft for publication ({A Paper outline is given below})
    \item Outline second experiment
%    \todo[inline]{\textbf{AKN}: Heißt dass du würdest ein PGW Experiment gerne auch schon für dein erstes Paper (3.) auswerten? In dem Paper Outline kommt es nicht vor, oder? \break \break \textbf{HC}:Am liebsten würde ich das schon in mein erstes Paper aufnehmen. ich. habe nur sorge, dass das etwas ambitioniert ist. Auf jeden Fall finde ich es ist Zeit Kontakt mit der Gruppe aufzunehmen und mal anzufragen, ob ich deren Deltas erben kann oder ob ich vielleicht Deltas nach dem gleichen Rezept erstelle.}
    \begin{enumerate}
        \item Talk to swiss group about deltas (EUREC4A MIP)
    \end{enumerate}
\end{enumerate}
%\todo[inline]{\textbf{AKN}: Was davon willst du im nächsten halben Jahr geschafft haben? Was kommt später? \break \break \textbf{HC}: Das soll alles in den nächsten sechs Monaten passieren}



\subsection*{Challenges}

Obtaining a usable binary of the ICON model was more challenging than anticipated. The binary that used to create the control simulation contained outdated dependencies. When recompiled from an up-to-date ICON source, the binaries did not work with the simulation runscript. This is because the runscript requires RTTOV, a synthetic satellite image generator module. RTTOV itself works correctly and does not cause any problems. Despite that, it did not find its way into the main ICON repository. It has been used by Hauke Schulz for the image classification algorithm for mesoscale patterns. If I am interested in using his image classifier, I rely on RTTOV. If I rely on RTTOV, I am bound to an outdated version of the ICON source code. Two options are available: either excluding RTTOV and its functionality and abandoning the mesoscale pattern classifier, or adding RTTOV to the updated ICON code base.

%Ideally I would be able to use an up to date version of ICON on the long run. This may be achieved by creating a ICON feature branch with a working RTTOV module or excluding RTTOV and its functionality. The latter would hinder my ability to analyse mesoscale patterns in the warming simulation.
%\todo[inline]{macht RTTOV immernoch probleme? was war das jetyt? compilation oder output writing errors?}




\subsection*{Supervision}

In our last panel meeting in January we discussed my need for more active supervision and more regular meetings of myself and one of my supervisors. In the aftermath of this discussion Ann Kristin and I started to meet biweekly for 30 to 60 minutes. This has been very helpful and I would like to continue in this modus. 


% \todo[inline]{comments from last time (AKN): Womit bist du zufrieden, womit eher nicht? Was würde dir helfen, schneller oder gezielter voran zu kommen? \break Ich habe gerade noch mal deine letzte Version überflogen. Dabei ist mir in den drei letzten Absätzen des first year Outline aufgefallen, dass du sehr passiv formulierst, z.B. " it is unclear which model output will be used" oder "it is still up to debate". Ich denke, hier wäre es sinnvoll schon in der Satzkonstruktion eine aktivere Rolle von dir zu signalisieren. Im Sinne von: es ist dein Projekt; du musst entscheiden, was du verwenden willst. Natürlich unterstützen wir dich gern in der Entscheidung, indem wir die Möglichkeiten mit dir diskutieren, aber ein Vorschlag oder Diskusionsgrundlage sollte von dir kommen. Vielleicht ist es möglich, das mehr heraus zu bringen, z.B., indem du es als "to do" oder "next step" mit Rollenverteilung und Zeitrahmen ausdrückst.}











\clearpage
\subsection*{Paper Outline}
I have not finished the work on my production runs. Nevertheless I would like to start working on a draft for publication. The following is meant to be an outline in chapter titles.

\begin{enumerate}
    \item Introduction
    \begin{enumerate}
        \item Cloud radiative effect and feedback
        \item Low cloud feedback in specific
        \item Eurec4a campaign and LES setup
        \item \textit{(Mesoscale organisation)}
    \end{enumerate}
    
    \item Methodology
    \begin{enumerate}
        \item Technical details of ICON LES
        \item Role of boundary conditions in ICON LES
        \item A recipe for generating adiabatic warming boundary conditions
    \end{enumerate}
    
    \item Cloud properties and radiative effect in simulations
    \begin{enumerate}
        \item Radiative fluxes (lw, sw, clear sky, cloud)
        \begin{enumerate}
            \item Control simulation
            \item Warming experiment
        \end{enumerate}
        \item Cloud properties (height, thickness, amount) and atmospheric structure
        % \todo[inline]{Von was? Wolken? Struktur der Atmosphäre? ...?}
        \begin{enumerate}
            \item Control simulation
            \item Warming experiment
        \end{enumerate}
        \item \textit{Optional: Organisation (inter quartile range, Iorg or mesoscale patterns)}
        \item Comparison warming versus control (feedback) 
        \item Attribution of cloud radiative effect to processes (e.g. inversion strengthening; see \cite{bretherton2015insights})
    \end{enumerate}
    \item Summary and Conlusion
\end{enumerate}


\begin{comment}
\clearpage
\section*{Time plan}
\todo[inline]{\textbf{AKN}: \break Ich finde den Zeitplan irgendwie unübersichtlich... Vlt weil du Monate doppelt aufführst und sich die tasks von Jahr zu Jahr unterscheiden. Manchmal ist mir nicht klar, ob vlt das Gleiche gemeint ist. Das zweite Jahr beginnt bei dir einmal im September, einmal im Oktober. Wenn ich z.B. sehen will, worauf du dich im Sep/Okt 22 fokusiert hast, muss man in zwei Tabellen schauen. Lange Rede, kurzer Sinn: Wenn du die Muße und Zeit hast, wäre eine einzige Tabelle, vlt am besten im Querformat, vermutlich übersichtlicher.}

% \todo[inline]{has not been modified yet. Same as version 3.1}
\vspace{5mm}
\begin{ganttchart}[
    expand chart=0.97\textwidth,
    y unit title=0.5cm,
    y unit chart=0.5cm,
    title/.style={fill=teal, draw=none},
    title label font=\color{white}\bfseries,
    vgrid,
%    inline,
    milestone inline label node/.append style={left=5mm},
    time slot format=isodate-yearmonth,
    time slot unit=month
    ]
    {2021-08}{2022-10}
    \gantttitlecalendar{year, month} \\

    % \ganttbar[bar/.append style={fill=red!15}]{Work with CIMD}{2021-08}{2021-08} \\
    \ganttbar[bar/.append style={fill=green!15}]{Work for CIMD}{2021-08}{2021-12} \\

    \ganttmilestone{Start PhD}{2021-08} \ganttnewline
    
    \ganttbar[bar/.append style={fill=black!50}]{Sick leave}{2022-01}{2022-01} \\
    \ganttbar[bar/.append style={fill=black!50}]{Vacation}{2022-03}{2022-03} 
    \ganttbar[bar/.append style={fill=black!50}]{}{2022-08}{2022-08} \\

    \ganttbar[bar/.append style={fill=yellow!50}]{Literature research}{2021-09}{2021-12} 
    \ganttbar[bar/.append style={fill=yellow!50}]{}{2022-02}{2022-02} \\
    
    \ganttbar[bar/.append style={fill=red!50}]{NextGEMS}{2021-10}{2021-10} \\
%    \ganttbar[bar/.append style={fill=red!50}]{Monsoon 2.0 Analysis}{2022-04}{2022-04} \\
%    \ganttbar[bar/.append style={fill=red!50}]{Theoretical framework}{2021-12}{2021-12}
%    \ganttbar[bar/.append style={fill=red!50}]{}{2022-02}{2022-02} \\
%    \ganttbar[bar/.append style={fill=green!50}]{Formulate hypothesis}{2022-04}{2022-04} \\
    \ganttbar[bar/.append style={fill=red!50}]{Simulation setup}{2022-04}{2022-07} 
    \ganttbar[bar/.append style={fill=red!50}]{}{2022-09}{2022-10} \\
    
    \ganttbar[bar/.append style={fill=red!50}]{Data analysis}{2022-07}{2022-07}
    \ganttbar[bar/.append style={fill=red!50}]{}{2022-09}{2022-10} \\ 
%    \ganttbar[bar/.append style={fill=red!50}]{Data documentation}{2022-06}{2022-09} \\
%    \ganttbar[bar/.append style={fill=green!50}]{Paper manuscript}{2022-07}{2022-10} \\
%    \ganttbar[bar/.append style={fill=yellow!50}]{}{2022-10}{2022-10} \\

    \ganttvrule[vrule/.append style={red,solid}, vrule offset=.5]{APM-1}{2021-10}
    \ganttvrule[vrule/.append style={red,solid}, vrule offset=.5]{APM-2.1}{2022-04}
    \ganttvrule[vrule/.append style={red,solid}, vrule offset=.5]{APM-2.2}{2022-06}
    \ganttvrule{start}{2021-08}
    \ganttvrule{1 year}{2022-08}
\end{ganttchart}



%\clearpage

%\subsection*{Second year}
%While the workflow should be similar to the first project, there is not yet a clear setup. The plan is to refine the simulation setup, according to the findings from the simulations from the first project, either refining the resolution, or enlarging the domain or refining the physical setup of the simulation. Enlarging the domain is the preferred path, as the goal of the project is to extend the research to a global scale. 

\vspace{5mm}
\begin{ganttchart}[
    expand chart=0.97\textwidth,
    y unit title=0.5cm,
    y unit chart=0.5cm,
    title/.style={fill=teal, draw=none},
    title label font=\color{white}\bfseries,
    vgrid,
%    inline,
    milestone inline label node/.append style={left=5mm},
    time slot format=isodate-yearmonth,
    time slot unit=month
    ]
    {2022-08}{2023-10}
    \gantttitlecalendar{year, month} \\
    
    
    \ganttbar[bar/.append style={fill=black!50}]{Vacation}{2022-08}{2022-08} 
    \ganttbar[bar/.append style={fill=black!50}]{}{2023-03}{2023-03} \\
    \ganttbar[bar/.append style={fill=red!50}]{Setup warming experiment}{2022-09}{2023-01} \\
    \ganttbar[bar/.append style={fill=red!50}]{Debug and validate rerun}{2022-11}{2023-04} \\
    \ganttbar[bar/.append style={fill=red!50}]{Debug and validate warming run}{2023-02}{2023-06} \\
    % \ganttbar[bar/.append style={fill=red!50}]{Data analysis}{2022-09}{2023-02} 
    % \ganttbar[bar/.append style={fill=red!50}]{Production run}{2023-02}{2023-02} \\

%    \ganttbar[bar/.append style={fill=green!50}]{}{2022-09}{2022-09} \\
%    \ganttbar[bar/.append style={fill=yellow!50}]{Literature research}{2022-10}{2022-12} \\
    
%    \ganttbar[bar/.append style={fill=red!50}]{Simulation layout}{2022-12}{2023-01} \\
    
%    \ganttbar[bar/.append style={fill=green!50}]{Formulate hypothesis}{2023-01}{2023-01} \\
%    \ganttbar[bar/.append style={fill=red!50}]{Simulation setup}{2023-02}{2023-04} \\
    % \ganttbar[bar/.append style={fill=red!50}]{Data analysis}{2023-05}{2023-06} \\
%    \ganttbar[bar/.append style={fill=red!50}]{Data documentation}{2023-04}{2023-07} \\
    
    \ganttbar[bar/.append style={fill=green!50}]{Paper manuscript}{2023-05}{2023-09} \\

    \ganttbar[bar/.append style={fill=yellow!50}]{Plan PGW experiment}{2023-05}{2023-10} \\
    \ganttvrule{1 year}{2022-08}
    \ganttvrule{2 years}{2023-08}
    \ganttvrule[vrule/.append style={red,solid}, vrule offset=.5]{APM-3.1}{2023-01}
    \ganttvrule[vrule/.append style={red,solid}, vrule offset=.5]{APM-3.2}{2023-03}
    % \ganttvrule[vrule/.append style={red,solid}, vrule offset=.5]{APM-4}{2023-09}
\end{ganttchart}


%\subsection*{Third year}
%The new generation of \textit{global storm-resolving} simulations is under constant development and -- with the aid of a new generation computing infrastructure -- may deliver model output in unprecedented accuracy. Ideally, I would apply the statistical and methodological tools from the previous two studies to a global scale. This could yield a better estimate of cloud feedback strength than current models can provide.

\vspace{5mm}

\begin{ganttchart}[
    expand chart=0.97\textwidth,
    y unit title=0.5cm,
    y unit chart=0.5cm,
    title/.style={fill=teal, draw=none},
    title label font=\color{white}\bfseries,
    vgrid,
%    inline,
    milestone inline label node/.append style={left=5mm},
    time slot format=isodate-yearmonth,
    time slot unit=month
    ]
    {2023-09}{2024-10}
    \gantttitlecalendar{year, month} \\

    \ganttbar[bar/.append style={fill=yellow!50}]{Literature research}{2023-10}{2023-12} \\
    
    \ganttbar[bar/.append style={fill=green!50}]{Formulate hypothesis}{2024-01}{2024-01} \\
%    \ganttbar[bar/.append style={fill=red!50}]{Simulation setup}{2024-02}{2024-04} \\
    \ganttbar[bar/.append style={fill=red!50}]{Data acquisition}{2024-02}{2024-03} \\
    \ganttbar[bar/.append style={fill=red!50}]{Data analysis}{2024-04}{2024-05} \\
%    \ganttbar[bar/.append style={fill=red!50}]{Data documentation}{2024-04}{2024-07} \\
    \ganttbar[bar/.append style={fill=green!50}]{Paper manuscript}{2024-06}{2024-08} \\
    \ganttbar[bar/.append style={fill=green!50}]{Thesis compilation}{2024-09}{2024-09} \\

    \ganttvrule{2 years}{2023-09}
    \ganttvrule{3 years}{2024-09}
\end{ganttchart}
\end{comment}



\clearpage
\begin{landscape}


% \vspace{5mm}
\begin{ganttchart}[
    expand chart=0.97\linewidth,
    % expand chart=0.97\textwidth,
    y unit title=0.5cm,
    y unit chart=0.5cm,
    title/.style={fill=teal, draw=none},
    title label font=\color{white}\bfseries,
    vgrid,
%    inline,
    milestone inline label node/.append style={left=5mm},
    time slot format=isodate-yearmonth,
    time slot unit=month
    ]
    {2021-08}{2024-10}
    \gantttitlecalendar{year, month} \\

    % before start
    \ganttbar[bar/.append style={fill=green!15}]{Work for CIMD}{2021-08}{2021-12} \\
    \ganttmilestone{Start PhD}{2021-08} \ganttnewline
    \ganttbar[bar/.append style={fill=black!50}]{Sick leave}{2022-01}{2022-01} \\
    \ganttbar[bar/.append style={fill=black!50}]{Vacation}{2022-03}{2022-03} 
    \ganttbar[bar/.append style={fill=black!50}]{}{2022-08}{2022-08} \\


    % first year
    \ganttbar[bar/.append style={fill=yellow!50}]{Literature research}{2021-09}{2021-12} 
    \ganttbar[bar/.append style={fill=yellow!50}]{}{2022-02}{2022-02} \\
    \ganttbar[bar/.append style={fill=red!50}]{NextGEMS}{2021-10}{2021-10} \\
    \ganttbar[bar/.append style={fill=red!50}]{Simulation setup}{2022-04}{2022-07} 
    \ganttbar[bar/.append style={fill=red!50}]{}{2022-09}{2022-10} \\
    \ganttbar[bar/.append style={fill=red!50}]{Data analysis}{2022-07}{2022-07}
    \ganttbar[bar/.append style={fill=red!50}]{}{2022-09}{2022-10} \\ 

    % second year
    \ganttbar[bar/.append style={fill=black!50}]{Vacation}{2022-08}{2022-08} 
    \ganttbar[bar/.append style={fill=black!50}]{}{2023-03}{2023-03} \\
    \ganttbar[bar/.append style={fill=red!50}]{Setup warming experiment}{2022-09}{2023-01} \\
    \ganttbar[bar/.append style={fill=red!50}]{Debug and validate rerun}{2022-11}{2023-04} \\
    \ganttbar[bar/.append style={fill=red!50}]{Debug and validate warming run}{2023-02}{2023-06} \\
    % \ganttbar[bar/.append style={fill=red!50}]{Data analysis}{2022-09}{2023-02} 
    % \ganttbar[bar/.append style={fill=red!50}]{Production run}{2023-02}{2023-02} \\

%    \ganttbar[bar/.append style={fill=green!50}]{}{2022-09}{2022-09} \\
%    \ganttbar[bar/.append style={fill=yellow!50}]{Literature research}{2022-10}{2022-12} \\
    
%    \ganttbar[bar/.append style={fill=red!50}]{Simulation layout}{2022-12}{2023-01} \\
    
%    \ganttbar[bar/.append style={fill=green!50}]{Formulate hypothesis}{2023-01}{2023-01} \\
%    \ganttbar[bar/.append style={fill=red!50}]{Simulation setup}{2023-02}{2023-04} \\
    % \ganttbar[bar/.append style={fill=red!50}]{Data analysis}{2023-05}{2023-06} \\
%    \ganttbar[bar/.append style={fill=red!50}]{Data documentation}{2023-04}{2023-07} \\
    
    \ganttbar[bar/.append style={fill=green!50}]{Paper manuscript}{2023-05}{2023-09} \\

    \ganttbar[bar/.append style={fill=yellow!50}]{Plan PGW experiment}{2023-05}{2023-10} \\
    % \ganttvrule[vrule/.append style={red,solid}, vrule offset=.5]{APM-4}{2023-09}
    
    
    
    % third year
    \ganttbar[bar/.append style={fill=yellow!50}]{Literature research}{2023-10}{2023-12} \\
    \ganttbar[bar/.append style={fill=green!50}]{Formulate hypothesis}{2024-01}{2024-01} \\
%    \ganttbar[bar/.append style={fill=red!50}]{Simulation setup}{2024-02}{2024-04} \\
    \ganttbar[bar/.append style={fill=red!50}]{Data acquisition}{2024-02}{2024-03} \\
    \ganttbar[bar/.append style={fill=red!50}]{Data analysis}{2024-04}{2024-05} \\
%    \ganttbar[bar/.append style={fill=red!50}]{Data documentation}{2024-04}{2024-07} \\
    \ganttbar[bar/.append style={fill=green!50}]{Paper manuscript}{2024-06}{2024-08} \\
    \ganttbar[bar/.append style={fill=green!50}]{Thesis compilation}{2024-08}{2024-08} \\
    
    % yearly marker
    \ganttvrule{\rotatebox{270}{start}}{2021-08}
    \ganttvrule{\rotatebox{270}{1 year}}{2022-08}
    \ganttvrule{\rotatebox{270}{2 years}}{2023-08}
    \ganttvrule{\rotatebox{270}{3 years}}{2024-08}
    % \ganttvrule{}{2021-08}
    % \ganttvrule{}{2022-08}
    % \ganttvrule{}{2023-08}
    % \ganttvrule{}{2024-08}
    
    % panel meetings
    \ganttvrule[vrule/.append style={red,solid}, vrule offset=.5]{\rotatebox{270}{APM-1}}{2021-10}
    \ganttvrule[vrule/.append style={red,solid}, vrule offset=.5]{\rotatebox{270}{APM-2.1}}{2022-04}
    \ganttvrule[vrule/.append style={red,solid}, vrule offset=.5]{\rotatebox{270}{APM-2.2}}{2022-06}
    \ganttvrule[vrule/.append style={red,solid}, vrule offset=.5]{\rotatebox{270}{APM-3.1}}{2023-01}
    \ganttvrule[vrule/.append style={red,solid}, vrule offset=.5]{\rotatebox{270}{APM-3.2}}{2023-03}
    \ganttvrule[vrule/.append style={red,solid}, vrule offset=.5]{\rotatebox{270}{APM-4(?)}}{2023-09}
    
\end{ganttchart}
\end{landscape}



\clearpage
\printbibliography


\end{document}
