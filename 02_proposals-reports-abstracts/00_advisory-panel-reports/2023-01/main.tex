\documentclass{article}

\usepackage[utf8]{inputenc}
\usepackage{comment} % multi-line comment
\usepackage{pdfpages} % include a pdf

%\usepackage[]{todonotes} 
\usepackage[disable]{todonotes} 

\usepackage{geometry} % less bordering white space
    % normal:
    \geometry{a4paper, top=25mm, left=20mm, right=20mm, bottom=40mm, headsep=10mm, footskip=20mm}
\usepackage{pgfgantt} % time table aka. Gantt Chart
\usepackage{csquotes} % \enquote{}
\usepackage{caption} % subfigures
    \usepackage{subcaption} 
\newcommand{\capwidth}{0.9\columnwidth} % used for setting the width of captions
\usepackage{enumitem,amssymb} % for todo lists
% https://tex.stackexchange.com/questions/247681/how-to-create-checkbox-todo-list
\newlist{todolist}{itemize}{2}
\setlist[todolist]{label=$\square$}
\usepackage[ % Für Benutzung von Citation-kürzeln wie natbib
    authordate,
    bibencoding=auto,
    strict,
    backend=biber,
    maxcitenames=2,
    natbib
    ]{biblatex-chicago}
\addbibresource{../bib.bib}
% make colored hyperref links for citations
\usepackage{hyperref}
\usepackage{xcolor}
\hypersetup{
    colorlinks,
    linkcolor={black!50!black},
    citecolor={blue!50!black},
    urlcolor={blue!80!black}
}
% keine schusterjungen, plz :sob:
% https://tex.stackexchange.com/questions/4152/how-do-i-prevent-widow-orphan-lines
\widowpenalty10000
\clubpenalty10000






\title{3. Advisory Panel Meeting on 20.01.2023 \\ Status Report and Outline}
\begin{document}



\vspace*{3cm}
\section*{3. Advisory Panel Meeting (20.01.2023) \\ Status Report and Outline}
\vfill{}
\begin{tabular}{ l l }
    PhD topic : & \textit{Tropical low cloud feedbacks in global storm resolving models} \\
    PhD student : & Hernan Campos \\
    Panel Members : & Bjorn Stevens, Ann-Kristin Naumann, Peter Korn \\
\end{tabular}



\newpage
\section*{Thesis synopsis}

An accurate estimate of the impact of climate change depends on an accurate estimate of climate sensitivity. Cloud feedback is an important factor for climate sensitivity. Poor understanding of the behaviour of clouds in a warming world are a major contribution to the total uncertainty in climate sensitivity \citep{bretherton2015insights,ipcc2021}. 

The difficulty in assessing cloud feedback has its basis in the complex nature of clouds. Both small scales processes as turbulence, and large scale processes as self organisation play an important role for clouds, their behaviour and their response to forcing. But investigating both small and large scales in the same simulation is difficult, because computational resources put a limit to the model setup. This results in studies focusing either on fine resolutions on small domains, or coarse resolutions on large domains. The new generation of global \textit{storm-resolving} simulations is under constant development and -- with the aid of a new generation computing infrastructure -- may close this gap. 

This project is aimed at improving the understanding of cloud feedback. The starting point will be an existing Large Eddy Simulation (LES) setup which will be adapted to reflect a global warming scenario. A good candidate for this model setup is the EUREC4A LES, that was designed to accompany a field campaign in a trade wind region in the South-Western Atlantic. To produce more realistic results than previous studies, we will apply a more realistic forcing. A climate change forcing signal will be derived from differences between a large scale global warming General Circulation Model (GCM) and a present day climate GCM. This method -- referred to as \textit{pseudo global warming} -- has successfully been applied to a variety of regional models to derive site specific climate change predictions \citep{kroner2017separating, li2019high}. 

In later stages of the project will move to larger domains, aiming to close the gap to global storm-resolving simulations. Storm-resolving models try to harness the power, latest computing infrastructure has to offer, to directly resolve convection, making the model more independent from assumptions and idealisations, which found their way into the climate models as parametrisations. A global storm-resolving model with a global warming scenario could cover both large and small domains and has the potential to give a significantly more accurate picture of the behaviour of clouds in a warming climate and giving a much better estimate of cloud feedback.



\newpage
\section*{Status Report}

In my last Panel Report I identified three main tasks for the upcoming months: to familiarize myself with the LES setup, to generate boundary conditions for the global warming experiment, and to analyse the existing control simulation. The first two tasks have been completed. The analysis of the control simulation has been neglected in favor of advancing the status of the warming simulations. I am currently test running simulations with a warming signal.


\section*{LES Setup}

Our experiment setup is based on the ICON-LES experiment, Hauke Schulz created to accompany the EUREC4A field campaign \citep{bony2017eurec4a}. It comprises two nested domains, with the outer domain covering an 10\textdegree by 15\textdegree rectangular area over the Northern Atlantic trade wind region (Fig.~\ref{fig:domains}). The outer domain is initialised via an Initial field and continuously forced via the lateral boundaries and the sea surface (lower boundary). The inner domain is forced at the lateral boundaries by the outer domain, and is initialised later, after the outer domain had time to spin up. Lateral boundary conditions and sea surface temperatures are updated hourly.


\begin{figure}[htb]
  \centering
  \captionsetup{format=plain, width=\capwidth}
  \includegraphics[width=0.9\textwidth]{2023-01/img/eureca_domain.png}
  \caption{\small The study region: position of the EUREC4A LES simulations computing domains (\textcolor{blue}{blue}) in the Atlantic trade wind region. The outer domain (\texttt{DOM01}) consists of 4.5 million cells with a 600\,m resolution (equivalent surface square edge length), the inner domain (\texttt{DOM02}) consists of 12 million cells with a 300\,m resolution (subsets of R2B12 and R2B13 respectively).}
  \label{fig:domains}
\end{figure}

The control simulations (done by Hauke Schulz) are completed and the data is available. Through the use of a suite of new tools they were made available in a way that greatly facilitates the use of the data.


\section*{Boundary conditions}

Unlike other LES models (\textit{large eddy simulation}), the simulation we are using is not idealised. Like a hindcast, it is meant to represent a real domain and time frame. In our case time and domain are modelled to match the EUREC4A field campaign in 2020 \citep{bony2017eurec4a}. 
The control simulation uses a combination of reanalysis data (ERA5 sea surface temperatures) and output from storm resolving ICON simulations \citep{klocke2017rediscovery} as boundary conditions. We reuse these realistic boundary conditions as a basis and add a signal on top which is meant to represent climate change. 
This approach gives is often referred to as a \textit{storyline} (e.g. \cite{shepherd2019storyline}) as it answers the question: \textit{How would this event have looked under global warming?}

Because we are looking at a small scale phenomenon, we want to compute a high resolution experiment and thus are constrained to a limited domain (Fig.~\ref{fig:domains}). There are different approaches of translating the effects of global warming into a small domain. In our first experiment, we decided for an increase in temperature by 4 degree at the surface and a warming profile at the lateral boundaries that reflects a moist adiabat derived from this 4 degree surface warming (Fig.~\ref{fig:deltas}, middle column). 


\begin{figure}[htb]
  \centering
  \captionsetup{format=plain, width=\capwidth}
  \includegraphics[width=0.8\textwidth]{2023-01/img/deltas.png}
  \caption{\small Deltas (warming - control) derived from global high resolution ICON simulations (\textit{GSRM}), analytically assuming a moist adiabatic temperature profile (\textit{Moist adiabat}) and using the single column radiative-convective equilibrium model \textit{konrad} (\textit{RCE}). The GSRM delta is the difference between a control run and an emission scenario run. For the latter two the surface temperature was chosen to mimic the one found in GSRM.}
  \label{fig:deltas}
\end{figure}


This forcing is one of the simpler, more idealised approaches to reflect global warming in a \textit{LES}. For a later experiment we aim to apply the most realistic global warming signal we can produce: pseudo global warming (PGW). This approach subtracts GCM output from a control simulation from output of a global warming simulation. The produced fields of differences (\textit{deltas}) are then added on top of our original boundary conditions. This approach yields the most detailed representation of our current understanding of global warming and combines it with the real scenario boundary conditions \citep{brogli2022pseudo}. Like the comparison of the simple moist adiabat with the result of the RCE in Figure~\ref{fig:deltas} enables us to separate effects of radiation and convection, the comparison of different levels of complexity in our LES forcings will increase our ability to attribute parts of the warming signal to different processes.


\clearpage
\section*{First experiment, a sneak peek}

The first run (4\,degree warming along moist adiabat) is in the making. It seems to spin up in a time frame that is comparable to the control simulation (Fig.~\ref{fig:t2m}). Compared to the control simulation there is a stronger cooling of the near surface layers during the spin-up. The air temperature cools roughly 1\,degree at 2\,m height.

\begin{figure}[htb]
  \centering
  \captionsetup{format=plain, width=\capwidth}
  \includegraphics[width=0.5\textwidth]{2023-01/img/t2m_timeseries.png}
  \caption{\small Evolution of the domain mean air temperature at 2\,m after model start-up for the control simulation (\textcolor{blue}{\textbf{blue}}) and adiabatic warming (by 4 degree; \textcolor{red}{\textbf{red}}).}
  \label{fig:t2m}
\end{figure}

If we look at the evolution of a vertical column over the same time frame, we see the same tendency of the near surface levels to cool much stronger during the spin-up, than they do in the control simulation. At the same time the stratosphere is warming. The warming of the stratosphere can be interpreted as an adjustment to the relatively crude changes we imposed on the intial conditions (Compare \textit{RCE} and \textit{Moist adiabat} in Fig.~\ref{fig:deltas}).


\begin{figure}[htb]
  \centering
  \captionsetup{format=plain, width=\capwidth}
  \includegraphics[width=0.99\textwidth]{2023-01/img/dual_bco_meteogram.png}
  \caption{\small Spin up in the meteogram (output in a fixed vertical column) temperature record at the Barbados cloud observatory. Plots show difference to initial field over time, with the control simulation (\textbf{left}) 
  versus simulation with adiabatically warmed boundary conditions (and initial field; \textbf{right}).}
  \label{fig:meteogram}
\end{figure}

The warmed domain is much cloudier than the control run, with an increase in cloud cover by 15\,percent (averaged over time and space). When we adjust the temperature in our boundary conditions, we increase specific humidity to keep relative humidity fixed, as is predicted for a warmer world \citep{romps2014analytical}. If the air is effectively cooled by roughly 1\,degree\,K as it enters the domain, this would cause condensation of excess humidity (Fig.~\ref{fig:clct}).

\begin{figure}[htb]
  \centering
  \captionsetup{format=plain, width=\capwidth}
  \includegraphics[width=0.99\textwidth]{2023-01/img/clct_compare.png}
  \caption{\small Comparison of the cloud field after 4h simulated time. Left and right show the control and the warming scenario respectively. They depict computing domain \texttt{DOM01}, which includes the island Barbados (green outline). An animated plot for the first 4h is provided under \href{https://owncloud.gwdg.de/index.php/s/8hIIsQGXMJxrXXi}{\url{s.gwdg.de/kOISfY}}
    }
  \label{fig:clct}
\end{figure}



% \clearpage

\section*{Outline}

I participated in a CLICCS proposal that -- if successful -- would provide computing resources for four full experiment runs. The test runs of the first experiment are promising (see above). It may need some smaller adjustments (e.g. of humidity) but could be ready to be run for the whole time span soon. One the holistic side of the  complexity spectrum would be a complete pseudo global warming simulation. In the context of EUREC4A modeling efforts, another group is working on pseudo global warming in the Atlantic trade wind region (\href{https://eurec4a.eu/motivation}{EUREC4A-MIP}). We may be able to use their set of deltas. Even if we don't, it will improve comparability to use the same GCM output as a basis.




\subsubsection*{Challenges}

It proved more challenging than expected to obtain a usable binary of the ICON model. The binary that was used to create the control simulation included outdated dependencies and recompiling from source yielded binaries that did not work with the runscript of the control simulations. The main culprit for the incompatibilities of the runscript and the AES version of ICON is RTTOV, a synthetic satellite image generator module. RTTOV has been used by Hauke Schulz for the image classification algorithm that detects mesoscale patterns. Because the image classifier was trained with satellite data, it can only be applied on model data with the help of RTTOV. The working binary we produced is based on a version of code that remained from the control simulations. Version control for this code has been neglected. Ideally I would be able to use an up to date version of ICON on the long run. This may be achieved by creating a ICON feature branch with a working RTTOV module.




\subsubsection*{Upcoming Tasks}

\begin{itemize}
    \item Finish the current production run (4 degree warming)
    \item Apply CRE ananlysis to warming experiment
    \item Set up a second experiment
    \item Start writing a paper (I could already start with methods and introduction)
    \item Data postprocessing (bit compression, conversion to \texttt{zarr}, upload to \textit{SWIFT}, \texttt{intake} catalog)
\end{itemize}


% comments from last time:
\todo[inline]{Womit bist du zufrieden, womit eher nicht? Was würde dir helfen, schneller oder gezielter voran zu kommen?}
\todo[inline]{Ich habe gerade noch mal deine letzte Version überflogen. Dabei ist mir in den drei letzten Absätzen des first year Outline aufgefallen, dass du sehr passiv formulierst, z.B. " it is unclear which model output will be used" oder "it is still up to debate". Ich denke, hier wäre es sinnvoll schon in der Satzkonstruktion eine aktivere Rolle von dir zu signalisieren. Im Sinne von: es ist dein Projekt; du musst entscheiden, was du verwenden willst. Natürlich unterstützen wir dich gern in der Entscheidung, indem wir die Möglichkeiten mit dir diskutieren, aber ein Vorschlag oder Diskusionsgrundlage sollte von dir kommen. Vielleicht ist es möglich, das mehr heraus zu bringen, z.B., indem du es als "to do" oder "next step" mit Rollenverteilung und Zeitrahmen ausdrückst.}






\clearpage
\vspace{5mm}
\begin{ganttchart}[
    expand chart=0.97\textwidth,
    y unit title=0.5cm,
    y unit chart=0.5cm,
    title/.style={fill=teal, draw=none},
    title label font=\color{white}\bfseries,
    vgrid,
%    inline,
    milestone inline label node/.append style={left=5mm},
    time slot format=isodate-yearmonth,
    time slot unit=month
    ]
    {2021-08}{2022-10}
    \gantttitlecalendar{year, month} \\

    % \ganttbar[bar/.append style={fill=red!15}]{Work with CIMD}{2021-08}{2021-08} \\
    \ganttbar[bar/.append style={fill=green!15}]{Work for CIMD}{2021-08}{2021-12} \\

    \ganttmilestone{Start PhD}{2021-08} \ganttnewline
    
    \ganttbar[bar/.append style={fill=black!50}]{Sick leave}{2022-01}{2022-01} \\
    \ganttbar[bar/.append style={fill=black!50}]{Vacation}{2022-03}{2022-03} 
    \ganttbar[bar/.append style={fill=black!50}]{}{2022-08}{2022-08} \\

    \ganttbar[bar/.append style={fill=yellow!50}]{Literature research}{2021-09}{2021-12} 
    \ganttbar[bar/.append style={fill=yellow!50}]{}{2022-02}{2022-02} \\
    
    \ganttbar[bar/.append style={fill=red!50}]{NextGEMS}{2021-10}{2021-10} \\
%    \ganttbar[bar/.append style={fill=red!50}]{Monsoon 2.0 Analysis}{2022-04}{2022-04} \\
%    \ganttbar[bar/.append style={fill=red!50}]{Theoretical framework}{2021-12}{2021-12}
%    \ganttbar[bar/.append style={fill=red!50}]{}{2022-02}{2022-02} \\
%    \ganttbar[bar/.append style={fill=green!50}]{Formulate hypothesis}{2022-04}{2022-04} \\
    \ganttbar[bar/.append style={fill=red!50}]{Simulation setup}{2022-04}{2022-07} 
    \ganttbar[bar/.append style={fill=red!50}]{}{2022-09}{2022-10} \\
    
    \ganttbar[bar/.append style={fill=red!50}]{Data analysis}{2022-07}{2022-07}
    \ganttbar[bar/.append style={fill=red!50}]{}{2022-09}{2022-10} \\ 
%    \ganttbar[bar/.append style={fill=red!50}]{Data documentation}{2022-06}{2022-09} \\
%    \ganttbar[bar/.append style={fill=green!50}]{Paper manuscript}{2022-07}{2022-10} \\
%    \ganttbar[bar/.append style={fill=yellow!50}]{}{2022-10}{2022-10} \\

    \ganttvrule[vrule/.append style={red,solid}, vrule offset=.5]{APM-1}{2021-10}
    \ganttvrule[vrule/.append style={red,solid}, vrule offset=.5]{APM-2.1}{2022-04}
    \ganttvrule[vrule/.append style={red,solid}, vrule offset=.5]{APM-2.2}{2022-06}
    \ganttvrule{start}{2021-08}
    \ganttvrule{1 year}{2022-08}
\end{ganttchart}



%\clearpage

%\subsection*{Second year}
%While the workflow should be similar to the first project, there is not yet a clear setup. The plan is to refine the simulation setup, according to the findings from the simulations from the first project, either refining the resolution, or enlarging the domain or refining the physical setup of the simulation. Enlarging the domain is the preferred path, as the goal of the project is to extend the research to a global scale. 

\vspace{5mm}
\begin{ganttchart}[
    expand chart=0.97\textwidth,
    y unit title=0.5cm,
    y unit chart=0.5cm,
    title/.style={fill=teal, draw=none},
    title label font=\color{white}\bfseries,
    vgrid,
%    inline,
    milestone inline label node/.append style={left=5mm},
    time slot format=isodate-yearmonth,
    time slot unit=month
    ]
    {2022-08}{2023-09}
    \gantttitlecalendar{year, month} \\
    
    
    \ganttbar[bar/.append style={fill=black!50}]{Vacation}{2022-08}{2022-08} 
    \ganttbar[bar/.append style={fill=black!50}]{}{2023-03}{2023-03} \\
    \ganttbar[bar/.append style={fill=red!50}]{Simulation setup}{2022-09}{2023-01} \\
    \ganttbar[bar/.append style={fill=red!50}]{Data analysis}{2022-09}{2023-02} 
    \ganttbar[bar/.append style={fill=red!50}]{}{2023-04}{2023-06} \\
    \ganttbar[bar/.append style={fill=red!50}]{Production run}{2023-02}{2023-02} \\

%    \ganttbar[bar/.append style={fill=green!50}]{}{2022-09}{2022-09} \\
%    \ganttbar[bar/.append style={fill=yellow!50}]{Literature research}{2022-10}{2022-12} \\
    
%    \ganttbar[bar/.append style={fill=red!50}]{Simulation layout}{2022-12}{2023-01} \\
    
%    \ganttbar[bar/.append style={fill=green!50}]{Formulate hypothesis}{2023-01}{2023-01} \\
%    \ganttbar[bar/.append style={fill=red!50}]{Simulation setup}{2023-02}{2023-04} \\
    % \ganttbar[bar/.append style={fill=red!50}]{Data analysis}{2023-05}{2023-06} \\
%    \ganttbar[bar/.append style={fill=red!50}]{Data documentation}{2023-04}{2023-07} \\
    
    \ganttbar[bar/.append style={fill=green!50}]{Paper manuscript}{2023-05}{2023-08} \\

    \ganttvrule{1 year}{2022-08}
    \ganttvrule{2 years}{2023-08}
    \ganttvrule[vrule/.append style={red,solid}, vrule offset=.5]{APM-3}{2023-01}
\end{ganttchart}


%\subsection*{Third year}
%The new generation of \textit{global storm-resolving} simulations is under constant development and -- with the aid of a new generation computing infrastructure -- may deliver model output in unprecedented accuracy. Ideally, I would apply the statistical and methodological tools from the previous two studies to a global scale. This could yield a better estimate of cloud feedback strength than current models can provide.

\vspace{5mm}

\begin{ganttchart}[
    expand chart=0.97\textwidth,
    y unit title=0.5cm,
    y unit chart=0.5cm,
    title/.style={fill=teal, draw=none},
    title label font=\color{white}\bfseries,
    vgrid,
%    inline,
    milestone inline label node/.append style={left=5mm},
    time slot format=isodate-yearmonth,
    time slot unit=month
    ]
    {2023-09}{2024-10}
    \gantttitlecalendar{year, month} \\

    \ganttbar[bar/.append style={fill=yellow!50}]{Literature research}{2023-10}{2023-12} \\
    
    \ganttbar[bar/.append style={fill=green!50}]{Formulate hypothesis}{2024-01}{2024-01} \\
%    \ganttbar[bar/.append style={fill=red!50}]{Simulation setup}{2024-02}{2024-04} \\
    \ganttbar[bar/.append style={fill=red!50}]{Data acquisition}{2024-02}{2024-03} \\
    \ganttbar[bar/.append style={fill=red!50}]{Data analysis}{2024-04}{2024-05} \\
%    \ganttbar[bar/.append style={fill=red!50}]{Data documentation}{2024-04}{2024-07} \\
    \ganttbar[bar/.append style={fill=green!50}]{Paper manuscript}{2024-06}{2024-08} \\
    \ganttbar[bar/.append style={fill=green!50}]{Thesis compilation}{2024-09}{2024-09} \\

    \ganttvrule{2 years}{2023-09}
    \ganttvrule{3 years}{2024-09}
\end{ganttchart}





\clearpage
\printbibliography



\end{document}
