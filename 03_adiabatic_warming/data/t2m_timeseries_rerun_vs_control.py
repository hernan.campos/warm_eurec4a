import os
import sys
# module_path = os.path.join(os.getcwd(), '../submodules/')
module_path = '/home/m/m300872/warm_eurec4a/submodules/'
if not module_path in sys.path:
    sys.path.append(module_path)

import xarray as xr
import numpy  as np
import file_handling as fh
import matplotlib.pyplot as plt
import plot_utils as pu

def masked_file_list(directory, mask):
    return [os.path.join(directory, f) for f in fh.get_filelist(directory) if mask in f]

def dataset_from_directory(directory, mask = 'DOM01_surf', rename_dict={'ncells':'cell'}, convert_time=True, clear_dims=False):
    data = xr.open_mfdataset(masked_file_list(directory, mask))
    if rename_dict: data = data.rename(rename_dict)
    if convert_time: data = data.assign_coords({"time": icontime2numpytime(data.time.values)})
    if clear_dims:   data = remove_dims(data, clear_dims)
    return data.rename()

def remove_dims(data, dims={'height':10, 'height_2':2}):
    for key, value in dims.items():
        data = data.sel({key:value}).drop(key)
    return data

def single_icontime2numpytime(timefloat):
    date_string = str(int(timefloat))
    h_ = 24 * (timefloat - int(timefloat))
    h  = int(h_)
    m_ = 60 * (h_ - h)
    m  = int(np.round(m_))
    if m == 60: m = 0; h += 1
    return np.datetime64(f'{date_string[0:4]}-{date_string[4:6]}-{date_string[6:8]}T{str(h).rjust(2,"0")}:{str(m).rjust(2,"0")}')

def is_iterable(x):
    try:    len(x); return True
    except: return False

def icontime2numpytime(timefloat):
    if is_iterable(timefloat): 
        return np.asarray([icontime2numpytime(t) for t in timefloat])
    else: return single_icontime2numpytime(timefloat)

def print_overview(data, title, dims=['cell', 'time']):
    print('-' * (14+13+9+9+4))
    print(title)
    print('short name'.ljust(14), 'mean'.rjust(12), 'min'.rjust(10), 'max'.rjust(10) )
    print('-' * (14+13+9+9+4))
    for var in list(data):
        mean = data[var].mean(dim=dims, skipna=True).values
        if is_iterable(mean): mean = mean[0]
        mini = data[var].min(dim=dims, skipna=True).values
        if is_iterable(mini): mini = mini[0]
        maxi = data[var].max(dim=dims, skipna=True).values
        if is_iterable(maxi): maxi = maxi[0]
        print(var.ljust(14), f'{mean:.2f}'.rjust(12), f'{mini:.2f}'.rjust(10), f'{maxi:.2f}'.rjust(10))
    print()

def get_path(experiment):
    # if   experiment == 'warming': return '/work/mh1126/m300872/eureca_icon/EUREC4A/experiments/4kadiabat'
    if   experiment == 'warming': return '/work/mh1126/m300872/eureca_icon/EUREC4A/experiments/4kadiabat2'
    elif experiment == 'rerun'  : return '/work/mh1126/m300872/eureca_icon/EUREC4A/experiments/r02EUREC4A'

def radiation_data(experiment):
    dat  = dataset_from_directory(get_path(experiment), mask='DOM01_rad', rename_dict={'ncells':'cell'}, convert_time=True)
    drops = ['ddt_temp_radsw','ddt_temp_radlw','lwflx_dn_clr','lwflx_dn','lwflx_up_clr','lwflx_up']
    return dat.drop_vars(drops).drop_dims('height_2')

def surface_data(experiment):
    # if experiment == 'control': 
    #     return cat.simulations.ICON.LES_CampaignDomain_control['surface_DOM01'].to_dask()
    data = dataset_from_directory(get_path(experiment) , convert_time=True, rename_dict={'ncells':'cell'})
    data = data.sel(height=10).sel(height_2=2).drop_vars(['height', 'height_2'])
    return data

def overlap_timesteps(datasets):
    t = datasets[0].time.values
    for dataset in datasets[1::]:
        t = [x for x in t if x in dataset.time.values]
    return t

def time_overlap(datasets):
    t = overlap_timesteps(datasets)
    return tuple([d.sel(time=t) for d in datasets])

def make():
    r = surface_data('rerun')
    w = surface_data('warming')
    r_series = (r.time.values, r['t_2m'].mean(dim=['cell']).values)
    w_series = (w.time.values, w['t_2m'].mean(dim=['cell']).values)
    
    fig, ax = plt.subplots()#figsize=(4,8))
    ax.plot(*r_series, color='blue')
    ax.plot(*w_series, color='red')
    ax.set_xlabel('model time / `MM-DD hh`')
    ax.set_ylabel('air temperature at 2m / K')
    # ax.invert_yaxis()
    # ax.set_xticklabels(ax.get_xticks(), rotation = -45)
    pu.remove_spines(ax)
    plt.xticks(rotation = 25)
    plt.show()
    
    