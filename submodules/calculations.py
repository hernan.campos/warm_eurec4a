"""
Collection of functions used in the setup and analysis of the EUREC4A LES warming simulations

This module provides functions for atmospheric profile manipulation and calculations, including the addition of prognostic variables, conversion between pressure and height, and handling of specific humidity and relative humidity in atmospheric datasets.
- add_cloud_top_height(dataset, standard_name, dim, threshold): Adds the cloud top height (CTH) as a new variable to the dataset based on cloud water content.
- add_cumulative_mean_variable(dataset, variable_name, bottom_up, name_modifier): Calculate the cumulative mean of a variable along its first dimension and add it to the dataset.
- add_density_from_ideal_gas(dataset, varnames, target_name): Calculate and add density from ideal gas law to the dataset.
- add_estimated_lapse_rate_stability(dataset, standard_name, dim): Adds the estimated lapse rate stability (ELRS = LTS - EIS) as a new variable to the dataset 
- add_full_level_height(dataset, varnames): Adds full-level height data to a dataset.
- add_lifting_condensation_level_pressure(dataset, standard_name, dim): Adds the LCL pressure as a new variable to the dataset for each element along a specified dimension.
- add_lower_tropospheric_stability(dataset, standard_name, dim): Adds the Lower Tropospheric Stability (LTS) as a new variable to the dataset for each element along a specified dimension.
- add_pressure_from_ideal_gas(dataset, varnames, target_name, drop_density): Calculate and add pressure from ideal gas law to the dataset.
- add_prognostic_variables(dataset, varnames): Calculates and adds prognostic variables like virtual potential temperature and density to the dataset. Uses metpy.
- add_relative_humidity(dataset, varnames): Calculates and adds relative humidity to the dataset based on specific humidity. Uses metpy.
- add_to_temperature(dataset, delta, varnames): -- No docstring available. --
- add_varnames_to_default(custom_varnames): Updates the default variable names dictionary with custom variable names.
- apply_warming_p_fixed(dataset, varnames): -- No docstring available. --
- apply_warming_rho_fixed(dataset, varnames): -- No docstring available. --
- approximate_moist_lapse_rate(T, p): Approximation of the local potential temperature lapse rate ([Wood and Bretherton, 2006](https://doi.org/10.1175/JCLI3988.1))
- construct_volume(column, shape): -- No docstring available. --
- delta_adiabatic_profile(t_high, t_low, heights, p_surf): -- No docstring available. --
- density_from_ideal_gas(pressure, specific_humidity, temperature): Calculate density using the ideal gas law.
- estimated_lapse_rate_stability(pressure, temperature, specific_humidity, altitude): Calculate the Estimated Lapse Rate Stability (ELRS)
- factorization(n): Find prime factors of an integer using Pollard's rho algorithm.
- get_varnames(custom_varnames): Alias for add_varnames_to_default function.
- get_z_full(z_ifc): Retrieves or calculates full-level heights.
- get_z_half(): Retrieves half-level heights from an online intake catalog.
- hydrostatic_profile(h, p_surf): Calculates the hydrostatic profile of pressure given an array of heights. 
- index_of_nearest_value(array, value): Finds the index of the element in the array closest to a given value.
- interpolate_full_level(half_level): Interpolates full-level heights based on half-level heights.
- isqrt(x, always_bigger): Calculate integer square roots and return two factors (m, n) closest to x.
- joined_profile(p, p_threshold, T_s, T_min): Combines dry and moist adiabatic lapse rates into a single profile. 
- lifting_condensation_level_pressure(pressure, temperature, specific_humidity): Calculate the Lifting Condensation Level (LCL) pressure from atmospheric profiles.
- lonlat_to_xyz(lon, lat): Converts geographical coordinates (longitude and latitude) to 3D Cartesian coordinates on a unit sphere.
- lower_tropospheric_stability(pressure, temperature): Calculate the Lower Tropospheric Stability (LTS) from atmospheric pressure and temperature profiles.
- metpy_dry_lapse_profile(p, T_s, T_min): Calculates the dry adiabatic lapse rate profile given an array of pressures and a starting temperature.
- metpy_moist_lapse_profile(p, T_s, T_min): Calculates the moist adiabatic lapse rate profile given an array of pressures and a starting temperature.
- minmax(dataarray): Finds minimum and maximum value of an array
- pres2heigth(p, max_height, meter_per_level): Converts pressure levels to corresponding heights based on a hydrostatic profile.
- pressure_from_ideal_gas(density, specific_humidity, temperature): Calculate pressure using the ideal gas law.
- replace_level_with_height(data, height_name): Replaces level data with corresponding height data in a dataset.
- restore_pressure(dataset, varnames, target_name, density_name): -- No docstring available. --
- restore_specific_humidity(dataset, varnames, drop_RH): Restores specific humidity from relative humidity in the dataset. Uses metpy.
- save_density(dataset, varnames, target_name): -- No docstring available. --
- select_nearest_cells(dataset, lon_list, lat_list, grid, varnames): Selects nearest cells from a dataset based on given longitude and latitude coordinates
- specific_gas_constant(specific_humidity): Calculate the specific gas constant.
- warm_atmospheric_profile_RHfixed(ifile, ofile, warming_profile, varnames): -- No docstring available. --

Please refer to the individual function docstrings for detailed descriptions and usage instructions.

Author: Hernan Campos
Email: hernan.campos@mpimet.mpg.de
"""

import math
import numpy as np
import xarray as xr
import metpy.calc
import metpy.constants
import metpy.units # this is a redundancy.
from   metpy.units import units
import eurec4a
from scipy.spatial import KDTree # select_nearest_cells

# The import from konrad was failing on the first, but succeding on second attempt. With this error message:
# File ~/.local/lib/python3.10/site-packages/climt/_components/_berger_solar_insolation.pyx:1, in init climt._components._berger_solar_insolation()
# ValueError: numpy.ndarray size changed, may indicate binary incompatibility. Expected 96 from C header, got 88 from PyObject
# This is the work around
try:
    from konrad.lapserate import get_moist_adiabat as konrad_moist_adiabat
except:
    try:    from konrad.lapserate import get_moist_adiabat as konrad_moist_adiabat
    except: print('import of konrad failed. the function `joined_profile` will not be usable.')
# get_moist_adiabat(p, p_s=None, T_s=300.0, T_min=155.0)
# https://konrad.readthedocs.io/_modules/konrad/lapserate.html#get_moist_adiabat


DEFAULT_VARNAMES = {'pressure':'pres',
                    'temperature':'temp',
                    'potential temperature':'theta',
                    'virtual potential temperature':'theta_v',
                    'density':'rho',
                    'specific humidity':'qv',
                    'relative humidity':'RH',
                    'half level height':'z_ifc',
                    'full level height':'z'}

def add_varnames_to_default(custom_varnames=None):
    """
    Updates the default variable names dictionary with custom variable names.

    Args:
        custom_varnames (dict): Dictionary containing custom variable names. Defaults to None.

    Returns:
        dict: Dictionary of variable names. Default values are taken from `DEFAULT_VARNAMES` and replaced if present in `custom_varnames`
    """
    global DEFAULT_VARNAMES
    varnames = DEFAULT_VARNAMES.copy()
    if custom_varnames: # if any given, this will overwrite the default
        for key, value in custom_varnames.items(): varnames[key] = value
    return varnames

def get_varnames(custom_varnames=None): 
    """
    Alias for add_varnames_to_default function.

    Args:
        custom_varnames (dict): Dictionary containing custom variable names. Defaults to None.

    Returns:
        dict: Dictionary of variable names. Default values are taken from `DEFAULT_VARNAMES` and replaced if present in `custom_varnames`
    """
    return add_varnames_to_default(custom_varnames)


def add_prognostic_variables(dataset, varnames=None):
    """
    Calculates and adds prognostic variables like virtual potential temperature and density to the dataset. Uses metpy.

    Args:
        dataset (xarray.Dataset): Input dataset.
        varnames (dict): Dictionary containing variable names. Defaults to None. `varnames` are added to default values from `add_varnames_to_default`.

    Returns:
        xarray.Dataset: Dataset with added prognostic variables.
    """
    varnames = add_varnames_to_default(varnames)
    p  = dataset[varnames['pressure'   ]].values * units(dataset[varnames['pressure'   ]].attrs['units'])
    t  = dataset[varnames['temperature']].values * units(dataset[varnames['temperature']].attrs['units'])
    qv = dataset[varnames['specific humidity']].values   # ratio of water vapor mass to total moist air parcel mass
    
    eps   = metpy.calc.mixing_ratio_from_specific_humidity(qv)
    theta = metpy.calc.potential_temperature(p, t)
    theta_v = metpy.calc.virtual_potential_temperature(p,t,eps)
    rho   = metpy.calc.density(p, t, eps)
    
    dimnames = dataset[varnames['temperature']].dims
    dataset[varnames['potential temperature']] = (dimnames, theta.magnitude)
    dataset[varnames['virtual potential temperature']] = (dimnames, theta_v.magnitude)
    dataset[varnames['density']] = (dimnames, rho.magnitude)
    attributes = {'long_name':'potential temperature', 'units':str(theta.units)}
    dataset[varnames['potential temperature']] = dataset[varnames['potential temperature']].assign_attrs(attributes)
    attributes = {'long_name':'virtual potential temperature', 'units':str(theta_v.units)}
    dataset[varnames['virtual potential temperature']] = dataset[varnames['virtual potential temperature']].assign_attrs(attributes)
    attributes = {'long_name':'density', 'units':str(rho.units)}
    dataset[varnames['density']] = dataset[varnames['density']].assign_attrs(attributes)
    return dataset

def add_relative_humidity(dataset, varnames=None):
    """
    Calculates and adds relative humidity to the dataset based on specific humidity. Uses metpy.

    Args:
        dataset (xarray.Dataset): Input dataset.
        varnames (dict): Dictionary containing variable names. Defaults to None. `varnames` are added to default values from `add_varnames_to_default`.

    Returns:
        xarray.Dataset: Dataset with added relative humidity.
    """
    varnames = add_varnames_to_default(varnames)
    pressure          = dataset[varnames['pressure']].values          * units(dataset[varnames['pressure']].attrs['units'])
    temperature       = dataset[varnames['temperature']].values       * units(dataset[varnames['temperature']].attrs['units'])
    specific_humidity = dataset[varnames['specific humidity']].values * units(dataset[varnames['specific humidity']].attrs['units'])
    relative_humidity = metpy.calc.relative_humidity_from_specific_humidity(pressure, temperature, specific_humidity)
    
    # On adding variables to the dataset: https://stackoverflow.com/a/56590572
    dimnames = dataset[varnames['specific humidity']].dims
    dataset[varnames['relative humidity']] = (dimnames, relative_humidity.magnitude)
    attributes = {'long_name':'relative humidity', 'units':'1'}
    dataset[varnames['relative humidity']] = dataset[varnames['relative humidity']].assign_attrs(attributes)
    return dataset

def restore_specific_humidity(dataset, varnames=None, drop_RH=True):
    """
    Restores specific humidity from relative humidity in the dataset. Uses metpy.

    Args:
        dataset (xarray.Dataset): Input dataset.
        varnames (dict): Dictionary containing variable names. Defaults to None. `varnames` are added to default values from `add_varnames_to_default`.
        drop_RH (bool): Flag to drop the relative humidity variable after restoring specific humidity. Defaults to True.

    Returns:
        xarray.Dataset: Dataset with restored specific humidity.
    """
    varnames = add_varnames_to_default(varnames)
    p  = dataset[varnames['pressure']].values    * units('pascal')
    t  = dataset[varnames['temperature']].values * units('kelvin')
    rh = dataset[varnames['relative humidity']].values
    
    eps = metpy.calc.mixing_ratio_from_relative_humidity(p, t, rh)
    qv = metpy.calc.specific_humidity_from_mixing_ratio(eps)
    # On adding variables to the dataset: https://stackoverflow.com/a/56590572
    dimnames = dataset[varnames['relative humidity']].dims
    attributes = dataset[varnames['specific humidity']].attrs
    dataset[varnames['specific humidity']] = (dimnames, qv.magnitude)
    dataset[varnames['specific humidity']].assign_attrs(attributes)
    if drop_RH: dataset = dataset.drop_vars(varnames['relative humidity'])
    return dataset

def hydrostatic_profile(h, p_surf=100000):
    """
    Calculates the hydrostatic profile of pressure given an array of heights. 
    
    Calculates stepwise from the surface assuming a standard atmosphere, using 
    [`metpy.calc.add_height_to_pressure`](https://unidata.github.io/MetPy/latest/api/generated/metpy.calc.add_height_to_pressure.html).

    Args:
        h (numpy.array): Array of heights im meter.
        p_surf (float, optional): Surface pressure in Pascal. Defaults to 100000.

    Returns:
        numpy.array: Hydrostatic pressure profile.
        
    Notes:
        Standard atmosphere reference: United States. National Oceanic, Atmospheric Administration, & United States. Air Force. (1976). US Standard Atmosphere, 1976 (Vol. 76, No. 1562). National Oceanic and Atmospheric Administration.
    """
    p = np.zeros(len(h))
    p[0] = p_surf
    for i in range(1,len(p)):
        pressure = p[i-1] * units('Pa')
        d_height   = (h[i] - h[i-1]) * units('m')
        calc = metpy.calc.add_height_to_pressure(pressure, d_height).to(units('Pa'))
        p[i] = calc.magnitude
        if p[i] == np.nan: p[i] = 0.0
    return p


def metpy_dry_lapse_profile(p, T_s=300, T_min=None):
    """
    Calculates the dry adiabatic lapse rate profile given an array of pressures and a starting temperature.
    
    Uses [`metpy.calc.dry_lapse`](https://unidata.github.io/MetPy/latest/api/generated/metpy.calc.dry_lapse.html).
    If `T_min` is provided every values < `T_min` will be replaced with `T_min`.
    
    Args:
        p (numpy.array): Array of pressures in Pascal.
        T_s (float, optional): Surface temperature in Kelvin. Defaults to 300 K.
        T_min (float, optional): Lowest returned temperature. Defaults to None (not applied).

    Returns:
        numpy.array: Dry adiabatic lapse rate profile.
    """
    t = metpy.calc.dry_lapse(p * units('Pa'),  temperature=T_s * units('kelvin'))
    if not T_min == None: t = np.where(t.magnitude > T_min, t, T_min * units('kelvin'))
    return t

def metpy_moist_lapse_profile(p, T_s=300, T_min=None):
    """
    Calculates the moist adiabatic lapse rate profile given an array of pressures and a starting temperature.
    
    Uses [`metpy.calc.moist_lapse`](https://unidata.github.io/MetPy/latest/api/generated/metpy.calc.moist_lapse.html).
    If `T_min` is provided every values < `T_min` will be replaced with `T_min`.

    Args:
        p (numpy.array): Array of pressures in Pascal.
        T_s (float, optional): Surface temperature in Kelvin. Defaults to 300 K.
        T_min (float, optional): Lowest returned temperature. Defaults to None (not applied).

    Returns:
        numpy.array: Moist adiabatic lapse rate profile.
    """
    t = metpy.calc.moist_lapse(p * units('Pa'), T_s * units('K'))
    if not T_min == None: t = np.where(t.magnitude > T_min, t, T_min * units('kelvin'))
    return t

# def joined_profile(p, p_threshold=95000, T_s=300, T_min=None):
#     """
#     Combines dry and moist adiabatic lapse rates into a single profile. 
    
#     The returned profile is a dry lapse rate above the given pressure threshold,
#     and a moist lapse rate below the given pressure threshold. 
#     If `T_min` is provided every values < `T_min` will be replaced with `T_min`.

#     Args:
#         p (numpy.array): Array of pressures in Pascal.
#         p_threshold (float, optional): Pressure threshold between dry and moist profiles. Defaults to 95000 Pascal.
#         T_s (float, optional): Surface temperature in Kelvin. Defaults to 300 K.
#         T_min (float, optional): Lowest returned temperature. Defaults to None (not applied).

#     Returns:
#         numpy.array: Combined lapse rate temperature profile.
#     """
#     dry = metpy_dry_lapse_profile(p, T_s=T_s)
    
#     is_moist_profile = p <= p_threshold
#     is_dry_profile = p >  p_threshold

#     if min_temperature is not None:
#         moist_lapse_profile = konrad_moist_adiabat(
#             pressure_levels[is_moist_profile], 
#             T_s=dry_lapse_profile[is_moist_profile][0].magnitude,
#             T_min=min_temperature
#         )
#     else:
#         moist_lapse_profile = konrad_moist_adiabat(
#             pressure_levels[is_moist_profile], 
#             T_s=dry_lapse_profile[is_moist_profile][0].magnitude
#         )
    
#     moist = konrad_moist_adiabat(p[hi], T_s=dry[hi][0].magnitude)#, T_min=T_min)
#     t = np.empty(len(p))
#     t[lo] = dry[lo]
#     t[hi] = moist
#     return t


# def joined_profile(pressure_levels, pressure_threshold=95000, surface_temperature=300, min_temperature=None):
#     """
#     Combines dry and moist adiabatic lapse rates into a single temperature profile.

#     The returned profile uses a dry lapse rate above the specified pressure threshold,
#     and a moist lapse rate below the specified pressure threshold. If `min_temperature` 
#     is provided, it will be passed to the moist adiabat calculation.

#     Args:
#         pressure_levels (numpy.array): Array of pressure levels in Pascal.
#         pressure_threshold (float, optional): Pressure threshold to switch between dry and moist profiles. Defaults to 95000 Pascal.
#         surface_temperature (float, optional): Surface temperature in Kelvin. Defaults to 300 K.
#         min_temperature (float, optional): Minimum allowable temperature for the moist adiabat. Defaults to None (not applied).

#     Returns:
#         numpy.array: Combined lapse rate temperature profile.
#     """
#     dry_lapse_profile = metpy_dry_lapse_profile(pressure_levels, T_s=surface_temperature)
    
#     is_moist_profile = pressure_levels <= pressure_threshold
#     is_dry_profile = pressure_levels > pressure_threshold
    
#     if min_temperature is not None:
#         moist_lapse_profile = konrad_moist_adiabat(
#             pressure_levels[is_moist_profile], 
#             T_s=dry_lapse_profile[is_moist_profile][0].magnitude,
#             T_min=min_temperature
#         )
#     else:
#         moist_lapse_profile = konrad_moist_adiabat(
#             pressure_levels[is_moist_profile], 
#             T_s=dry_lapse_profile[is_moist_profile][0].magnitude
#         )
    
#     combined_profile = np.empty(len(pressure_levels))
#     combined_profile[is_dry_profile] = dry_lapse_profile[is_dry_profile]
#     combined_profile[is_moist_profile] = moist_lapse_profile
    
#     return combined_profile



def joined_profile(p, p_threshold=95000, T_s=300, T_min=None):
    """
    Combines dry and moist adiabatic lapse rates into a single temperature profile.

    The returned profile uses a dry lapse rate above the specified pressure threshold,
    and a moist lapse rate below the specified pressure threshold. If `T_min` 
    is provided, it will be passed to the moist adiabat calculation.

    Args:
        p (numpy.array): Array of pressures in Pascal.
        p_threshold (float, optional): Pressure threshold to switch between dry and moist profiles. Defaults to 95000 Pascal.
        T_s (float, optional): Surface temperature in Kelvin. Defaults to 300 K.
        T_min (float, optional): Minimum allowable temperature for the moist adiabat. Defaults to None (not applied).

    Returns:
        numpy.array: Combined lapse rate temperature profile.
    """
    dry_lapse_profile = metpy_dry_lapse_profile(p, T_s=T_s)
    
    is_moist_profile = p <= p_threshold
    is_dry_profile = p > p_threshold
    
    if T_min is not None:
        moist_lapse_profile = konrad_moist_adiabat(
            p[is_moist_profile], 
            T_s=dry_lapse_profile[is_moist_profile][0].magnitude,
            T_min=T_min
        )
    else:
        moist_lapse_profile = konrad_moist_adiabat(
            p[is_moist_profile], 
            T_s=dry_lapse_profile[is_moist_profile][0].magnitude
        )
    
    combined_profile = np.empty(len(p))
    combined_profile[is_dry_profile] = dry_lapse_profile[is_dry_profile]
    combined_profile[is_moist_profile] = moist_lapse_profile
    
    return combined_profile


def index_of_nearest_value(array, value):
    """
    Finds the index of the element in the array closest to a given value.

    Args:
        array (numpy.array): The input array.
        value (float): The target value to find the closest element in the array.

    Returns:
        int or None: Index of the element closest to the given value in the array,
                     or None if the array consists entirely of NaNs.
        
    Notes:
        Based on https://www.geeksforgeeks.org/find-the-nearest-value-and-the-index-of-numpy-array/
    """
    # Check if the array consists entirely of NaNs
    if np.all(np.isnan(array)): return None
    difference_array = np.absolute(array - value)
    if np.all(np.isnan(difference_array)): return None
    index = np.nanargmin(difference_array)
    return index


def pres2heigth(p, max_height=50000, meter_per_level=1):
    """
    Converts pressure levels to corresponding heights based on a hydrostatic profile.

    Args:
        p (numpy.array): Array of pressure levels.
        max_height (float, optional): Maximum height in meters. Defaults to 50000 meter.
        meter_per_level (float, optional): Height difference per level in meters. Defaults to 1 meter.

    Returns:
        numpy.array: Array of heights corresponding to the input pressure levels.
    """
    h = np.linspace(0,max_height, int(max_height/meter_per_level))
    p_ref = hydrostatic_profile(h, p[0])
    h = np.asarray([h[index_of_nearest_value(p_ref, value)] for value in p])
    return h


def isqrt(x, always_bigger=True):
    '''
    Calculate integer square roots and return two factors (m, n) closest to x.

    This function calculates the integer square root for the input x by finding two integers m and n 
    such that their product is closest to x without exceeding it. If always_bigger is set to True,
    it ensures that m * n is always larger than or equal to x. Useful for distributing plots in a grid.
    
    Args:
    - x (int): The integer to be partitioned.
    - always_bigger (bool): If True, ensures m * n is always >= x. Defaults to True.

    Returns:
    - Tuple[int, int]: Two integers (m, n) where m * n is closest to x.
    '''
    m = int(np.sqrt(x))
    n = int(x/m)
    if m*n < x and always_bigger : m += 1
    return m,n


def factorization(n):
    '''
    Find prime factors of an integer using Pollard's rho algorithm.

    Args:
    - n (int): The integer to be factorized.

    Returns:
    - List[int]: A list of prime factors of n.

    This function employs Pollard's rho algorithm to find the prime factors of the input integer n. 
    It iteratively looks for factors by cycling through a sequence, aiming to find divisors of n.
    '''
    factors = []
    def get_factor(n):
        x_fixed = 2
        cycle_size = 2
        x = 2
        factor = 1

        while factor == 1:
            for count in range(cycle_size):
                if factor > 1: break
                x = (x * x + 1) % n
                factor = math.gcd(x - x_fixed, n)
            cycle_size *= 2
            x_fixed = x
        return factor

    while n > 1:
        next = get_factor(n)
        factors.append(next)
        n //= next
    return factors


def lonlat_to_xyz(lon, lat):    
    """
    Converts geographical coordinates (longitude and latitude) to 3D Cartesian coordinates on a unit sphere.

    Args:
        lon: longitude in degree E
        lat: latitude in degree N
    
    Returns:
        numpy array (3, len (lon)) with coordinates on unit sphere.
    
    Note:
        Taken from [EasyGEMS](https://easy.gems.dkrz.de/Processing/healpix/plot-difference.html)
    """
    return np.array(
        (
            np.cos(np.deg2rad(lon)) * np.cos(np.deg2rad(lat)),
            np.sin(np.deg2rad(lon)) * np.cos(np.deg2rad(lat)),
            np.sin(np.deg2rad(lat)),
        )
    ).T

def select_nearest_cells(dataset, lon_list, lat_list, grid=None, varnames={'cell':'cell', 'latitude':'lat', 'longitude':'lon'}):
    """
    Selects nearest cells from a dataset based on given longitude and latitude coordinates
    
    Uses KDTree for efficient querying.

    Args:
        dataset: xarray dataset. If it has no grid information, please provide this information via argument `grid`
        lon_list: list of longitudes in degree E
        lat: list of latitudes in degree N
        grid: xarray dataset containing grid information. If none is given `dataset` is expected to contain the required information.
        varnames: dictionary of coordinate names, defaults to ['cell', 'lat', 'lon']
    
    Returns:
        dataset with selected cells
    
    Note:
        Taken from [EasyGEMS](https://easy.gems.dkrz.de/Processing/healpix/plot-difference.html)
    """
    if grid == None: grid = dataset
    var_xyz = lonlat_to_xyz(lon=dataset[varnames['longitude']], lat=dataset[varnames['latitude']])
    tree = KDTree(var_xyz)
    distances, indices = tree.query(lonlat_to_xyz(lon_list, lat_list))
    return dataset.isel({varnames['cell']:indices})


def specific_gas_constant(specific_humidity):
    '''
    Calculate the specific gas constant.

    This value can be calculated as the weighted average of the gas constants of dry air and water vapor:

    $$
    R_{\text{specific}} = R_d + ( R_v - R_d ) \times q_v
    $$

    Args:
    - specific_humidity (Quantity): Specific humidity of the air parcel.

    Returns:
    - Quantity: The specific gas constant.

    Raises:
    - ValueError: If specific humidity is missing units.
    '''
    try: unit = specific_humidity.units # make sure specific_humidity has a unit.
    except: specific_humidity = specific_humidity * units('kilogram/kilogram')
    Rd = metpy.constants.dry_air_gas_constant 
    Rv = metpy.constants.water_gas_constant
    Rspecific = Rd.to_base_units() + (Rv.to_base_units() - Rd.to_base_units()) * specific_humidity.to_base_units()
    return Rspecific.to(Rd.units)


def pressure_from_ideal_gas(density, specific_humidity, temperature):
    r'''
    Calculate pressure using the ideal gas law.

    $$ P = \rho \cdot R_{\text{specific}} \cdot T $$

    Args:
    - density (Quantity): Density of the air parcel.
    - specific_humidity (Quantity): Specific humidity of the air parcel.
    - temperature (Quantity): Temperature of the air parcel.

    Returns:
    - Quantity: The calculated pressure.

    Raises:
    - ValueError: If density or temperature is missing units.
    '''
    try: unit = density.units # make sure specific_humidity has a unit.
    except: density = density * units('kg / m**3')
    try: unit = temperature.units # make sure specific_humidity has a unit.
    except: temperature = temperature * units('kelvin')
    R = specific_gas_constant(specific_humidity)
    pressure = density.to_base_units() * R.to_base_units() * temperature.to_base_units()
    return pressure.to(units('Pa'))


def density_from_ideal_gas(pressure, specific_humidity, temperature):
    r'''
    Calculate density using the ideal gas law.

    $$ \rho = \frac{P}{R_{\text{specific}} \cdot T} $$

    Args:
    - pressure (Quantity): Pressure of the air parcel.
    - specific_humidity (Quantity): Specific humidity of the air parcel.
    - temperature (Quantity): Temperature of the air parcel.

    Returns:
    - Quantity: The calculated density.

    Raises:
    - ValueError: If pressure or temperature is missing units.
    '''
    try: unit = pressure.units # make sure specific_humidity has a unit.
    except: pressure = pressure * units('Pa')
    try: unit = temperature.units # make sure specific_humidity has a unit.
    except: temperature = temperature * units('kelvin')
    R = specific_gas_constant(specific_humidity)
    density =  pressure.to_base_units() / (R.to_base_units() * temperature.to_base_units())
    return density.to(units('kg / m**3'))


def add_pressure_from_ideal_gas(dataset, varnames=None, target_name=False, drop_density=True):
    '''
    Calculate and add pressure from ideal gas law to the dataset.

    Args:
    - dataset (xarray.Dataset): Dataset containing specific humidity, temperature, and possibly density.
    - varnames (dict, optional): Dictionary containing variable names. Defaults to None.
    - target_name (str or bool, optional): Name to assign to the pressure variable. Defaults to False.
    - drop_density (bool, optional): If True, drops the density variable after calculation. Defaults to True.

    Returns:
    - xarray.Dataset: Dataset with added pressure variable.

    Raises:
    - ValueError: If density variable is not found or if density is missing units.
    '''
    varnames = add_varnames_to_default(varnames)
    if not target_name: target_name = varnames['pressure']
    try: density = dataset[varnames['density']].values * units(str(dataset[varnames['density']].attrs['units']))
    except: raise ValueError(f'density not given under given name "{varnames["density"]}"')
    
    specific_humidity = dataset[varnames['specific humidity']].values * units('g / kg')
    temperature = dataset[varnames['temperature']].values * units('kelvin') 
    dimnames = dataset[varnames['pressure']].dims
    dataset[target_name] = (dimnames, pressure_from_ideal_gas(density, specific_humidity, temperature).magnitude)
    dataset[target_name] = dataset[target_name].assign_attrs({'long_name':'pressure', 'units':'Pa'})
    if drop_density: dataset = dataset.drop_vars([varnames['density']])
    return dataset


def add_density_from_ideal_gas(dataset, varnames=None, target_name=False):
    '''
    Calculate and add density from ideal gas law to the dataset.

    Args:
    - dataset (xarray.Dataset): Dataset containing specific humidity, temperature, and pressure.
    - varnames (dict, optional): Dictionary containing variable names. Defaults to None.
    - target_name (str or bool, optional): Name to assign to the density variable. Defaults to False.

    Returns:
    - xarray.Dataset: Dataset with added density variable.
    '''
    varnames = add_varnames_to_default(varnames)
    if not target_name: target_name = varnames['density']

    specific_humidity = dataset[varnames['specific humidity']].values * units('g / kg')
    temperature = dataset[varnames['temperature']].values * units('kelvin') 
    pressure = dataset[varnames['pressure']].values * units('Pa') 
    dimnames = dataset[varnames['pressure']].dims
    density = density_from_ideal_gas(pressure, specific_humidity, temperature)
    density = density.to(units('kg / m^3')) # comes as 'kg * Pa / J' which is the same with J = Pa m^3
    dataset[target_name] = (dimnames, density.magnitude)
    dataset[target_name] = dataset[target_name].assign_attrs({'long_name':'density', 'units':str(density.units)})
    return dataset


def minmax(dataarray):
    '''
    Finds minimum and maximum value of an array

    Args:
    - dataarray (xarray.Dataarray or numpy.ndarray): An array with values.

    Returns:
    - Tuple[int or float, int or float]: The lowest and the highest value encountered in dataarray
    '''
    return np.nanmin(dataarray.values), np.nanmax(dataarray.values)


def interpolate_full_level(half_level):
    """
    Interpolates full-level heights based on half-level heights.

    Args:
    - half_level (array-like): Array containing half-level heights.

    Returns:
    - numpy.ndarray: Interpolated full-level heights.
    """
    return np.asarray([(half_level[i] + half_level[i - 1]) / 2 for i in range(1, len(half_level))])

def get_z_half(): 
    """
    Retrieves half-level heights from an online intake catalog.
    """
    cat = eurec4a.get_intake_catalog()['simulations']['grids']
    d = cat['ecf22d17-dcee-1510-a807-11ae4a612be0'].to_dask()
    d = d['z_ifc'].median(dim='cell')
    return np.flip(d.values)

def get_z_full(z_ifc=None):
    """
    Retrieves or calculates full-level heights.

    Args:
    - z_ifc (numpy.ndarray or None): Array of half-level heights. If None, defaulting to data from eurec4a intake catalog.

    Returns:
    - numpy.ndarray: Full-level heights.
    """
    if z_ifc is None: # default case
        try: 
            cat = eurec4a.get_intake_catalog()['simulations']['grids']
            d = cat['ecf22d17-dcee-1510-a807-11ae4a612be0'].to_dask()
            d = d['z_ifc'].median(dim='cell')
            z_ifc = np.flip(d.values)
        except Exception as e:
            print(f"Failed to retrieve z_ifc data: {e}")
    z_full = interpolate_full_level(z_ifc)
    return z_full


def add_full_level_height(dataset, varnames=None):
    """
    Adds full-level height data to a dataset.

    Args:
    - dataset (xarray.Dataset): Dataset to which the full-level height data will be added.
    - varnames (dict or None): Dictionary containing variable names for specific data fields.

    Returns:
    - xarray.Dataset: Dataset with added full-level height data.
    """
    varnames = add_varnames_to_default(varnames) 
    dimnames = dataset[varnames['pressure']].dims
    try:
        dataset[varnames['full level height']] = (dimnames, get_z_full(dataset['z_ifc']))
    except:
        dataset[varnames['full level height']] = (dimnames, get_z_full())
    dataset[varnames['full level height']].assign_attrs(
        long_name=f'full level height (interpolated)', unit=f'meter'
    )
    return dataset


def replace_level_with_height(data, height_name='height'):
    """
    Replaces level data with corresponding height data in a dataset.

    Args:
    - data (xarray.Dataset): Dataset containing level data to be replaced.
    - height_name (str): Name of the height variable to be assigned.

    Returns:
    - xarray.Dataset: Dataset with replaced level data.
    """
    height = get_z_full()
    replacements = height[data[height_name].values.astype(int) - 1]
    replacements = np.flip(replacements)
    data = data.assign_coords({height_name: replacements})
    return data


def construct_volume(column, shape):
    """
    Constructs a 3D volume with specified shape based on a column of values.

    Args:
    - column (array-like): 1D array or list containing values representing a column in the volume.
    - shape (tuple): Tuple specifying the desired shape of the resulting volume (3D array).

    Returns:
    - numpy.ndarray: 3D array representing the volume, where each level is constructed by replicating the column values.
      The levels are stacked along the first axis to form the volume.
    """
    levels = [np.ones(shape) * value for value in column]
    return np.asarray(levels)


def add_cumulative_mean_variable(dataset, variable_name, bottom_up=True, name_modifier=None):
    """
    Calculate the cumulative mean of a variable along its first dimension and add it to the dataset.

    Args:
    - dataset: xarray.Dataset containing the variable of interest
    - variable_name: Name of the variable to calculate the cumulative mean for
    - bottom_up: Boolean indicating the direction of integration (True for bottom-up, False for top-down). Default is True.
    - name_modifier: Function to modify the name of the new variable. Default is to append an underscore.

    Returns:
    - dataset_with_mean: xarray.Dataset with the cumulative mean variable added
    """
    def default_name_modifier(name): return f'{name}_mean'
    if name_modifier is None: name_modifier = default_name_modifier
    dim_name = dataset[variable_name].dims[0]
    slice_indices = slice(None, None, -1) if bottom_up else slice(None)  # work in reverse order (bottom_up) or not
    variable = dataset[variable_name].isel({dim_name: slice_indices})
    cumulative_sum_variable = variable.cumsum(dim=dim_name)
    num_elements = len(dataset[dim_name])
    cumulative_mean_variable = cumulative_sum_variable / xr.DataArray(np.arange(1, num_elements + 1), dims=dim_name)
    if bottom_up: cumulative_mean_variable = cumulative_mean_variable.isel({dim_name: slice_indices})
    dataset = dataset.assign({name_modifier(variable_name): cumulative_mean_variable})
    return dataset
    

def lifting_condensation_level_pressure(pressure, temperature, specific_humidity):
    """
    Calculate the Lifting Condensation Level (LCL) pressure from atmospheric profiles.

    Parameters:
    - pressure: xarray.DataArray
        Atmospheric pressure profile in Pascals.
    - temperature: xarray.DataArray
        Atmospheric temperature profile in Kelvin.
    - specific_humidity: xarray.DataArray
        Atmospheric specific humidity profile (dimensionless).

    Returns:
    - float
        The LCL pressure in Pascals for the surface values in the profile.

    Notes:
    - It is assumed that [-1] is the index of the surface values.
    - The function calculates the dewpoint from specific humidity and then computes the LCL using MetPy's functions.
    """
    pressure    = pressure    * metpy.units.units('Pa')
    temperature = temperature * metpy.units.units('K')
    specific_humidity = specific_humidity
    dewpoint = metpy.calc.dewpoint_from_specific_humidity(pressure, temperature, specific_humidity)
    try:
        LCL_pres, _ = metpy.calc.lcl(pressure[-1], temperature[-1], dewpoint[-1], eps=1e-2) # limiting the accuracy to 1% for efficiencies sake
        return LCL_pres.magnitude
    except ValueError:
        return np.nan

def add_lifting_condensation_level_pressure(dataset, standard_name='lcl_pres', dim='height'):
    """
    Adds the LCL pressure as a new variable to the dataset for each element along a specified dimension.

    Parameters:
    - dataset: xarray.Dataset
        The dataset containing the atmospheric variables necessary for the calculation.
    - standard_name: str, optional
        The name for the new variable to be added to the dataset. Default is 'lcl_pres'.
    - dim: str, optional
        The name of the dimension along which the LCL pressure will be calculated. Default is 'height'.

    Returns:
    - xarray.Dataset
        The input dataset with an added variable containing the LCL pressure values.

    Notes:
    - The function checks for the presence of required variables ('pres', 'temp', 'qv') and their units.
    - The LCL pressure is calculated for each profile along the specified dimension and added to the dataset.
    """
    for var in ['pres', 'temp', 'qv']:
        assert var in dataset, f"Variable '{var}' is missing from the dataset."
    assert metpy.units.units(dataset['temp'].attrs['units']).to_base_units() == metpy.units.units('kelvin').to_base_units(), f"Variable 'temp' is not in unit 'K'."
    assert metpy.units.units(dataset['pres'].attrs['units']).to_base_units() == metpy.units.units('pascal').to_base_units(), f"Variable 'pres' is not in unit 'Pa'."
    
    original_attrs = {var: dataset[var].attrs for var in list(dataset.dims)} # store dim attrs
    dataset[standard_name] = xr.apply_ufunc(
        lifting_condensation_level_pressure,
        dataset["pres"],
        dataset["temp"],
        dataset["qv"],
        input_core_dims=([dim], [dim], [dim]), # Apply along dim
        vectorize=True,  # Enable vectorized execution
        dask="parallelized",  # allow dask to work in parallel
        output_dtypes=[float],
        dask_gufunc_kwargs={'allow_rechunk': True} # so the whole profile can be forwarded at once
    )
    
    attributes = {'standard_name': standard_name, 'long_name': 'LCL pressure', 'units': 'Pa'}
    dataset[standard_name] = dataset[standard_name].assign_attrs(attributes)
    for var, attrs in original_attrs.items(): dataset[var] = dataset[var].assign_attrs(attrs) # restore dim attrs
    return dataset

def lower_tropospheric_stability(pressure, temperature):
    """
    Calculate the Lower Tropospheric Stability (LTS) from atmospheric pressure and temperature profiles.

    Parameters:
    - pressure: xarray.DataArray
        Atmospheric pressure profile in Pascals.
    - temperature: xarray.DataArray
        Atmospheric temperature profile in Kelvin.

    Returns:
    - float or None: The LTS value in Kelvin, calculated as the difference in potential temperature between 
                     700 hPa and the surface or NaN if the pressure array consists entirely of NaNs.

    Notes:
    - The function identifies the nearest pressure levels to 700 hPa and the surface (assumed to < 1200 hPa) for LTS calculation.
    - Potential temperature is calculated for these levels, and LTS is the difference between them.
    """
    pressure    = pressure    * metpy.units.units('Pa')
    temperature = temperature * metpy.units.units('K')
    potential_temperature = metpy.calc.potential_temperature(pressure, temperature)
    i_700  = index_of_nearest_value(pressure.magnitude,  70000)
    i_surf = index_of_nearest_value(pressure.magnitude, 120000)
    if i_700 == None or i_surf == None: return np.nan 
    LTS = potential_temperature[i_700] - potential_temperature[i_surf]
    return LTS.magnitude

def add_lower_tropospheric_stability(dataset, standard_name='lts', dim='height'):
    """
    Adds the Lower Tropospheric Stability (LTS) as a new variable to the dataset for each element along a specified dimension.

    Parameters:
    - dataset: xarray.Dataset
        The dataset containing the atmospheric variables necessary for the calculation.
    - standard_name: str, optional
        The name for the new variable to be added to the dataset. Default is 'lts'.
    - dim: str, optional
        The name of the dimension along which the LTS will be calculated. Default is 'height'.

    Returns:
    - xarray.Dataset
        The input dataset with an added variable containing the LTS values.

    Notes:
    - The function checks for the presence of required variables ('pres', 'temp') and their units.
    - LTS is calculated for each profile along the specified dimension and added to the dataset.
    """
    for var in ['pres', 'temp']:
        assert var in dataset, f"Variable '{var}' is missing from the dataset."
    assert metpy.units.units(dataset['temp'].attrs['units']).to_base_units() == metpy.units.units('kelvin').to_base_units(), f"Variable 'temp' is not in unit 'K'."
    assert metpy.units.units(dataset['pres'].attrs['units']).to_base_units() == metpy.units.units('pascal').to_base_units(), f"Variable 'pres' is not in unit 'Pa'."
    
    original_attrs = {var: dataset[var].attrs for var in list(dataset.dims)} # store dim attrs
    dataset[standard_name] = xr.apply_ufunc(
        lower_tropospheric_stability,
        dataset["pres"],
        dataset["temp"],
        input_core_dims=([dim], [dim]), # Apply along dim
        vectorize=True,  # Enable vectorized execution
        dask="parallelized",  # allow dask to work in parallel
        output_dtypes=[float],
        dask_gufunc_kwargs={'allow_rechunk': True} # so the whole profile can be forwarded at once
    )
    
    attributes = {'standard_name':standard_name, 'long_name':'lower tropospheric stability', 'units':'K'}
    dataset[standard_name] = dataset[standard_name].assign_attrs(attributes)
    for var, attrs in original_attrs.items(): dataset[var] = dataset[var].assign_attrs(attrs) # restore dim attrs
    return dataset

def approximate_moist_lapse_rate(T,p):
    r'''
    Approximation of the local potential temperature lapse rate ([Wood and Bretherton, 2006](https://doi.org/10.1175/JCLI3988.1))
    
    $\Gamma_m(T,p) = \frac{g}{c_p} \Big[ 1 - \frac{1 + L_v q_s(T,p) R_a^{-1}T^{-1}}{1 + L_v^2 q_s(T,p) c_p^{-1}R_v^{-1}T^{-2}} \Big], \quad(5)$ 
    
    where 
    - $L_υ$ is the latent heat of vaporization
    - $q_s$ is the saturation mixing ratio,
    - $R_a$ and $R_υ$ are the gas constants for dry and water vapor, respectively,
    - $g$ is the gravitational acceleration, and
    - $c_p$ is the specific heat of air at constant pressure.

    Parameters:
    - T: xarray.DataArray or float
        Temperature in Kelvin.
    - p: xarray.DataArray or float
        Pressure in Pascals.

    Returns:
    - float
        The local lapse rate in K/m.
    '''
    Lv = metpy.constants.water_heat_vaporization
    Ra = metpy.constants.dry_air_gas_constant
    Rv = metpy.constants.water_gas_constant
    g  = metpy.constants.earth_gravity
    cp = metpy.constants.dry_air_spec_heat_press
    qs = metpy.calc.saturation_mixing_ratio(p, T)
    upper = 1 + Lv * qs / (Ra * T)
    lower = 1 + Lv**2 * qs / (cp * Rv * T**2)
    result = (g/cp) * (1 - (upper/lower))
    return result.to_base_units()

def estimated_lapse_rate_stability(pressure, temperature, specific_humidity, altitude):
    """
    Calculate the Estimated Lapse Rate Stability (ELRS)
    
    ELRS is a component of the Estimated Inversion Strength (EIS). ELRS is calculated based on the residual of the more 
    known formulation of EIS, which is based on Lower Tropospheric Stability (LTS) and a lapse rate contribution to LTS, 
    estimated from a fixed lapse rate at 850 hPa.

    Where EIS is defined as:
    EIS = LTS - Γ_m (z_{700hPa} - z_{LCL})
    the residual is:
    EIS - LTS = Γ_m (z_{700hPa} - z_{LCL}) = ELRS
    where Γ_m is the moist adiabatic lapse rate, z_{700hPa} is the altitude at 700 hPa, and z_{LCL} is the altitude at the Lifting Condensation Level (LCL).

    Parameters:
    - pressure: xarray.DataArray
        Atmospheric pressure profile in Pascals.
    - temperature: xarray.DataArray
        Atmospheric temperature profile in Kelvin.
    - specific_humidity: xarray.DataArray
        Atmospheric specific humidity profile (dimensionless).
    - altitude: xarray.DataArray
        Altitude profile in meters.

    Returns:
    - float
        The ELRS value in Kelvin, representing the lapse rate contribution to LTS.

    Notes:
    - This function calculates the LCL pressure, identifies the indices for the LCL, 850 hPa, and 700 hPa levels, and computes the potential temperature at 850 hPa.
    - It then estimates the lapse rate (Γ_m) at 850 hPa and calculates the ELRS based on the difference in potential temperature between 700 hPa and the LCL.
    - This calculation is based on the work of Wood and Bretherton (2006).
    """
    def linear_potential_temperature(z):
        nonlocal pressure, temperature, altitude
        nonlocal i_850
        potential_temperature = metpy.calc.potential_temperature(pressure[i_850], temperature[i_850])
        lapse_rate = approximate_moist_lapse_rate(temperature[i_850], pressure[i_850])
        return lapse_rate * (z - altitude[i_850]) + potential_temperature
    
    p_lcl = lifting_condensation_level_pressure(pressure, temperature, specific_humidity)
    if p_lcl == np.nan: return np.nan # no LCL no party
    i_lcl  = index_of_nearest_value(pressure,  p_lcl)
    i_850  = index_of_nearest_value(pressure,  85000)
    i_700  = index_of_nearest_value(pressure,  70000)
    for index in [i_lcl, i_850, i_700]:
        if index == None: return np.nan
    pressure    = pressure    * metpy.units.units('pascal')
    temperature = temperature * metpy.units.units('kelvin')
    altitude    = altitude    * metpy.units.units('meter')
    eLRS = linear_potential_temperature(altitude[i_700]) - linear_potential_temperature(altitude[i_lcl])
    return eLRS.magnitude

def add_estimated_lapse_rate_stability(dataset, standard_name='elrs', dim='height'):
    """
    Adds the estimated lapse rate stability (ELRS = LTS - EIS) as a new variable to the dataset 
    
    Done for each element along a specified dimension.

    Parameters:
    - dataset: xarray.Dataset
        The dataset containing the atmospheric variables necessary for the calculation.
    - standard_name: str, optional
        The name for the new variable to be added to the dataset. Default is 'elrs'.
    - dim: str, optional
        The name of the dimension along which the ELRS will be calculated. Default is 'height'.

    Returns:
    - xarray.Dataset
        The input dataset with an added variable containing the ELRS values.

    Notes:
    - The function checks for the presence of required variables ('pres', 'temp', 'qv', 'height') and their units.
    """
    for var in ['pres', 'temp', 'qv', 'height']:
        assert var in dataset, f"Variable '{var}' is missing from the dataset."
    assert metpy.units.units(dataset['temp'].attrs['units']).to_base_units() == metpy.units.units('kelvin').to_base_units(), f"Variable 'temp' is not in unit 'kelvin'."
    assert metpy.units.units(dataset['pres'].attrs['units']).to_base_units() == metpy.units.units('pascal').to_base_units(), f"Variable 'pres' is not in unit 'pascal'."
    assert metpy.units.units(dataset['qv'  ].attrs['units']).to_base_units() == metpy.units.units('kg/kg').to_base_units(), f"Variable 'qv' is not in unit 'dimensionless'."
    assert metpy.units.units(dataset['height'].attrs['units']).to_base_units() == metpy.units.units('meter').to_base_units(), f"Variable 'height' is not in unit 'meter'."
    
    dataset[standard_name] = xr.apply_ufunc(
        estimated_lapse_rate_stability,
        dataset["pres"],
        dataset["temp"],
        dataset["qv"],
        dataset["height"],
        input_core_dims=([dim], [dim], [dim], [dim]), # Apply along dim
        vectorize=True,  # Enable vectorized execution
        dask="parallelized",  # allow dask to work in parallel
        output_dtypes=[float],
        dask_gufunc_kwargs={'allow_rechunk': True} # so the whole profile can be forwarded at once
    )
    
    attributes = {'standard_name':standard_name, 'long_name':'estimated lapse rate stability', 'units':'K'}
    dataset[standard_name] = dataset[standard_name].assign_attrs(attributes)
    return dataset


def add_cloud_top_height(dataset, standard_name='cth', dim='height', threshold=1e-5):
    """
    Adds the cloud top height (CTH) as a new variable to the dataset based on cloud water content.

    The cloud top height is determined as the highest altitude where the cloud water content exceeds a threshold (1e-5 kg/kg).

    Parameters:
    - dataset : xarray.Dataset
        The dataset containing the cloud water content ('qc') and altitude ('height') variables.
    - standard_name : str, optional
        The name for the new variable to be added to the dataset. Default is 'cth'.
    - dim : str, optional
        The name of the dimension along which the cloud top height will be calculated. Default is 'height'.
    - threshold : float, optional
        The threshold for cloud top detection. Default is 0.00001.
    
    Returns:
    - xarray.Dataset
        The input dataset with an added variable containing the cloud top height values.

    Notes:
    - The function assumes the 'height' dimension is ordered from top to bottom.
    - Cloud water content values below 1e-5 kg/kg are considered non-cloudy.
    """
    def cloud_top_height(cloud_water, altitude):
        nonlocal threshold
        cloud = cloud_water > threshold
        cloud_height = np.argmax(cloud, axis=0)
        ground_level = len(altitude) -1 # assuming top to bottom order
        if cloud_height == 0: cloud_height = ground_level
        cloud_height = altitude[cloud_height]
        return cloud_height
    
    for var in ['qc', 'height']:
        assert var in dataset, f"Variable '{var}' is missing from the dataset."
    assert metpy.units.units(dataset['qc'  ].attrs['units']).to_base_units() == metpy.units.units('kg/kg').to_base_units(), f"Variable 'qv' is not in unit 'dimensionless'."
    # assert metpy.units.units(dataset['height'].attrs['units']).to_base_units() == metpy.units.units('meter').to_base_units(), f"Variable 'height' is not in unit 'meter'."
    
    dataset[standard_name] = xr.apply_ufunc(
        cloud_top_height,
        dataset["qc"],
        dataset["height"],
        input_core_dims=([dim], [dim]), # Apply along dim
        vectorize=True,  # Enable vectorized execution
        dask="parallelized",  # allow dask to work in parallel
        output_dtypes=[float],
        dask_gufunc_kwargs={'allow_rechunk': True} # so the whole profile can be forwarded at once
    )
    
    attributes = {'standard_name':standard_name, 'long_name':'cloud top height level', 'units':'1'}
    dataset[standard_name] = dataset[standard_name].assign_attrs(attributes)
    return dataset





#########################################################
###    Functions below this line miss a docstring:    ###
#########################################################

def construct_volume(column, shape):
    levels = [np.ones(shape) * value for value in np.flip(column)]
    return np.asarray(levels)

def warm_atmospheric_profile_RHfixed(ifile, ofile, warming_profile, varnames=None):
    d = xr.open_dataset(ifile)
    d = add_relative_humidity(d, varnames=varnames)
    d = add_to_temperature(d, warming_profile, varnames=varnames)
    d = restore_specific_humidity(d, drop_RH=True, varnames=varnames)
    d.to_netcdf(ofile)

# outdated version. please replace in your notebook
def _delta_adiabatic_profile(t_high, t_low, z_full, p_surf=100000):
    p = hydrostatic_profile(np.flip(z_full), p_surf)
    p = np.flip(p) # no flipping in new version
    delta = joined_profile(p, T_s=t_high) - joined_profile(p, T_s=t_low)
    return delta
def delta_adiabatic_profile(t_high, t_low, heights, p_surf=100000):
    p = hydrostatic_profile(heights, p_surf)
    delta = joined_profile(p, T_s=t_high) - joined_profile(p, T_s=t_low)
    return delta

# alternative names
def restore_pressure(dataset, varnames=None, target_name=False, density_name='rho_pre'):
    varnames = add_varnames_to_default(varnames)
    varnames['density'] = density_name
    return add_pressure_from_ideal_gas(dataset, varnames=varnames, target_name=target_name, drop_density=True)
def save_density(dataset, varnames=None, target_name='rho_pre'):
    return add_density_from_ideal_gas(dataset, varnames=varnames, target_name=target_name)

def add_to_temperature(dataset, delta, varnames=None):
    varnames = add_varnames_to_default(varnames)
    delta = np.flip(delta) # ICON atmosphere levels are indexed from top to bottom
    # try:    p = np.flip(dataset[varnames['pressure']].mean(dim='ncells').values)
    # except: p = np.flip(dataset[varnames['pressure']].values)
    try:    temp_delta = construct_volume(delta, dataset.ncells.shape)
    except: temp_delta = delta
    dataset[varnames['temperature']].values = dataset[varnames['temperature']].values + temp_delta
    return dataset

def apply_warming_rho_fixed(dataset, varnames=None):
    varnames = add_varnames_to_default(varnames)
    dataset_1D = dataset
    if 'time' in dataset_1D.dims: dataset_1D = dataset_1D.isel(time=0)
    if 'ncells' in dataset_1D.dims: dataset_1D = dataset_1D.median(dim='ncells')
    if 'cell' in dataset_1D.dims: dataset_1D = dataset_1D.median(dim='cell')
    heights = dataset_1D[varnames['half level height']]
    heights = interpolate_full_level(heights)
    heights = np.flip(heights)
    warming_profile = delta_adiabatic_profile(304, 300, heights, p_surf=100000)
    # warming_profile = np.flip(warming_profile)

    dataset = save_density(             dataset, varnames=varnames)
    dataset = add_relative_humidity(    dataset, varnames=varnames)
    dataset = add_to_temperature(       dataset, warming_profile, varnames=varnames)
    dataset = restore_pressure(         dataset, varnames=varnames)
    dataset = restore_specific_humidity(dataset, varnames=varnames, drop_RH=True)
    return dataset

def apply_warming_p_fixed(dataset, varnames=None):
    varnames = add_varnames_to_default(varnames)
    dataset_1D = dataset
    if 'time' in dataset_1D.dims: dataset_1D = dataset_1D.isel(time=0)
    if 'ncells' in dataset_1D.dims: dataset_1D = dataset_1D.median(dim='ncells')
    if 'cell' in dataset_1D.dims: dataset_1D = dataset_1D.median(dim='cell')
    heights = dataset_1D[varnames['half level height']]
    heights = interpolate_full_level(heights)
    heights = np.flip(heights)
    warming_profile = delta_adiabatic_profile(304, 300, heights, p_surf=100000)
    warming_profile = np.flip(warming_profile)
    
    dataset = add_relative_humidity(    dataset, varnames=varnames)
    dataset = add_to_temperature(       dataset, warming_profile, varnames=varnames)
    dataset = restore_specific_humidity(dataset, varnames=varnames, drop_RH=True)
    return dataset

def apply_uniform_warming_p_fixed(dataset, varnames=None):
    varnames = add_varnames_to_default(varnames)
    dataset_1D = dataset
    if 'time' in dataset_1D.dims: dataset_1D = dataset_1D.isel(time=0)
    if 'ncells' in dataset_1D.dims: dataset_1D = dataset_1D.median(dim='ncells')
    if 'cell' in dataset_1D.dims: dataset_1D = dataset_1D.median(dim='cell')
    heights = dataset_1D[varnames['half level height']]
    heights = interpolate_full_level(heights)
    warming_profile = (heights * 0) + 4 
    
    dataset = add_relative_humidity(    dataset, varnames=varnames)
    dataset = add_to_temperature(       dataset, warming_profile, varnames=varnames)
    dataset = restore_specific_humidity(dataset, varnames=varnames, drop_RH=True)
    return dataset

def apply_uniform_warming_rho_fixed(dataset, varnames=None):
    varnames = add_varnames_to_default(varnames)
    dataset_1D = dataset
    if 'time' in dataset_1D.dims: dataset_1D = dataset_1D.isel(time=0)
    if 'ncells' in dataset_1D.dims: dataset_1D = dataset_1D.median(dim='ncells')
    if 'cell' in dataset_1D.dims: dataset_1D = dataset_1D.median(dim='cell')
    heights = dataset_1D[varnames['half level height']]
    heights = interpolate_full_level(heights)
    heights = np.flip(heights)
    warming_profile = (heights * 0) + 4 

    dataset = save_density(             dataset, varnames=varnames)
    dataset = add_relative_humidity(    dataset, varnames=varnames)
    dataset = add_to_temperature(       dataset, warming_profile, varnames=varnames)
    dataset = restore_pressure(         dataset, varnames=varnames)
    dataset = restore_specific_humidity(dataset, varnames=varnames, drop_RH=True)
    return dataset
    
#########################################################
###              Function graveyard                   ###
#########################################################

# def warm_atmospheric_profile(ifile,ofile,warming_profile,varnames=None):
#     d = xr.open_dataset(ifile)
#     d = add_relative_humidity(d, varnames=varnames)
#     d = add_to_temperature(d, warming_profile, varnames=varnames)
#     d = restore_specific_humidity(d, drop_RH=True, varnames=varnames)
#     d.to_netcdf(ofile)

# def delta_adiabatic_profile(t_high, t_low, file_for_z):
#     d = xr.open_dataset(file_for_z)
#     z_ifc = np.flip(d['z_ifc'].isel(time=0,ncells=0).values)
#     z_full = interpolate_full_level(z_ifc)
    
#     p_surf = 100000 # np.ravel(d['pres'].isel(time=0,ncells=0).values)[-1]
#     p = hydrostatic_profile(z_full, p_surf)
#     delta = joined_profile(p, T_s=t_high) - joined_profile(p, T_s=t_low)
#     return delta