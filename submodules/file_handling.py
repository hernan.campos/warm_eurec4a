"""
Utility Module for Data Processing and Job Management

This module provides a collection of utility functions for various tasks, including handling files and directories,
calculating hash values, generating SLURM job headers, and interacting with SLURM job schedulers. It also includes a
custom catalog class for organizing and accessing datasets and functions to load data based on keywords and match file
extensions.

Functions:
- all_module_files(directory, suffix): Load the text content of all source files in a directory.
- archive_file(file_path, save_directory): Archives a file to a specified directory.
- basename(path, extension, include_extension): Get the base name of a file path with or without the file extension.
- beep(fr, sec): Play a sound alarm to notify when a cell has finished execution (I get distracted).
- check_queue(user): Retrieves job queue information for a specified user using the 'squeue' command.
- count_elements(dataset): Calculate the total number of elements in an xarray dataset.
- create_directory_structure(file_list, out_directory): Replicate the directory structure of files in 'file_list' in 'out_directory.
- dirname(path): Get the directory name of a file path.
- disable_warnings(): Disable warnings issued by Python's 'warnings' module.
- disk_data_from_keywords(directory, keywords, exclude, return_file_list, supress_warning): Load data from disk files in a directory based on matching keywords.
- eureca_data_from_keywords(keywords): Load EUREC4A simulation data based on matching keywords.
- extension(path, include_dot): Get the file extension of a file path.
- find_matching_lines(file_path, keyword): Search for lines in a text file that contain a specified keyword.
- gb(byte): Convert bytes to gigabytes (GB).
- get_hash(string): Compute the SHA-1 hash of a given string and return it as a hexadecimal string.
- icontime2numpytime(timefloat): Depreceated function to convert dates
- image_display_snippet(): Generates a code snippet for a workaround to display images with relative paths.
- is_netcdf(f): Check if a file has the '.nc' extension, indicating it is a NetCDF file.
- launch_slurm_script(slurm_script): Launches a SLURM script using the `subprocess` module and SLURM's `sbatch` command.
- list_files(directory_path, keywords, full_path): Recursively list files within a directory, optionally filtering by keywords.
- list_slurm_jobs(user_id, name): Retrieve a list of SLURM jobs for a specified user and job name.
- list_src_files(directory, suffix): List source files with a specified file extension in a directory.
- load_text_files(file_list): Load and concatenate text files from a list into a single text string.
- log_efficiency_analysis(logfile, verbose): Analyze efficiency metrics from a log file and print the results.
- match(string, keywords): Check if a string contains all the specified keywords.
- minutes2clockstring(m): Convert a duration in minutes to a clock-style string (HH:MM:SS).
- print_function_descriptions(module_path, deprecation_marker, internal_marker): Prints function names and their descriptions from a Python module.
- print_nested_dict(dictionary, method): Print a nested dictionary making the hirarchy visible
- read_file(file_path): Reads a text file and returns a list of its lines.
- scan_files(files, searchstring, max_line_length, verbose): Scan files in a directory for a given search string.
- scancel_by_name(name): Cancel SLURM jobs by their job name.
- seconds2clockstring(s): Convert a duration in seconds to a clock-style string (HH:MM:SS).
- slurm_header(minutes, binary_path, partition, account, email, log_path, return_string): Generate a SLURM job header for job submission.
- some_module_files(files): Load the text content of selected source files.
- sublist(inlist, n_lists, index): Extract a sublist from the input list by splitting it into multiple sublists.
- tb(byte): Convert bytes to terrabytes (TB) by dividing by 1e12.
- timestamp(date, formatstring): Generate a formatted timestamp string from a given date.

Please refer to the individual function docstrings for detailed descriptions and usage instructions.

Author: Hernan Campos
Email:  hernan.campos@mpimet.mpg.de
"""

import os
import re
import warnings
import subprocess

import ast
import inspect
import json
import datetime

import numpy as np
import eurec4a
import xarray as xr
import pandas as pd

def archive_file(file_path, save_directory='./data/'):
    '''
    Archives a file to a specified directory.

    Args:
    - file_path (str): The path to the file to be archived.
    - save_directory (str): The directory path to save the file. Defaults to './data/'.

    Raises:
    - Warning: If the file size is larger than 10MB.
    '''
    # Get the file size
    file_size = os.path.getsize(file_path)
    if file_size > 10 * 1024 * 1024:  # 10 MB in bytes
        warnings.warn("File size is larger than 10MB.")
    
    # Create the save directory if it doesn't exist
    if not os.path.exists(save_directory):
        os.makedirs(save_directory)
    
    # Extracting file name from the path
    file_name = os.path.basename(file_path)
    destination = os.path.join(save_directory, file_name)
    
    # Copy the file to the save directory
    with open(file_path, 'rb') as fsrc, open(destination, 'wb') as fdst:
        fdst.write(fsrc.read())

def read_file(file_path):
    '''
    Reads a text file and returns a list of its lines.

    Args:
    - file_path (str): The path to the text file to be read.

    Returns:
    - list: A list containing lines from the text file.
    '''
    with open(file_path, 'r') as file:
        lines = file.readlines()
    return lines

def disable_warnings(verbose=True):
    """
    Disable warnings issued by Python's 'warnings' module.

    This function suppresses all warning messages generated by Python's 'warnings'
    module. It can be useful when you want to suppress warnings that are printed to
    the console during program execution.

    Example:
        >>> disable_warnings()
        # Any code that generates warnings will no longer print them to the console.

    Note:
        - Use this function with caution, as it disables all warnings globally in your
          Python program. Make sure you are aware of the consequences and only use it
          when you are confident that warnings can safely be ignored.
    """
    warnings.filterwarnings('ignore')
    if verbose: print('Any code that generates warnings will no longer print them to the console.')
    
    
def get_hash(string):
    """
    Compute the SHA-1 hash of a given string and return it as a hexadecimal string.

    This function takes a string as input and computes the SHA-1 hash (Secure Hash
    Algorithm 1) of the input string. The result is returned as a hexadecimal string
    representing the hash value.
    
    See: https://www.pythoncentral.io/hashing-strings-with-python/

    Args:
        string (str): The input string to be hashed.

    Returns:
        str: A hexadecimal string representing the SHA-1 hash of the input string.

    Example:
        >>> input_string = "Hello, world!"
        >>> hash_result = get_hash(input_string)
        >>> print(hash_result)
        '2ef7bde608ce5404e97d5f042f95f89f1c8d0f0124'

    Note:
        - SHA-1 is no longer considered secure for cryptographic purposes and is deprecated
          due to vulnerabilities. For security-critical applications, consider using a
          stronger hashing algorithm, such as SHA-256 or SHA-512.
    """
    hash_object = hashlib.sha1(string.encode())
    return hash_object.hexdigest()


def basename(path, extension=None, include_extension=None):
    """
    Get the base name of a file path with or without the file extension.

    Args:
        path (str): The file path to extract the base name from.
        extension (bool, optional): Deprecated. Use 'include_extension' instead.
        include_extension (bool, optional): Set to True (default) to include the file
            extension in the result, or False to exclude it.

    Returns:
        str: The base name of the file path.

    Example:
        >>> path = "/path/to/file.txt"
        >>> with_extension = basename(path)
        >>> without_extension = basename(path, include_extension=False)
        >>> print(with_extension)
        'file.txt'
        >>> print(without_extension)
        'file'
    """
    if extension is not None:
        include_extension = extension
        warnings.warn("The 'extension' parameter is deprecated. Use 'include_extension' instead.", DeprecationWarning)

    base_name = os.path.basename(path)
    if not include_extension:
        base_name = os.path.splitext(base_name)[0]
    return base_name
    
    
def dirname(path):
    """
    Get the directory name of a file path.

    Args:
        path (str): The file path from which to extract the directory name.

    Returns:
        str: The directory name.

    Example:
        >>> path = "/path/to/file.txt"
        >>> directory = dirname(path)
        >>> print(directory)
        '/path/to'
    """
    return os.path.dirname(path)


def extension(path, include_dot=True):
    """
    Get the file extension of a file path.

    Args:
        path (str): The file path from which to extract the file extension.

    Returns:
        str: The file extension.

    Example:
        >>> path = "/path/to/file.txt"
        >>> ext = extension(path)
        >>> print(ext)
        'txt'
    """
    ext = os.path.splitext(path)[1]
    if not include_dot:
        ext = ext.lstrip(".")
    return ext


def is_netcdf(f):
    """
    Check if a file has the '.nc' extension, indicating it is a NetCDF file.

    Args:
        f (str): The file path to check for a '.nc' extension.

    Returns:
        bool: True if the file has the '.nc' extension, indicating it is a NetCDF file;
            False otherwise.

    Example:
        >>> file_path = "data.nc"
        >>> result = is_netcdf(file_path)
        >>> print(result)
        True

    Note:
        - This function is a simple check to determine if a file is a NetCDF file based on
          its extension. It does not validate the file's content.
    """
    return extension(f) == '.nc'



def list_files(directory_path, keywords=None, full_path=True):
    """
    Recursively list files within a directory, optionally filtering by keywords.

    Args:
        directory_path (str): The directory path to search for files.
        keywords (list of str, optional): A list of keywords to filter files.
        full_path (bool, optional): If True, return full file paths (default: False).

    Returns:
        list of str: List of matched file paths based on the specified options.
        
    Note:
        - The function performs a case-insensitive search for keywords in file names.
    """
    if keywords is None: keywords = []    
    matched_files = list()
   
    for root, _, files in os.walk(directory_path):
        for file_name in files:
            if keywords == [] or all(keyword.lower() in file_name.lower() for keyword in keywords):
                path = os.path.join(os.path.relpath(root, start=directory_path), file_name)
                if   full_path:     path = os.path.join(directory_path, path)
                matched_files.append(path)
    return sorted(matched_files)



def get_filelist(lost='/', conserved='./', keyword=None, keywords=None):
    '''
    deprecated
    
    crawls the directory `lost_path`+`conserved_path` and returns a list of files in all subdirectories
    This function is depreceated. Use list_files instead.
    '''
    warnings.warn("The 'get_filelist' function is deprecated. Use 'list_files' instead.", DeprecationWarning)
    file_list = []
    for root, directories,files in os.walk(os.path.join(lost, conserved)):
        for name in files: 
            if is_netcdf(name):
                path = os.path.join(os.path.relpath(root, start=lost), name)
                path = os.path.abspath(path)
                file_list.append(path)
    if not keyword == None: file_list = [f for f in file_list if keyword in f]
    if not keywords == None: 
        for kw in keywords: file_list = [f for f in file_list if kw in f]
    file_list.sort()
    return file_list


def n_elements(dataset):
    """
    deprecated
    
    Use 'count_elements' instead.
    """
    warnings.warn("The 'n_elements' function is deprecated. Use 'count_elements' instead.", DeprecationWarning)
    return count_elements(dataset)

def count_elements(dataset):
    """
    Calculate the total number of elements in an xarray dataset.

    This function takes an xarray dataset as input and calculates the total number of
    elements within the dataset. Each variable's size (number of elements) is computed,
    and the function returns the sum of all variable sizes, representing the total
    number of elements in the dataset.

    Args:
        dataset (xarray.Dataset): An xarray dataset containing variables.

    Returns:
        int: The total number of elements in the dataset.

    Example:
        >>> import xarray as xr
        >>> data = xr.Dataset({'var1': [1, 2, 3], 'var2': [4, 5, 6]})
        >>> total_elements = count_elements(data)
        >>> print(total_elements)
        6

    Note:
        - The function assumes that the input 'dataset' is an xarray dataset.
        - The total number of elements is calculated by summing the sizes of all variables
          within the dataset.
    """
    variable_sizes = [dataset[variable_name].size for variable_name in dataset]
    total_elements = np.sum(variable_sizes)
    return total_elements


def gb(byte):
    """
    Convert bytes to gigabytes (GB).

    This function takes a value in bytes and converts it to gigabytes (GB) by dividing
    the input value by 1e9.

    Args:
        byte (float or int): The value in bytes to be converted to gigabytes.

    Returns:
        float: The equivalent value in gigabytes (GB).

    Note:
        - The function assumes that the input 'byte' is a non-negative numeric value.
    """
    return byte / 1e9
def tb(byte):
    """
    Convert bytes to terrabytes (TB) by dividing by 1e12.
    """
    return byte / 1e12

def icontime2numpytime(timefloat):
    """
    Depreceated function to convert dates

    Please use `postprocessing.icon2datetime()` instead.
    """
    warnings.warn("The 'icontime2numpytime' function is deprecated. Use postprocessing.icon2datetime() instead.", DeprecationWarning)
    date_string = str(int(timefloat))
    h_ = 24 * (timefloat - int(timefloat))
    h  = int(h_)
    m_ = 60 * (h_ - h)
    m  = int(np.round(m_))
    if m == 60: m = 0; h += 1
    return np.datetime64(f'{date_string[0:4]}-{date_string[4:6]}-{date_string[6:8]}T{str(h).rjust(2,"0")}:{str(m).rjust(2,"0")}')


def create_directory_structure(file_list, out_directory):
    """
    Replicate the directory structure of files in 'file_list' in 'out_directory.

    This function takes a list of file paths in 'file_list' and creates a matching directory
    structure in the 'out_directory'. Each file's directory path is replicated within 'out_directory,
    ensuring that the file structure is preserved.

    Args:
        file_list (list of str): A list of file paths that define the source directory structure to replicate.
        out_directory (str): The destination directory where the structure will be created.

    Returns:
        None

    Example:
        >>> file_list = ['/source/dir1/file1.txt', '/source/dir2/file2.txt']
        >>> out_directory = '/destination'
        >>> create_directory_structure(file_list, out_directory)

    Note:
        - The 'file_list' parameter should be a list of file paths with corresponding directory structure
          or obtained using 'get_filelist(lost, conserved)'.
        - The function ensures that the directory structure is replicated within 'out_directory.'
        - Existing directories are not overwritten, thanks to 'os.makedirs' with 'exist_ok=True.'
    """
    for f in file_list:
        dirpath = os.path.join(out_directory, os.path.dirname(f))
        os.makedirs(dirpath, exist_ok=True)

  

def sublist(inlist, n_lists, index):
    """
    Extract a sublist from the input list by splitting it into multiple sublists.

    This function takes an input list 'inlist' and divides it into 'n_lists' sublists.
    It then returns the sublist at the specified 'index' position. This can be useful for
    partitioning a large list into smaller segments and retrieving a specific part.

    Args:
        inlist (list): The input list to be divided into sublists.
        n_lists (int): The number of sublists to create from 'inlist'.
        index (int): The index of the desired sublist to extract (0-based).

    Returns:
        list: A sublist from the input list based on the 'index' value.

    Example:
        >>> input_list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        >>> num_sublists = 3
        >>> sublist_index = 1
        >>> result = sublist(input_list, num_sublists, sublist_index)
        >>> print(result)
        [4, 5]

    Note:
        - The function uses 'np.array_split' to evenly split 'inlist' into 'n_lists' sublists.
        - 'index' determines which sublist is returned (0 for the first sublist, 1 for the second, and so on).
    """
    return list(np.array_split(inlist, n_lists)[index])


from IPython.lib.display import Audio
def beep(fr=4410, sec=1.0/7):
    """
    Play a sound alarm to notify when a cell has finished execution (I get distracted).

    The audio data for the beep is generated by superimposing two sine waves with frequencies
    of 300 Hz and 240 Hz for a specified duration.

    Args:
        fr (int, optional): The audio sampling rate in Hz (default is 4410).
        sec (float, optional): The duration of the beep in seconds (default is 1/7 seconds).

    Returns:
        None

    Example:
        >>> beep()  # Plays the default beep sound.
        >>> beep(fr=2205, sec=0.5)  # Plays a custom beep with different sampling rate and duration.

    Notes:
        - The function uses 'IPython.lib.display.Audio' to play the generated audio.
        - The code for generating the beep sound was adapted from a Gist by tamsanh 
          (https://gist.github.com/tamsanh/a658c1b29b8cba7d782a8b3aed685a24) and packed into
          a function (with help from here: https://stackoverflow.com/a/64139240)
    """
    t = np.linspace(0, sec*2, int(fr*sec))
    audio_data = np.sin(2*np.pi*300*t) + np.sin(2*np.pi*240*t)
    display(Audio(audio_data, rate=fr, autoplay=True))

    

class MinIntake:
    """
    A depreceated custom catalog class for organizing and accessing datasets.
    
    Use CustomCatalog instead.
    """
    def __init__(self):
        warnings.warn("The 'MinIntake' class is deprecated. Use CustomCatalog instead.", DeprecationWarning)
    def __catalog__(self):
        return [s for s in dir(self) if not s.startswith('__')]
    def __getitem__(self, item):
        return self.__catalog__()[item]
    pass

class CustomCatalog:
    """
    A custom catalog class for organizing and accessing datasets.

    This class provides a tree-like structure for organizing datasets,
    allowing users to access data using dot notation or dictionary-like access.

    Attributes:
        data (dict): A dictionary to store datasets organized in a tree structure.
        metadata (dict): A dictionary to store metadata associated with the datasets in data.

    Methods:
        __init__(): Initialize an empty CustomCatalog object.
        
        add_dataset(self, name, data, metadata=None): Add a dataset to the catalog.
        
        get_metadata(self, name): Get metadata about a dataset or object.

        __getitem__(key): Allow dictionary-like access to datasets.

        __getattr__(name): Allow access via dot notation.

        __iter__(): Allow listing of keys at the current level using list().

    Examples:
        # Initialize the catalog
        catalog = CustomCatalog()

        # Add datasets to the catalog
        data1 = [1, 2, 3]
        metadata1 = {"description": "Sample data 1"}
        catalog.add_dataset("dataset1", data1, metadata1)

        data2 = {"name": "John", "age": 30}
        metadata2 = {"description": "Sample data 2"}
        catalog.add_dataset("dataset2", data2, metadata2)

        # List available datasets
        print(list(catalog))

        # Access datasets and metadata
        for key in ["dataset1", "dataset2"]:
            dataset = catalog[key]
            metadata = catalog.get_metadata(key)
            print(key, dataset, metadata)
    """
    def __init__(self):
        self.data = {}  # Store data in a dictionary
        self.metadata = {}  # Store metadata about the datasets

    def add_dataset(self, name, data, metadata=None):
        """
        Add a dataset to the catalog.

        Args:
            name (str): A unique name for the dataset.
            data: The dataset or object to be added.
            metadata (dict, optional): Metadata about the dataset.
        """
        self.data[name] = data
        if metadata:
            self.metadata[name] = metadata
            
    def get_metadata(self, name):
        """
        Get metadata about a dataset or object.

        Args:
            name (str): The name of the dataset or object.

        Returns:
            Metadata associated with the specified name, or None if no metadata is available.
        """
        return self.metadata.get(name, None)

    def __getitem__(self, item):
        return self.data[item]

    def __getattr__(self, name):
        return self.data[name]#, self.metadata[name]

    def __iter__(self):
        return iter(self.data)

    def __catalog__(self):
        return self.data
    
    def __str__(self):
        content = list(self)
        if content == list():
            return f"empty CustomCatalog"
        else:
            return f"CustomCatalog with ({list(self)})"
        
    def __repr__(self): 
        return str(self)


def match(string, keywords):
    """
    Check if a string contains all the specified keywords.

    This function checks whether a given 'string' contains all of the 'keywords' specified in the 'keywords' list.
    It returns 'True' if the 'string' includes all the 'keywords', and 'False' otherwise.

    Args:
        string (str): The input string to be examined for the presence of keywords.
        keywords (list): A list of keywords to check for in the 'string'.

    Returns:
        bool: 'True' if all 'keywords' are present in the 'string'; 'False' otherwise.

    Example:
        >>> text = "This is an example sentence."
        >>> keyword_list = ["example", "sentence"]
        >>> result = match(text, keyword_list)
        >>> print(result)
        True

    Note:
        - The function performs a case-sensitive check for the keywords in the string.
        - The order of keywords in the 'keywords' list does not matter; all must be present.
    """
    checklist = [k in string for k in keywords]
    return all(checklist)


def load_text_files(file_list):
    """
    Load and concatenate text files from a list into a single text string.

    This function takes a list of file paths in 'file_list' and loads the content of each text file,
    concatenating them into a single text string. Each file's content is separated by comment lines
    indicating the original file name.

    Args:
        file_list (list of str): A list of file paths to be loaded and concatenated.

    Returns:
        str: A text string containing the concatenated content of all text files.

    Example:
        >>> files_to_load = ["file1.txt", "file2.txt", "file3.txt"]
        >>> concatenated_text = load_text_files(files_to_load)
        >>> print(concatenated_text)

    Note:
        - Each file's content is separated by comment lines that indicate the original file name.
        - The resulting text string includes extra line breaks for clear separation.
    """
    data = []
    for f in file_list:
        data.append(f'# {f}\n')
        with open(f, 'r') as file:
            data.extend(file.readlines())
        data.extend(['\n\n\n'])
    return ''.join(data)


def seconds2clockstring(s):
    """
    Convert a duration in seconds to a clock-style string (HH:MM:SS).

    This function takes a time duration in seconds and converts it to a clock-style
    string representation in the format 'HH:MM:SS'.

    Args:
        s (float or int): The duration in seconds to be converted.

    Returns:
        str: A clock-style string representation of the input duration.

    Example:
        >>> duration_in_seconds = 3665  # 1 hour, 1 minute, and 5 seconds
        >>> clock_string = seconds2clockstring(duration_in_seconds)
        >>> print(clock_string)
        '01:01:05'
    """
    hours, remainder = divmod(s, 3600)
    minutes, seconds = divmod(remainder, 60)
    return '{:02}:{:02}:{:02}'.format(int(hours), int(minutes), int(seconds))


def minutes2clockstring(m):
    """
    Convert a duration in minutes to a clock-style string (HH:MM:SS).

    This function takes a time duration in minutes and converts it to a clock-style
    string representation in the format 'HH:MM:SS'.

    Args:
        m (float or int): The duration in minutes to be converted.

    Returns:
        str: A clock-style string representation of the input duration.

    Example:
        >>> duration_in_minutes = 75  # 1 hour and 15 minutes
        >>> clock_string = minutes2clockstring(duration_in_minutes)
        >>> print(clock_string)
        '01:15:00'

    Note:
        - This function internally uses 'seconds2clockstring' for the conversion.
    """
    s = pd.Timedelta(m, 'm').total_seconds()
    return seconds2clockstring(s)


def slurm_header(minutes=10, binary_path='/sw/spack-levante/mambaforge-22.9.0-2-Linux-x86_64-kptncg/bin/python3', job_name='minion',
                 partition='compute', account='mh1126', email='hernan.campos@mpimet.mpg.de', log_path='/scratch/m/m300872/log/', return_string=False):
    """
    Generate a SLURM job header for job submission.

    This function generates a SLURM (Simple Linux Utility for Resource Management) job header script for job submission 
    on a high-performance computing (HPC) cluster. The header includes directives such as the binary path, partition, 
    account, job duration, email notifications, and output file configuration.

    Args:
        minutes (int): The estimated job duration in minutes.
        binary_path (str, optional): The path to the Python binary to be used (default is provided path).
        job_name (str, optional) name of the job. e.g. useful for canceling via `scancel_by_name` (default is "minion").
        partition (str, optional): The SLURM partition to allocate resources (default is 'compute').
        account (str, optional): The SLURM account to charge resources (default is 'mh1126').
        email (str, optional): The email address for job notifications (default is 'hernan.campos@mpimet.mpg.de').
        log_path (str, optional): An optional path where logfiles should be written (default is on levantes scratch partition)

    Returns:
        str: A string containing the SLURM job header script.

    Note:
        - SLURM is a job scheduling system used on high-performance computing clusters.
        - The 'python_path' and 'script_name' variables should be defined before calling this function.
    """
    lines = [
        f"#!{binary_path}",
        f"#SBATCH --job-name={job_name}",
        f"#SBATCH --partition={partition}",
        f"#SBATCH --account={account}",
        f"#SBATCH --nodes=1",
        f"#SBATCH --time={minutes2clockstring(minutes)}",
        f"#SBATCH --mail-type=fail",
        f"#SBATCH --mail-user={email}",
        f"#SBATCH --output={log_path}%j_%x.log",
        f""
    ]
    if 'python' in binary_path:
        lines += [
            f"import builtins",
            f"def print(*args, **kwargs):",
            f"\t''' for printing into SLURM logs'''",
            f"\tbuiltins.print(*args, **kwargs, flush=True)",
            f"",
        ]
    lines += [f"# THIS SCRIPT WAS AUTOMATICALLY GENERATED\n\n"]

    if return_string: return '\n'.join(lines)
    else:             return lines


def scancel_by_name(name):
    """
    Cancel SLURM jobs by their job name.

    This function cancels SLURM jobs based on their job name. It first retrieves a list of jobs with
    the specified name using 'list_slurm_jobs', and then attempts to cancel each of them using the 'scancel'
    command. If a job is successfully canceled, its job ID is added to the 'canceled_jobs' list.

    Args:
        name (str): The job name to match for job cancellation.

    Returns:
        list: A list of canceled job IDs.

    Example:
        >>> job_name_to_cancel = 'my_job_name'
        >>> canceled_job_ids = scancel_by_name(job_name_to_cancel)
        >>> print(canceled_job_ids)
        ['12345', '67890']

    Note:
        - The function uses 'list_slurm_jobs' to retrieve a list of jobs with the specified name.
        - For each matching job, it attempts to cancel it using 'scancel'.
        - If a job cancellation fails, an error message is printed, but the function continues.
    """
    canceled_jobs = []

    for job in list_slurm_jobs(name=name):
        try:
            subprocess.check_call(['scancel', job['id']])
            canceled_jobs.append(job['id'])
        except subprocess.CalledProcessError as e:
            # Handle the case when scancel fails to cancel a job
            print(f"Failed to cancel job {job['id']}: {e}")

    return canceled_jobs


def list_slurm_jobs(user_id='m300872', name=None):
    """
    Retrieve a list of SLURM jobs for a specified user and job name.

    This function interacts with the SLURM (Simple Linux Utility for Resource Management) job scheduler
    to retrieve a list of jobs submitted by a specific user. It can also filter the jobs based on their
    names.

    Args:
        user_id (str, optional): The user ID for whom to list SLURM jobs (default is 'm300872').
        name (str, optional): A specific job name to filter the jobs (default is None, meaning no filtering).

    Returns:
        list: A list of dictionaries, each representing a SLURM job with attributes 'id', 'partition', and 'name'.

    Example:
        >>> user_id = 'm300872'
        >>> job_list = list_slurm_jobs(user_id=user_id, name='my_job_name')
        >>> print(job_list)
        [{'id': '12345', 'partition': 'compute', 'name': 'my_job_name'}]

    Note:
        - The function uses the 'squeue' command to query SLURM for job information.
        - It extracts and processes job data from the command output.
        - Jobs can be filtered by specifying the 'name' parameter. If 'name' is None, no filtering is applied.
    """
    slurm_output = subprocess.check_output(['squeue', '-o', '"%.18i %.9P %.25j"', '-u', user_id]).decode('ascii')
    slurm_output = slurm_output.strip().split('\n')[1:]

    jobs = []
    for line in slurm_output:
        fields = line[1:-1].split()
        job_id, partition, job_name = fields[0], fields[1], fields[2]

        if name is None or name == job_name:
            jobs.append({'id': job_id, 'partition': partition, 'name': job_name})

    return jobs


def list_src_files(directory='/home/m/m300872/warm_eurec4a/submodules/', suffix='.py'):
    """
    List source files with a specified file extension in a directory.

    This function lists source files within a specified directory that match a given file extension.
    It can be used to identify and gather source code files, such as Python files, in a specific directory.

    Args:
        directory (str, optional): The directory to search for source files (default is a specific directory).
        suffix (str, optional): The file extension to filter source files (default is '.py').

    Returns:
        list: A list of file paths to the identified source files.

    Raises:
        ValueError: If the specified 'directory' does not exist or is not a valid directory.

    Example:
        >>> source_directory = '/path/to/source/files/'
        >>> python_source_files = list_src_files(source_directory, suffix='.py')
        >>> print(python_source_files)
        ['/path/to/source/files/file1.py', '/path/to/source/files/file2.py']

    Note:
        - The function checks the existence and validity of the specified 'directory'.
        - It appends the file extension 'suffix' to file names when generating the list of file paths.
    """
    if not os.path.exists(directory) or not os.path.isdir(directory):
        raise ValueError(f"The specified directory '{directory}' does not exist or is not a valid directory.")

    if not directory.endswith(os.path.sep):
        directory += os.path.sep

    return [os.path.join(directory, f) for f in os.listdir(directory) if f.endswith(suffix)]



def all_module_files(directory='/home/m/m300872/warm_eurec4a/submodules/', suffix='.py'):
    """
    Load the text content of all source files in a directory.

    This function lists all source files with a specified file extension within a directory and
    then loads and concatenates their text content into a single text string using 'load_text_files'.

    Args:
        directory (str, optional): The directory containing source files (default is a specific directory).
        suffix (str, optional): The file extension to filter source files (default is '.py').

    Returns:
        str: A single text string containing the content of all identified source files.

    Note:
        - The function some_module_files provides select modules
    """
    # List all source files in the specified directory.
    source_files = list_src_files(directory, suffix)
    return load_text_files(source_files)



def some_module_files(files=['/home/m/m300872/warm_eurec4a/submodules/file_handling.py',
                             '/home/m/m300872/warm_eurec4a/submodules/calculations.py',
                             '/home/m/m300872/warm_eurec4a/submodules/postprocessing.py']):
    """
    Load the text content of selected source files.

    This function loads and concatenates the text content of a reduced list of source files specified in the 'files' parameter.
    The selection is reduced because not all source files may be needed, and some files may have their main body that
    interferes with their usage in this way.

    Args:
        files (list, optional): A list of file paths to load and concatenate (default is a specific list).

    Returns:
        str: A single text string containing the content of the selected source files.

    Example:
        >>> script = slurm_header() + some_module_files() + script_body
    """
    return load_text_files(files)


def disk_data_from_keywords(directory, keywords, exclude=None, return_file_list=False, supress_warning=False):
    """
    Load data from disk files in a directory based on matching keywords.

    This function filters and loads data from disk files in a specified directory based on matching keywords.
    It can also exclude files that match specific exclude keywords. The data is loaded and returned as an
    xarray dataset.

    Args:
        directory (str): The directory containing data files to be loaded.
        keywords (list): A list of keywords to match data files for loading.
        exclude (list, optional): A list of keywords to exclude files (default is None).
        return_file_list (bool, optional): Whether to return the list of matching file paths (default is False).

    Returns:
        xarray.Dataset or list: An xarray dataset containing loaded data or a list of matching file paths.

    Example:
        >>> data_directory = '/path/to/data/files/'
        >>> data_keywords = ['temperature', 'humidity']
        >>> loaded_data = disk_data_from_keywords(data_directory, data_keywords)
        >>> print(loaded_data)

    Note:
        - The function filters and loads data files that match the provided 'keywords' list.
        - Files can be optionally excluded based on 'exclude' keywords.
        - If 'return_file_list' is True, the function returns a list of matching file paths for debugging purposes.
    """
    matching_files = [f for f in os.listdir(directory) if match(f, keywords)]
    
    if return_file_list: # For debugging purposes, return the list of file paths
        return sorted([os.path.join(directory, f) for f in matching_files])
    if exclude:
        for ex in exclude:
            matching_files = [f for f in matching_files if ex not in f]
    file_paths = [os.path.join(directory, f) for f in matching_files]

    try:
        dataset = xr.open_mfdataset(file_paths, combine='nested', concat_dim='time', chunks={})
    except: 
        # this deals with non monotonic time axis 
        # (double entries created by overlapping output after simulation restarts) 
        dataset = xr.open_mfdataset(file_paths, combine='nested', concat_dim='time', chunks={})
        # remove doubles in the time axis, see postprocessing.open_mfdataset_with_timedouble()
        dataset = dataset.isel(time=np.unique(dataset.time, return_index=True)[1]) 
        if not supress_warning: warnings.warn(
            "There might have been a problem with double output for some time steps. Data was loaded with `concat_dim='time'` and sorted via `dataset.sortby(dataset['time'])`. Check the time axis!"
        )
    dataset = dataset.sortby(dataset['time'])
    _, idx = np.unique(dataset['time'].values, return_index=True)
    dataset = dataset.isel(time=idx)
    return dataset


def eureca_data_from_keywords(keywords):
    """
    Load EUREC4A simulation data based on matching keywords.

    This function loads EUREC4A simulation data using the intake catalog. It selects a dataset based on
    matching keywords and returns the dataset as a Dask array.

    Args:
        keywords (list): A list of keywords to match the desired dataset.

    Returns:
        dask.array: The selected dataset loaded as a Dask array.

    Raises:
        ValueError: If the keywords match more than one dataset or if there is no match for the provided keywords.

    Example:
        >>> selected_keywords = ['surface', 'DOM01']
        >>> eurec4a_data = eureca_data_from_keywords(selected_keywords)
        >>> print(eurec4a_data)

    Note:
        - The function uses the EUREC4A intake catalog to access simulation datasets.
        - It selects a dataset based on matching keywords and loads it as a Dask array.
        - If there are multiple matches, a ValueError is raised.
        - If no match is found, a ValueError is raised.
    """
    cat = eurec4a.get_intake_catalog()['simulations']['ICON']['LES_CampaignDomain_control']
    identifier = [f for f in list(cat) if match(f, keywords)]
    if len(identifier) > 1:
        raise ValueError(f'Keywords match more than one dataset: {identifier}')
    elif len(identifier) < 1:
        raise ValueError('No match for these keywords.')
    else:
        return cat[identifier[0]].to_dask()
    
    
def find_matching_lines(file_path, keyword):
    """
    Search for lines in a text file that contain a specified keyword.

    Args:
        file_path (str): The path to the text file to be searched.
        keyword (str): The keyword to search for within the lines of the file.

    Returns:
        list: A list of lines from the file that contain the specified keyword.
        
    Raises:
        FileNotFoundError: If the specified file cannot be found.
        IOError: If there is an issue reading the file.
    """
    try:
        matching_lines = []
        with open(file_path, 'r') as file:
            for line in file:
                if keyword in line:
                    matching_lines.append(line.strip())  # .strip() to remove newline characters
        return matching_lines
    except FileNotFoundError: raise FileNotFoundError(f"The file '{file_path}' was not found.")
    except IOError:           raise IOError(f"An error occurred while reading the file '{file_path}'.")


def log_efficiency_analysis(logfile, verbose=False):
    """
    Analyze efficiency metrics from a log file and print the results.

    Args:
        logfile (str): Path to the log file.

    Prints:
        Efficiency metrics such as walltime, nodes, simulated time, and simulated seconds per node hour used.
    """
    def get_timestamp(string):
        """Extract timestamp from a string."""
        for word in string.split(' '): 
            try:    timestamp = pd.to_datetime(word)
            except: pass
        return timestamp

    def get_nodes(filename):
        """Extract the number of nodes from a log file."""
        return int(find_matching_lines(filename, '#SBATCH --nodes=')[0].split('=')[-1])

    def get_walltime(filename):
        """Extract the time the script ran from a log file."""
        try: # if the run went through, we read the slurm report at the end
            lines = find_matching_lines(filename, 'Elapsed time')
            lines = lines[0].split(' ')
            for word in lines:
                try:
                    d = pd.Timedelta(word)
                    if type(d) == type(pd.Timedelta('01h01m01s')): return pd.Timedelta(word).total_seconds() / (60*60)
                except: pass
        except: # if a restart is launched there is no slurm timer report at the end of the log
            lines = find_matching_lines(filename, 'total')[-2].split(' ')
            for word in lines:
                if word.endswith('m'):
                    try:
                        d = pd.Timedelta(word)
                        if type(d) == type(pd.Timedelta('01h01m01s')): 
                            return pd.Timedelta(word).total_seconds() / (60*60)
                    except: pass 

    def get_end_date(filename):
        """Extract the simulation time, the run ended on from a log file."""
        line = find_matching_lines(filename, 'model time:')[-1]
        line = line.split('model time:')[-1]
        line = line.split('forecast time')[0]
        try:    date = pd.to_datetime(line)
        except: date = None
        return date

    def get_start_date(filename):
        """Extract the simulation time, the run startet with from a log file."""
        line = find_matching_lines(filename, 'model time:')[0]
        line = line.split('model time:')[-1]
        line = line.split('forecast time')[0]
        try:    date = pd.to_datetime(line)
        except: date = None
        return date

    def get_simulated_time(filename):
        """Extract the total simulated time from a log file."""
        return (get_end_date(filename) -  get_start_date(filename)).total_seconds() / (60*60)

    walltime = get_walltime(logfile)
    nodes = get_nodes(logfile)
    simulated_time = get_simulated_time(logfile)
    spnh = (simulated_time*60*60) / (nodes * walltime)
    if verbose:
        print(f'walltime:       {walltime:4.3f} ')
        print(f'nodes:          {nodes:4.3f} ')
        print(f'simulated time: {simulated_time:4.3f} ')
        print('simulated seconds per node hour used:', spnh )
    return {'walltime':walltime, 'nodes':nodes, 'simulated time':simulated_time, 'simulated seconds per node hour':spnh}


def check_queue(user='m300872'):
    """
    Retrieves job queue information for a specified user using the 'squeue' command.

    Args:
    - user (str, optional): Username for which the job queue information is requested (default is 'm300872').

    Returns:
    - None: The function interacts with the job scheduler to retrieve and display job queue information
      for the specified user using the 'squeue' command. The output is printed to the console.
    """
    query = ['squeue', '-u', user, '-o', '"%.18i %.9P %.25j %.8u %.2t %.10M %.6D %.20R %.20S"']
    subprocess.call(query)


def launch_slurm_script(slurm_script: str) -> int:
    """
    Launches a SLURM script using the `subprocess` module and SLURM's `sbatch` command.

    Parameters:
        slurm_script (str): A string representing the SLURM script.

    Returns:
        int: The job ID assigned by SLURM.

    Raises:
        subprocess.CalledProcessError: If the `sbatch` command fails.
        ValueError: If the input is empty or not a string.

    Example:
        >>> slurm_script = '''
        ... #!/bin/bash
        ... #SBATCH --job-name=my_job
        ... #SBATCH --output=output.txt
        ... #SBATCH --partition=compute
        ... #SBATCH --nodes=1
        ... #SBATCH --ntasks-per-node=1
        ... echo "Hello, SLURM!"
        ... '''
        >>> job_id = launch_slurm_script(slurm_script)
    """
    if not isinstance(slurm_script, str):
        raise ValueError("Input must be a string.")

    if not slurm_script.strip():
        raise ValueError("Input SLURM script is empty.")

    try:
        sbatch_process = subprocess.Popen(['sbatch'], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
        stdout, stderr = sbatch_process.communicate(input=slurm_script)
        if sbatch_process.returncode != 0:
            raise subprocess.CalledProcessError(sbatch_process.returncode, 'sbatch', output=stdout, stderr=stderr)
        
        # Parse SLURM job ID from stdout
        job_id = int(stdout.split()[-1])
        return job_id
        
    except FileNotFoundError:
        raise FileNotFoundError("SLURM sbatch command not found. Make sure SLURM is installed and configured correctly.")


def _print_nested_dict_ascii(dictionary, indent=0, prefix=''):
    """
    Print the hierarchical structure of a nested dictionary with ASCII lines.

    Parameters:
        dictionary (dict): The nested dictionary to visualize.
        indent (int): The current level of indentation.
        prefix (str): The prefix for the ASCII lines.
    """
    keys = list(dictionary.keys())
    for i, key in enumerate(keys):
        is_last = i == len(keys) - 1
        print(prefix + '└── ' if is_last else prefix + '├── ', end='')
        print(f'{key}:')
        if isinstance(dictionary[key], dict):
            print_nested_dict_ascii(dictionary[key], indent + 1, prefix + ('    ' if is_last else '│   '))
            
        else:
            print(prefix + '    ' if is_last else prefix + '│   ', end='')
            print(dictionary[key])
            
def _print_nested_dicts_json(dictionary):
    """
    print the dictionary in JSON formatingspaces.

    Parameters:
    nested_dict (dict): The nested dictionary to be serialized into JSON format.

    Returns:
    None
    """
    print(json.dumps(dictionary, indent=4))
    
def print_nested_dict(dictionary, method='ascii'):
    """
    Print a nested dictionary making the hirarchy visible
    
    This defaults to ASCII based printout. A JSON based view is also available

    Parameters:
    nested_dict (dict): The nested dictionary to be printed.
    method (str): The method used, defaults to 'ascii' other input will be interpreted as 'json'

    Returns:
    None
    """
    if method == 'ascii': _print_nested_dict_ascii(dictionary, indent=0, prefix='')
    else: _print_nested_dicts_json(dictionary)
    

def _extract_function_short_info(module_path, deprecation_marker='deprecated', internal_marker='_'):
    """
    Extracts function names, parameters, and their first paragraph docstrings from a Python module.
    
    excludes functions marked as deprecated.

    Parameters:
    - module_path (str):                  The path to the Python module file.
    - deprecation_marker (str, optional): Functions are excluded if the short description is equal to this marker. 
                                          Can be disabled by setting to None. Defaults to 'deprecated'
    - internal_marker (str, optional):    The string marking internal functions. Defaults to "_".


    Returns:
    - dict: A dictionary mapping function names (with parameters) to their first paragraph docstrings,
            sorted by function names, excluding deprecated functions.
    """
    with open(module_path, 'r') as file:
        module_source = file.read()

    module_ast = ast.parse(module_source)
    function_info = {}

    for node in module_ast.body:
        if isinstance(node, ast.FunctionDef):
            function_name = node.name
            parameters = [arg.arg for arg in node.args.args]
            function_signature = f"{function_name}({', '.join(parameters)})"
            function_docstring = ast.get_docstring(node)
            if function_docstring:
                first_paragraph = inspect.cleandoc(function_docstring).split('\n\n')[0]
            else:
                first_paragraph = "-- No docstring available. --"

            # Exclude deprecated functions
            if (not deprecation_marker == None and first_paragraph.lower() != deprecation_marker) and (not function_name.startswith(internal_marker)):
                function_info[function_signature] = first_paragraph

    # Sort the dictionary by function names
    sorted_function_info = dict(sorted(function_info.items()))

    return sorted_function_info

        
def print_function_descriptions(module_path, deprecation_marker='deprecated', internal_marker='_'):
    """
    Prints function names and their descriptions from a Python module.

    Parameters:
    - module_path (str): The path to the Python module file.
    - deprecation_marker (str, optional): Functions are excluded if the short description is equal to this marker.
                                          Can be disabled by setting to None. Defaults to 'deprecated'.
    - internal_marker (str, optional): The string marking internal functions. Defaults to "_".

    Returns:
    - None
    """
    function_info = _extract_function_short_info(module_path, deprecation_marker, internal_marker)

    for function_name, description in function_info.items():
        print(f"- {function_name}: {description}")
        

        
def timestamp(date=None, formatstring='%Y%m%d'):
    """
    Generate a formatted timestamp string from a given date.

    Parameters:
    - date (datetime-like or str, optional): The date to be formatted. If not provided, the current date and time will be used.
    - formatstring (str, optional): The format string to specify the output format of the timestamp. 
      Default is '%Y%M%d' which represents year, minute, and day.

    Returns:
    - str: A formatted timestamp string based on the input date and formatstring.
    """
    if date == None: date = datetime.datetime.now()
    date = pd.to_datetime(date)
    return date.strftime(formatstring)


def image_display_snippet():
    """
    Generates a code snippet for a workaround to display images with relative paths.

    This function returns a string containing Python code that defines a workaround 
    function for displaying images with relative paths in Jupyter notebooks. 
    The code snippet can be copied and pasted into a notebook cell.

    Example:
    >>> print(image_display_snippet())
    """
    text = ['# Workaround for displaying images with relative paths',
            'import IPython.display',
            'def display_image(path): return IPython.display.Image(filename=path)']
    return '\n'.join(text)
    
    
def _scan_file(file_path, searchstring, max_line_length=100):
    """
    Scan a single file for the given search string.

    Args:
    - file_path (str): Path to the file to scan.
    - searchstring (str): The string to search for in the file.
    - max_line_length (int): Maximum length of the line to print.

    Yields:
    dict: Dictionary containing filepath, line number, and line for each match.
    """
    try:
        with open(file_path, 'r') as f:
            for line_number, line in enumerate(f):
                if searchstring in line:
                    yield {'filepath': file_path, 'line_number': line_number, 'line': line.rstrip()[:max_line_length] + ' [...]' if len(line) > max_line_length else line.rstrip()}
    except (FileNotFoundError, PermissionError) as e:
        print(f"Error accessing file {file_path}: {e}")

        
def scan_files(files, searchstring, max_line_length=100, verbose=False):
    """
    Scan files in a directory for a given search string.

    Args:
    - directory (str): The directory path to scan.
    - searchstring (str): The string to search for in the files.
    - max_line_length (int): Maximum length of the line to print.
    - verbose (bool): If True, prints the results; otherwise, returns a list of dicts.

    Returns:
    list: List of dictionaries containing filepath, line number, and line for each match.
    """
    matches = []
    for file_path in files:
        if verbose:
            print(file_path)
        for match in _scan_file(file_path, searchstring, max_line_length):
            if verbose:
                print(f'Found string in {os.path.basename(file_path)}::{match["line_number"]}')
                print('\t', match["line"])
            matches.append(match)
    return matches


def is_running_in_notebook() -> bool:
    """
    Determines if the current environment is a Jupyter Notebook.

    Returns:
        bool: True if running in a Jupyter Notebook, False otherwise.
    """
    try:
        shell = get_ipython().__class__.__name__
        return shell == 'ZMQInteractiveShell'
    except NameError:
        return False


def filter_user_info(error_message):
    """
    Sanitize an error message by replacing sensitive information with generic placeholders.

    This function replaces file paths and environment variables in the given error message
    with generic fillers to prevent the exposure of sensitive user information.

    Args:
        error_message (str): The error message containing potential sensitive information.

    Returns:
        str: The sanitized error message with file paths and environment variables replaced
             by '[FILE_PATH]' and '[ENV_VAR]' respectively.
    """
    # Replace file paths with generic filler
    error_message = re.sub(r'~/.*/', '[FILE_PATH]', error_message)
    error_message = re.sub(r'/.*\.py', '[FILE_PATH]', error_message)
    error_message = re.sub(r'/.*\.so', '[FILE_PATH]', error_message)
    # Replace environment variables with generic filler
    error_message = re.sub(r'\$.*\$', '[ENV_VAR]', error_message)
    error_message = re.sub(r'~.*\$', '[ENV_VAR]', error_message)

    return error_message





##############################################################################################################
# to be deleted: 
##############################################################################################################
    
    
    
# moved to calculations:
# def int_factorize(x, always_bigger=True):
#     '''
#     square root for integers
#     '''
#     m = int(np.sqrt(x))
#     n = int(x/m)
#     if m*n < x and always_bigger : m += 1
#     return m,n



# def disk_data_from_keywords(directory, keywords, exclude=False, return_file_list=False):
#     files = [f for f in os.listdir(directory) if match(f, keywords)]
#     # for debugging purposes you can get the file list instead of the dataset
#     if return_file_list: return files
#     if exclude: 
#         for x in exclude:
#             files = [f for f in files if not x in files]
#     files = [os.path.join(directory, f) for f in files]
#     dataset = xr.open_mfdataset(files)
#     return dataset

# def eureca_data_from_keywords(keywords):
#     cat = eurec4a.get_intake_catalog()['simulations']['ICON']['LES_CampaignDomain_control']
#     identifier = [f for f in list(cat) if match(f, keywords)]
#     if    len(identifier) > 1 : raise(ValueError(f'Keywords match more than one dataset: {identifier}'))
#     elif  len(identifier) < 0 : raise(ValueError(f'No match for these keywords.'))
#     else: return cat[identifier[0]].to_dask()


# def eureca_data_from_keywords(keywords: list) -> dask.array.Array:
#     # Get the EUREC4A catalog
#     cat = intake.open_catalog("eurec4a_catalog.yml")

#     # Find dataset identifiers that match the keywords
#     identifier = [f for f in list(cat) if match(f, keywords)]

#     # Handle matching scenarios
#     if len(identifier) > 1:
#         raise ValueError(f"Keywords match more than one dataset: {identifier}")
#     elif len(identifier) == 0:
#         raise ValueError("No match for these keywords.")
#     else:
#         # Retrieve the dataset and return it as a Dask array
#         dataset = cat[identifier[0]].to_dask()
#         return dataset
    
    
# def list_src_files(directory='/home/m/m300872/warm_eurec4a/submodules/', suffix='.py'):
#     return [os.path.join(directory, f) for f in os.listdir(directory) if f.endswith(suffix)]


# def all_module_files():
#     return load_text_files(list_src_files())

# def some_module_files():
#     # reduced list, because 1. not all are needed, 2. the meteogram extract has its own main body, that interferes with its usage in this way.
#     files = ['/home/m/m300872/warm_eurec4a/submodules/file_handling.py',
#              '/home/m/m300872/warm_eurec4a/submodules/calculations.py',
#              '/home/m/m300872/warm_eurec4a/submodules/postprocessing.py']
#     return load_text_files(files)





# def list_slurm_jobs(user_id='m300872', name=None):
#     slurm_output = subprocess.check_output(['squeue', '-o' '"%.18i %.9P %.25j"', '-u', user_id ]).decode('ascii')
#     slurm_output = slurm_output.split('\n')[1:-1]
#     slurm_output = [s[1:-1] for s in slurm_output]
#     slurm_output = [[s for s in line.split(' ') if not s == ''] for line in slurm_output]
#     jobs = list()
#     for line in slurm_output:
#         if name == None or name == line[2]: jobs.append({'id':line[0], 'partition':line[1], 'name':line[2]})
#     return jobs


