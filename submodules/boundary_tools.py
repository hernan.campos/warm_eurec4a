# functions for boundary script launch
import os
import hashlib
import numpy as np
import xarray as xr
import metpy.calc as mpcalc
from metpy.units import units

# The import from konrad was failing on the first, but succeding on second attempt. With this error message:
# File ~/.local/lib/python3.10/site-packages/climt/_components/_berger_solar_insolation.pyx:1, in init climt._components._berger_solar_insolation()
# ValueError: numpy.ndarray size changed, may indicate binary incompatibility. Expected 96 from C header, got 88 from PyObject
# This is the work around
try:
    from konrad.lapserate import get_moist_adiabat as konrad_moist_adiabat
except:
    from konrad.lapserate import get_moist_adiabat as konrad_moist_adiabat
# get_moist_adiabat(p, p_s=None, T_s=300.0, T_min=155.0)
# https://konrad.readthedocs.io/_modules/konrad/lapserate.html#get_moist_adiabat

def get_hash(string):
    # https://www.pythoncentral.io/hashing-strings-with-python/
    hash_object = hashlib.sha1(string.encode())
    return hash_object.hexdigest()

def basename(path, extension=True):
    if extension: 
        return os.path.basename(path)
    else: # removes extension
        return os.path.splitext(os.path.basename(path))[0]
    
def dirname(path):
    return os.path.dirname(path)

def extension(path):
    return os.path.splitext(path)[1]

def is_netcdf(f):
    return extension(f) == '.nc'

def get_filelist(lost_path, conserved_path):
    file_list = []
    for root, directories,files in os.walk(os.path.join(lost_path, conserved_path)):
        for name in files: 
            if is_netcdf(name):
                path = os.path.join(os.path.relpath(root, start=lost_path), name)
                file_list.append(path)
    file_list.sort()
    return file_list

def create_directory_structure(file_list, out_directory):
    for f in file_list:
        dirpath = os.path.join(out_directory,dirname(f))
        os.makedirs(dirpath, exist_ok=True)
        
def sublist(inlist, n_lists, index):
    return list(np.array_split(inlist, n_lists)[index])

def hydrostatic_profile(h, p_surf=100000):
    # https://unidata.github.io/MetPy/latest/api/generated/metpy.calc.add_height_to_pressure.html
    # This assumes a standard atmosphere [NOAA1976].
    # mpcalc.add_height_to_pressure(pressure, height)
    p = np.zeros(len(h))
    p[0] = p_surf
    for i in range(1,len(p)):
        pressure = p[i-1] * units('Pa')
        height   = (h[i] - h[i-1]) * units('m')
        calc = mpcalc.add_height_to_pressure(pressure, height).to(units('Pa'))
        p[i] = calc.magnitude
        if p[i] == np.nan: p[i] = 0.0
    return p

# these two still have the problem, that they crash if the temperature gets too cold.
def metpy_dry_lapse_profile(p, T_s=300, T_min=None):
    # https://unidata.github.io/MetPy/latest/api/generated/metpy.calc.dry_lapse.html#metpy.calc.dry_lapse
    # metpy.calc.dry_lapse(pressure, temperature, reference_pressure=None, vertical_dim=0)
    t = mpcalc.dry_lapse(p * units('Pa'),  temperature=T_s * units('kelvin'))
    if T_min: t = np.where(t.magnitude > T_min, t, T_min * units('kelvin'))
    return t

def joined_profile(p, p_threshold=95000, T_s=300, T_min=None):
    dry = metpy_dry_lapse_profile(p, T_s=T_s)
    hi = p <= p_threshold
    lo = p >  p_threshold
    moist = konrad_moist_adiabat(p[hi], T_s=dry[hi][0].magnitude)#, T_min=T_min)
    t = np.empty(len(p))
    t[lo] = dry[lo]
    t[hi] = moist
    return t

def construct_volume(column, shape):
    levels = [np.ones(shape) * value for value in np.flip(column)]
    return np.asarray(levels)

def interpolate_full_level(half_level):
    return np.asarray([(half_level[i]+half_level[i-1])/2 for i in range(1,len(half_level))])

DEFAULT_VARNAMES = {'pressure':'pres',
                    'temperature':'temp',
                    'virtual potential temperature':'theta_v',
                    'density':'rho',
                    'specific humidity':'qv',
                    'relative humidity':'RH',
                    'half level height':'z_ifc'}

def add_varnames_to_default(custom_varnames):
    global DEFAULT_VARNAMES
    varnames = DEFAULT_VARNAMES
    if custom_varnames: # if any given, this will overwrite the default
        for key, value in custom_varnames.items(): varnames[key] = value
    return varnames
    
def add_relative_humidity(dataset, varnames=None):
    varnames = add_varnames_to_default(varnames)
    p  = dataset[varnames['pressure']].values    * units('pascal')
    t  = dataset[varnames['temperature']].values * units('kelvin')
    qv = dataset[varnames['specific humidity']].values   # ratio of water vapor mass to total moist air parcel mass

    rh = mpcalc.relative_humidity_from_specific_humidity(p, t, qv)
    # On adding variables to the dataset: https://stackoverflow.com/a/56590572
    dimnames = dataset[varnames['specific humidity']].dims
    dataset[varnames['relative humidity']] = (dimnames, rh.magnitude)
    return dataset

def restore_specific_humidity(dataset, varnames=None, drop_RH=True):
    varnames = add_varnames_to_default(varnames)
    p  = dataset[varnames['pressure']].values    * units('pascal')
    t  = dataset[varnames['temperature']].values * units('kelvin')
    rh = dataset[varnames['relative humidity']].values
    
    eps = mpcalc.mixing_ratio_from_relative_humidity(p, t, rh)
    qv = mpcalc.specific_humidity_from_mixing_ratio(eps)
    # On adding variables to the dataset: https://stackoverflow.com/a/56590572
    dimnames = dataset[varnames['relative humidity']].dims
    dataset[varnames['specific humidity']] = (dimnames, qv.magnitude)
    if drop_RH: dataset = dataset.drop_vars(varnames['relative humidity'])
    return dataset

def add_to_temperature(dataset, delta, varnames=None):
    varnames = add_varnames_to_default(varnames)
    z = dataset[varnames['half level height']].isel(ncells=0).values
    h = interpolate_full_level(np.flip(z))
    p = np.flip(dataset[varnames['pressure']].mean(dim='ncells').values)

    temp_delta = construct_volume(delta, dataset.ncells.shape)
    dataset[varnames['temperature']].values = dataset[varnames['temperature']].values + temp_delta
    return dataset

def add_prognostic_variables(dataset, varnames=None):
    varnames = add_varnames_to_default(varnames)
    p  = dataset[varnames['pressure']].values    * units('pascal')
    t  = dataset[varnames['temperature']].values * units('kelvin')
    qv = dataset[varnames['specific humidity']].values   # ratio of water vapor mass to total moist air parcel mass
    
    eps   = mpcalc.mixing_ratio_from_specific_humidity(qv)
    theta = mpcalc.virtual_potential_temperature(p,t,eps)
    rho   = mpcalc.density(p, t, eps)
    
    dimnames = dataset[varnames['temperature']].dims
    dataset[varnames['virtual potential temperature']] = (dimnames, theta.magnitude)
    dataset[varnames['density']] = (dimnames, rho.magnitude)
    return dataset

def warm_atmospheric_profile(ifile,ofile,warming_profile,varnames=None):
    d = xr.open_dataset(ifile)
    d = add_relative_humidity(d, varnames=varnames)
    d = add_to_temperature(d, warming_profile, varnames=varnames)
    d = restore_specific_humidity(d, drop_RH=True, varnames=varnames)
    d.to_netcdf(ofile)

def delta_adiabatic_profile(t_high,t_low,file_for_z):
    d = xr.open_dataset(file_for_z)
    z_ifc = np.flip(d['z_ifc'].isel(time=0,ncells=0).values)
    z_full = interpolate_full_level(z_ifc)
    p_surf = 100000 # np.ravel(d['pres'].isel(time=0,ncells=0).values)[-1]
    p = hydrostatic_profile(z_full, p_surf)
    delta = joined_profile(p, T_s=t_high) - joined_profile(p, T_s=t_low)
    return delta
