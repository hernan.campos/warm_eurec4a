"""
Function Timer Decorator Module

This module provides a decorator, `function_timer`, that measures and prints the execution time of functions. The decorator records the time taken for a function to execute and prints the results, including the function's name and the execution time in seconds.

Usage:
    Import this module and apply the `@function_timer` decorator to functions you want to time.

Functions:
    - `function_timer(func)`: A decorator function for measuring and printing the execution time of other functions.

Example:
    >>> from function_timer import function_timer
    >>>
    >>> @function_timer
    ... def my_function():
    ...     # Code to be timed
    ...     pass
    ...
    >>> my_function()
    Function 'my_function' executed in 0.1234s

Notes:
    - The function is taken from https://www.geeksforgeeks.org/timing-functions-with-decorators-python/
    - The decorator uses the `time()` function from the `time` module to record the current time before and after the execution of the decorated function.
    - The execution time is printed in seconds with a precision of four decimal places.

Author: Hernan Campos
Email: hernan.campos@mpimet.mpg.de
"""

from time import time as time

def function_timer(func):
    """
    Decorator that measures and prints the execution time of a function.

    This decorator is used to measure the execution time of a function and print the
    time it took to execute. When applied to a function using the `@function_timer`
    syntax, it will print the function's name and the execution time in seconds.

    Args:
        func (function): The function to be timed.

    Returns:
        function: A wrapped version of the original function that includes timing
            functionality.

    Example:
        >>> @function_timer
        ... def my_function():
        ...     # Code to be timed
        ...     pass
        ...
        >>> my_function()
        Function 'my_function' executed in 0.1234s

    Note:
        - The decorator uses the `time()` function from the `time` module to record
          the current time before and after the execution of the decorated function.
        - The execution time is printed in seconds with a precision of four decimal places.
    """
    def wrap_func(*args, **kwargs):
        t1 = time()
        result = func(*args, **kwargs)
        t2 = time()
        print(f'Function {func.__name__!r} executed in {(t2-t1):.4f}s')
        return result
    return wrap_func