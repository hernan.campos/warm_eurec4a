"""
Dask Cluster Launcher Module

This module provides a function, `launch_dask_client`, to launch a Dask distributed cluster on a SLURM-managed 
cluster. The module provides a single function:
    - `launch_dask_client`: Launches a Dask distributed cluster and returns a Dask client.

There are many **mashine specific** (e.g. `queue = {partition name}`) and *use specific* (`account_name`) default parameters set.

Example:
    >>> from dask_cluster_launcher import launch_dask_client
    >>> client, cluster = launch_dask_client(account_name='myaccount', queue='compute', job_name='DaskJob')
    >>> future = client.submit(my_function, arg1, arg2)
    >>> result = future.result()

Note:
- Adjust the `account_name`, `queue`, `job_name`, and other parameters to match your specific HPC cluster.
- For optimal use, consult your HPC system documentation and tailor the parameters accordingly.

Author: Hernan Campos
Email:  hernan.campos@mpimet.mpg.de
"""

import pathlib
import getpass
import tempfile
import dask_jobqueue as djq
import dask.distributed as ddist

def launch_dask_client(
    account_name = 'mh0926', # Account that is going to be 'charged' fore the computation
    queue = 'compute', # Name of the partition we want to use
    job_name = 'LC', # Job name that is submitted via sbatch
    memory = "60GiB", # Max memory per node that is going to be used - this depends on the partition
    cores = 8, # Max number of cores per task that are reserved - also partition dependend
    walltime = '1:00:00' # Walltime - also partition dependent
    ):
     """
    Launch a Dask distributed cluster and return a Dask client.

    This function launches a Dask distributed cluster on a high-performance computing (HPC) system,
    such as a SLURM-managed cluster. It provides options to customize the cluster configuration,
    including account name, queue, job name, memory, cores, and walltime. The function returns a
    Dask client connected to the cluster.

    Args:
        account_name (str): The HPC account name that provides the computational resources.
        queue (str): The name of the partition to use on the HPC cluster.
        job_name (str): The name of the job submitted via SLURM.
        memory (str): The maximum memory per node to be used (e.g., '60GiB').
        cores (int): The maximum number of cores per task to be reserved.
        walltime (str): The walltime for the job in HH:MM:SS format.

    Returns:
        dask.distributed.Client: A Dask client connected to the launched cluster.

    Example:
        >>> client, cluster = launch_dask_client(account_name='myaccount', queue='compute', job_name='DaskJob')
        >>> future = client.submit(my_function, arg1, arg2)
        >>> result = future.result()

    Note:
        - The `account_name`, `queue`, `job_name`, and other parameters are system-specific
          and must be adjusted for your environment.
    """
    scratch_dir = pathlib.Path('/scratch') / getpass.getuser()[0] / getpass.getuser() # Define the users scratch dir
    print(scratch_dir)
    # Create a temp directory where the output of distributed cluster will be written to, after this notebook
    # is closed the temp directory will be closed
    dask_tmp_dir = tempfile.TemporaryDirectory(dir=scratch_dir, prefix=job_name)
    cluster = djq.SLURMCluster(memory=memory,
                           cores=cores,
                           project=account_name,
                           walltime=walltime,
                           queue=queue,
                           name=job_name,
                           scheduler_options={'dashboard_address': ':187'},
                           local_directory=dask_tmp_dir.name,
                           job_extra=[f'-J {job_name}', 
                                      f'-D {dask_tmp_dir.name}',
                                      f'--begin=now',
                                      f'--output={dask_tmp_dir.name}/LOG_cluster.%j.o',
                                      f'--output={dask_tmp_dir.name}/LOG_cluster.%j.o'
                                     ],
                            extra=["--lifetime", "50m", "--lifetime-stagger", "4m"],  # When using cluster.adapt() this limits the 
                            # actual cluster lifetime and allows a 4m overlap for dask to start a new job, once you run into the 
                            # walltime limit
                           interface='ib0')
    # cluster.scale(jobs=1) #order n nodes that will give us n*memory of distributed memory and n*cpus-per-task cores to work on
    return ddist.Client(cluster), cluster