import os
import numpy as np
import xarray as xr
import pandas as pd
import scipy.stats

import matplotlib.pyplot as plt
import matplotlib.collections 
import matplotlib.dates
import matplotlib.artist
import matplotlib.lines
import matplotlib.patches
import matplotlib.text
import mpl_toolkits.axes_grid1 # make_axes_locatable

import PIL
import cartopy.feature as cfeature
import cartopy.crs as ccrs

import inspect

def center_around_zero(array):
    """
    Adjusts the array to ensure symmetry around zero.

    This function replaces the highest or lowest value in the array
    to make the absolute value of the minimum equal to the absolute
    value of the maximum.

    Parameters:
        array (numpy.ndarray): The input array to be adjusted.

    Returns:
        numpy.ndarray: The adjusted array with symmetric properties around zero.

    Notes:
        For colorbars centered around zero, this function ensures symmetry
        by replacing the highest or lowest value in the array with the 
        absolute value of the opposite extreme, ensuring |min| == |max|.

    Example:
        >>> import numpy as np
        >>> arr = np.array([-3, 1, 4, -2])
        >>> adjusted_arr = center_around_zero(arr)
    """
    array_flat = np.ravel(array)
    min_index, min_value = np.argmin(array), array_flat[np.argmin(array)]
    max_index, max_value = np.argmax(array), array_flat[np.argmax(array)]
    if np.abs(min_value) > np.abs(max_value):
        array_flat[max_index] = np.abs(min_value)
    else:
        array_flat[min_index] = - max_value
    array = np.reshape(array_flat, array.shape)
    return array

def bipolar_vminmax(x):
    """
    Determine the symmetric limits for a bipolar colormap based on data.

    This function calculates the upper and lower limits that can be used
    to symmetrically center a colormap around zero based on the maximum
    absolute value present in the data.

    Parameters:
        x (numpy.ndarray or xarray.DataArray): Input data.

    Returns:
        tuple: A tuple containing the lower and upper limits.

    Example:
        >>> import numpy as np
        >>> arr = np.array([-3, 1, 4, -2])
        >>> limits = bipolar_vminmax(arr)
    """
    upper = np.abs(np.nanmax(x.values))
    lower = np.abs(np.nanmin(x.values))
    both  = np.max((upper, lower))
    return (-both, both)

def remove_spines(ax, borders=['top','bottom','left','right']):  
    """
    Hide specific spines (axes borders) on a matplotlib Axes object.

    Parameters:
        ax (matplotlib Axes): The Axes object whose spines are to be hidden.
        borders (list): A list of strings specifying which spines to hide.
                        Possible values: 'top', 'bottom', 'left', 'right'.

    Returns:
        matplotlib Axes: The modified Axes object with the specified spines hidden.

    Example:
        >>> import matplotlib.pyplot as plt
        >>> fig, ax = plt.subplots()
        >>> ax.plot([1, 2, 3])
        >>> ax = remove_spines(ax, borders=['top', 'right'])
    """  
    for border in borders:
        ax.spines[border].set_visible(False)
    return ax
          
def remove_ticks(ax, borders=['left','right','bottom']):
    """
    Hide ticks and tick labels on specific sides of a matplotlib Axes object.

    Parameters:
        ax (matplotlib Axes): The Axes object whose ticks and labels are to be hidden.
        borders (list): A list of strings specifying which sides to hide ticks and labels.
                        Possible values: 'left', 'right', 'bottom'.

    Returns:
        matplotlib Axes: The modified Axes object with ticks and labels hidden on specified sides.

    Example:
        >>> import matplotlib.pyplot as plt
        >>> fig, ax = plt.subplots()
        >>> ax.plot([1, 2, 3])
        >>> ax = remove_ticks(ax, borders=['left', 'right'])
    """
    left, right, bottom, labelleft, labelbottom = True, True, True, True, True
    if 'left'   in borders: left = False; labelleft = False
    if 'right'  in borders: right = False
    if 'bottom' in borders: bottom = False; labelbottom = False
    ax.tick_params(left=left, right=right, bottom=bottom, 
                   labelleft=labelleft, labelbottom=labelbottom)
    return ax


def icon2datetime(icon_dates):
    """
    Convert datetime objects in ICON format to Python datetime objects.

    Parameters:
        icon_dates (collection): Collection of datetime values in ICON format.

    Returns:
        pd.DatetimeIndex: Pandas DatetimeIndex containing Python datetime objects.

    Notes:
        Taken from the esm_analysis lib.
        This function converts datetime values in ICON format to Python datetime objects.
        ICON format represents dates as floating-point numbers, where the integer part
        represents the date in YYYYMMDD format, and the fractional part represents
        the time in hours.

    Example:
        >>> import numpy as np
        >>> import pandas as pd
        >>> icon_dates = [20011201.5, 20020101.25, 20030115.75]
        >>> python_dates = icon2datetime(icon_dates)
        >>> print(python_dates)
        DatetimeIndex(['2001-12-01 12:00:00', '2002-01-01 06:00:00',
                       '2003-01-15 18:00:00'],
                      dtype='datetime64[ns]', freq=None)
    """
    # Convert to numpy array if not already
    try: icon_dates = icon_dates.values
    except AttributeError: pass

    # Ensure array-like
    try: icon_dates = icon_dates[:]
    except TypeError:
        icon_dates = np.array([icon_dates])

    # Function to convert each ICON datetime to Python datetime
    def _convert(icon_date):
        frac_day, date = np.modf(icon_date)
        frac_day *= 60 ** 2 * 24  # Convert fractional day to seconds
        date = str(int(date))
        date_str = datetime.datetime.strptime(date, '%Y%m%d')  # Parse date part
        td = datetime.timedelta(seconds=int(frac_day.round(0)))  # Create timedelta for time part
        return date_str + td  # Combine date and time

    # Vectorize conversion function
    conv = np.vectorize(_convert)
    # Convert datetime values
    try:
        out = conv(icon_dates)
    except TypeError:
        out = icon_dates

    # Return single datetime if only one input
    if len(out) == 1:
        return pd.DatetimeIndex(out)[0]
    # Return DatetimeIndex
    return pd.DatetimeIndex(out)


def month2int(month):
    """
    Convert a month name to its corresponding integer representation.

    Parameters:
        month (str): The name of the month (case insensitive).

    Returns:
        int: The integer representation of the month (1 for January, 2 for February, etc.).

    Raises:
        KeyError: If an invalid month name is provided.

    Example:
        >>> month2int('March')
        3
    """
    months = {'january':1,   'february':2, 'march':3,     'april':4,
              'may':5,       'june':6,     'july':7,      'august':8,
              'september':9, 'october':10, 'november':11, 'december':12}
    try: return months[month.lower()]
    except KeyError:
        raise KeyError(f"Invalid month name given: {month}")


def int2month(i):
    """
    Convert an integer representing a month to its corresponding month name.

    Parameters:
        i (int): The integer representing the month (1 for January, 2 for February, etc.).

    Returns:
        str: The name of the month.

    Raises:
        KeyError: If an invalid month index is provided.

    Example:
        >>> int2month(6)
        'June'
    """
    months = {'January':1,   'February':2, 'March':3,     'April':4,
              'May':5,       'June':6,     'July':7,      'August':8,
              'September':9, 'October':10, 'November':11, 'December':12}
    try: return list(months)[i-1]
    except IndexError:
        raise KeyError(f"Invalid month index given: {i}")

        
def gif_from_image_list(file_list, save_name=False, delete_frames=False):
    if not save_name: f'generated_clip_{len(file_list)}-frames.gif'
    # https://pillow.readthedocs.io/en/stable/handbook/image-file-formats.html#gif
    img, *imgs = [PIL.Image.open(f) for f in file_list]
    img.save(fp=save_name, format='GIF', append_images=imgs,
             save_all=True, duration=len(file_list), loop=0)
    if delete_frames:
        for file in file_list: os.remove(file)
    return save_name

# Lukas's ffmpeg version
def image2mpeg(glob, outfile, framerate=12, resolution='1920x1080'):
    """
    Combine image files to a video using ``ffmpeg``.
    
    Notes:
        The function is tested for ``ffmpeg`` versions 2.8.6 and 3.2.2.
    Parameters:
        glob (str): Glob pattern for input files.
        outfile (str): Path to output file.
            The file fileformat is determined by the extension.
        framerate (int or str): Number of frames per second.
        resolution (str or tuple): Video resolution given in width and height
            (``"WxH"`` or ``(W, H)``).
    Raises:
        Exception: The function raises an exception if the
            underlying ``ffmpeg`` process returns a non-zero exit code.
    Example:
        >>> image2mpeg('foo_*.png', 'foo.mp4')
    """
    if not shutil.which('ffmpeg'):
        raise Exception('``ffmpeg`` not found.')

    # If video resolution is given as tuple, convert it into string format
    # to directly pass it to ffmpeg later.
    if isinstance(resolution, tuple) and len(resolution) == 2:
        resolution = '{width}x{height}'.format(width=resolution[0],
                                               height=resolution[1])

    p = subprocess.run(
        ['ffmpeg',
         '-framerate', str(framerate),
         '-pattern_type', 'glob', '-i', glob,
         '-s:v', resolution,
         '-c:v', 'libx264',
         '-profile:v', 'high',
         '-crf', '20',
         '-pix_fmt', 'yuv420p',
         '-y', outfile
         ],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        universal_newlines=True
        )
    
def non_redundant_legend(ax):
    """
    Remove duplicate entries from the legend of a matplotlib Axes object.

    Parameters:
        ax (matplotlib Axes): The Axes object containing the legend to be deduplicated.

    Returns:
        tuple: A tuple containing the handles and labels of the deduplicated legend.

    Example:
        >>> handles, labels = non_redundant_legend(ax)
        >>> ax.legend(handles, labels)
    """
    handles, labels = ax.get_legend_handles_labels()
    newLabels, newHandles = [], []
    for handle, label in zip(handles, labels):
      if label not in newLabels:
        newLabels.append(label)
        newHandles.append(handle)
    ax.legend(newHandles, newLabels)
    return newHandles, newLabels


def add_unified_legend(ax, axs_list):
    """
    Add a unified legend from multiple axes to a single axis.

    Parameters:
        ax (matplotlib.axes.Axes): The axis to which the unified legend will be added.
        axs_list (list): List of matplotlib.axes.Axes objects containing the legends to be unified.

    Returns:
        None
    """
    # Initialize empty lists for collecting legend handles and labels
    all_lines = []
    all_labels = []

    # Iterate over each axis in the list
    for axs in axs_list:
        # Get legend handles and labels from the axis
        lines, labels = axs.get_legend_handles_labels()
        all_lines.extend(lines)
        all_labels.extend(labels)

    # Add unified legend to the specified axis
    ax.legend(all_lines, all_labels, loc='upper right')


def adjust_spines(ax, spines):
    # https://matplotlib.org/3.4.3/gallery/ticks_and_spines/spine_placement_demo.html
    for loc, spine in ax.spines.items():
        if loc in spines:
            spine.set_position(('outward', 10))  # outward by 10 points
        else:
            spine.set_color('none')  # don't draw spine

    # turn off ticks where there is no spine
    if 'left' in spines:
        ax.yaxis.set_ticks_position('left')
    else:
        # no yaxis ticks
        ax.yaxis.set_ticks([])

    if 'bottom' in spines:
        ax.xaxis.set_ticks_position('bottom')
    else:
        # no xaxis ticks
        ax.xaxis.set_ticks([])
        
def savefig(fig, filename, overwrite=True):
    # bbox_inches="tight" will prevent labels being cut off
    # https://stackoverflow.com/a/44649558
    if (not os.path.exists(filename)) or overwrite: fig.savefig(filename, bbox_inches="tight")

def add_map_view(ax, map_projection, latlon_limits, colored_map=False, gridlines=True):
    # decorations
    ax.coastlines()
    if colored_map:
        # (https://techoverflow.net/2021/04/25/how-to-add-colored-background-to-cartopy-map/)
        ax.stock_img()
    else:
        # or just color the land:
        ax.add_feature(cfeature.LAND, facecolor='black', alpha=0.4)
    ax.set_extent(latlon_limits, ccrs.PlateCarree())
    if gridlines:
        ax.gridlines(draw_labels=True, crs=map_projection)

def add_rectangles(ax, rectangles, **kwargs):
    # Create patch collection with specified colour/alpha, inside the loop, because it can not be reused
    pc = matplotlib.collections.PatchCollection(rectangles, **kwargs)
    ax.add_collection(pc)

def provide_fig(im_path, plotting_function, dpi=80, display=True):
    '''
    either loads a fig from im_path, or uses the plotting_function to create it if not there yet
    '''
    if os.path.exists(im_path):
        im_data = plt.imread(im_path)
        height, width, depth = im_data.shape
        figsize = width / float(dpi), height / float(dpi)

        fig, ax = plt.subplots(figsize=figsize)
        ax.imshow(im_data, aspect='auto')
        remove_spines(ax)
        remove_ticks(ax)
    else:
        fig, ax = plotting_function()
        savefig(fig, im_path)
    if display: 
        fig.show()
    else:
        return fig, ax

def rotate_xticks(ax, rotation=30):
    # Rotating X-axis labels
    for label in ax.get_xticklabels(which='major'):
        label.set(rotation=rotation, horizontalalignment='right')
    return ax

def offset_axis(ax):
    # https://matplotlib.org/2.0.2/examples/pylab_examples/spine_placement_demo.html
    ax.spines['right'].set_color('none')
    ax.spines['top'].set_color('none')
    ax.spines['bottom'].set_position(('axes', -.1)) #
    ax.spines['left'  ].set_position(('axes', -.1)) # 
    ax.xaxis.set_ticks_position('bottom')
    ax.yaxis.set_ticks_position('left')
    return ax

def ax_vertical_profile(ax, dataset, var, kwargs={}):
    ax.plot(dataset[var], dataset[dataset[var].dims[0]].values / 1000, **kwargs)
    ax.set_ylim(dataset[dataset[var].dims[0]].values[-1] / 1000, dataset[dataset[var].dims[0]].values[0] / 1000)
    ax.set_ylabel('height / km')
    try: ax.set_xlabel(dataset[var].attrs['long_name'] + ' / ' + dataset[var].attrs['units'] )
    except: pass
    ax = offset_axis(ax)
    return ax


def datetime2str(datetime, format_string='%d.%m.,%H:%M'):
    """
    Converts a numpy.datetime64 object to a nicely formatted string.

    Parameters:
        datetime (numpy.datetime64): The numpy datetime object to be formatted.
        format_string (str): Optional. The format string specifying the desired output format.
            Default is '%d.%m.,%H:%M'.

    Returns:
        str: A nicely formatted string representation of the datetime object.
    """
    ts = pd.to_datetime(str(datetime)) 
    d = ts.strftime(format_string)
    return d


def ax_triangles(ax, dataset, var, cell_vertex_varname='vertex_of_cell', vminmax=(None, None), 
                 cmap='viridis', edgecolor='none', set_xy_lim=False):    
    """
    Plot triangular mesh on a given Axes object.

    This function plots a triangular mesh on a given Axes object using the matplotlib.tripcolor method. The triangular
    mesh is defined by the vertices of cells provided in the dataset. Optionally, a variable can be mapped onto the mesh
    using a colormap.

    Args:
        ax (matplotlib.axes.Axes): The Axes object onto which the triangular mesh will be plotted.
        dataset (xarray.Dataset): The dataset containing the mesh information.
        var (str or None): The name of the variable to be mapped onto the mesh. If None, only the mesh will be plotted
            without coloring.
        cell_vertex_varname (str, optional): The name of the variable in the dataset containing the vertex indices of
            cells. Defaults to 'vertex_of_cell'.
        vminmax (tuple, optional): A tuple containing the minimum and maximum values for the color scale. Defaults to
            (None, None), which means that the color scale is automatically determined based on the data.
        cmap (str or Colormap, optional): The colormap to be used for coloring the mesh. Defaults to 'viridis'.
        edgecolor (str or None, optional): The color of the edges of the triangles. If None, no edges will be drawn.
            Defaults to None.
        set_xy_lim (bool, optional): Whether to automatically set the x and y limits of the Axes object to fit the
            mesh. Defaults to False.

    Returns:
        matplotlib.axes.Axes: The Axes object with the triangular mesh plotted.

    Note:
        - Shoutout to Tobias Kölling, who showcased this here:https://easy.gems.dkrz.de/Processing/playing_with_triangles/tripcolor.html.
        - The vertices of cells should be provided in the dataset as a variable with dimensions
            (number_of_vertices, number_of_cells).
        - If var is not None, the variable specified by var will be mapped onto the mesh using the given colormap.
        - If set_xy_lim is True, the x and y limits of the Axes object will be automatically adjusted to fit the mesh.
    """
    vertices_of_cells = dataset['vertex_of_cell'].T.values - 1
    vmin, vmax = vminmax
    if var == None: 
        nan_field = np.full(len(vertices_of_cells), np.nan)
        trip = ax.tripcolor(dataset.vlon, dataset.vlat, 
                            vertices_of_cells, 
                            nan_field, 
                            vmin=vmin, vmax=vmax, 
                            edgecolors=edgecolor)
    else:
        z_field = dataset[var].values
        trip = ax.tripcolor(dataset.vlon, dataset.vlat, 
                            vertices_of_cells, z_field,   
                            vmin=vmin, vmax=vmax, 
                            edgecolors=edgecolor, 
                            cmap=cmap)
        try:    label = f'{dataset[var].attrs["long_name"]}/ {dataset[var].attrs["units"]}'
        except: label = None
        plt.colorbar(trip, ax=ax, label=label)
    if set_xy_lim:
        used_vertices = np.unique(vertices_of_cells) 
        lat_min = dataset.vlat[used_vertices].min().values
        lat_max = dataset.vlat[used_vertices].max().values
        lon_min = dataset.vlon[used_vertices].min().values
        lon_max = dataset.vlon[used_vertices].max().values
        ax.set_xlim(lon_min, lon_max)
        ax.set_ylim(lat_min, lat_max)
    return ax

def ax_profile(ax, data, **kwargs):
    x = data
    y = data[data.dims[0]] / 1000
    y.attrs = {'long_name':'heigth', 'units':'km'}
    ax.plot(x,y, **kwargs)
    ax.set_xlabel(f"{x.attrs['long_name']} / {x.attrs['units']}")
    ax.set_ylabel(f"{y.attrs['long_name']} / {y.attrs['units']}")
    ax = remove_spines(ax, ['right', 'top'])
    ax = remove_ticks(ax, ['right', 'top'])
    ax.set_ylim(0, None)
    return ax

def ax_nan_distribution(ax, dataarray, meaningful_labels=False, add_legend=True):
    """
    Plots a heatmap indicating the presence of NaN values in a 2D DataArray on the provided Axes object, with the first dimension on the x-axis
    and the second dimension on the y-axis. Optionally, tick labels can reflect the actual coordinate values without changing the number of labels.
    
    Parameters:
    - ax: Matplotlib Axes object where the heatmap will be plotted.
    - dataarray: xarray.DataArray, a 2D dataset with potential NaN values.
    - meaningful_labels: bool, if True, tick labels will reflect the actual coordinate values.
    
    The plot uses a binary color scheme to indicate the presence (white) or absence (black) of NaN values.
    """
    if len(dataarray.dims) != 2:
        raise ValueError("DataArray must be 2D.")
    
    nan_mask = np.isnan(dataarray)
    dim_names = dataarray.dims
    
    # Use 'binary' colormap for binary data (NaN presence)
    im = ax.imshow(nan_mask.T, cmap='binary', aspect='auto', interpolation='none')  # Transpose the mask for correct orientation
    
    if meaningful_labels:
        # Adjust x-ticks
        xticks = ax.get_xticks()
        xticklabels = [dataarray[dim_names[0]].values[int(tick)] if 0 <= int(tick) < len(dataarray[dim_names[0]].values) else '' for tick in xticks]
        ax.set_xticklabels(xticklabels, rotation=90, ha='right')
        # Adjust y-ticks
        yticks = ax.get_yticks()
        yticklabels = [dataarray[dim_names[1]].values[int(tick)] if 0 <= int(tick) < len(dataarray[dim_names[1]].values) else '' for tick in yticks]
        ax.set_yticklabels(yticklabels)
    
    ax.set_xlabel(dim_names[0])  # First dimension name for x-axis
    ax.set_ylabel(dim_names[1])  # Second dimension name for y-axis
    ax.set_title('NaN Distribution in Dataset')
    
    if add_legend:
        white_patch = plt.Line2D([0], [0], marker='o', color='w', label='NaN',
                              markerfacecolor='white', markersize=10)
        black_patch = plt.Line2D([0], [0], marker='o', color='w', label='Value',
                              markerfacecolor='black', markersize=10)
        ax.legend(handles=[white_patch, black_patch], bbox_to_anchor=(1.05, 1), loc='upper left', borderaxespad=0.)
    return ax


# copied here from calculations.py, because it is needed to distribute plots in a rectangle
def isqrt(x, always_bigger=True):
    '''
    Calculate integer square roots and return two factors (m, n) closest to x.

    This function calculates the integer square root for the input x by finding two integers m and n 
    such that their product is closest to x without exceeding it. If always_bigger is set to True,
    it ensures that m * n is always larger than or equal to x. Useful for distributing plots in a grid.
    
    Args:
    - x (int): The integer to be partitioned.
    - always_bigger (bool): If True, ensures m * n is always >= x. Defaults to True.

    Returns:
    - Tuple[int, int]: Two integers (m, n) where m * n is closest to x.
    '''
    m = int(np.sqrt(x))
    n = int(x/m)
    if m*n < x and always_bigger : m += 1
    return m,n

def format_dates_on_axis(ax, format_string='%d-%H:%m', rotation=45):
    ax.xaxis.set_major_formatter(matplotlib.dates.DateFormatter(format_string))
    if not rotation == None: ax.tick_params(axis='x', rotation=rotation)
    return ax

    
def _is_valid_kwarg(func, kwarg):
    """
    Check if a string is a valid keyword argument for a given function.

    Parameters:
    func (function): The function to check against.
    kwarg (str): The string to check if it's a valid keyword argument.

    Returns:
    bool: True if the string is a valid keyword argument, False otherwise.
    """
    if not callable(func):
        raise ValueError("The first argument must be a callable "
                         "function.")
    if not isinstance(kwarg, str):
        raise ValueError("The second argument must be a string.")
    
    argspec = inspect.getfullargspec(func)
    if argspec.varkw:
        # If the function accepts **kwargs, we need to check the documentation
        doc = func.__doc__
        if doc and kwarg in doc:
            return True
    
    return kwarg in argspec.args or kwarg in argspec.kwonlyargs

def ax_variable_correlation(ax, dataset, variables,
                            label_elements=['pearson'], kwargs={}):
    """
    Plots the correlation between two variables from a dataset on
    a provided Matplotlib axis. The plot includes a scatter plot
    of the variables, a linear regression line, and displays key
    metrics for assessing the correlation's goodness, such as the
    Pearson correlation coefficient and p-value.

    Parameters
    ----------
    ax : matplotlib.axes.Axes
        The Matplotlib axis object where the correlation plot will
        be drawn. This allows the function to be integrated into
        larger figures or subplot layouts.
    
    dataset : xarray.Dataset or pandas.DataFrame
        The dataset containing the variables to be correlated. It
        must have the two specified variables as columns (if a
        DataFrame) or data variables (if an xarray.Dataset).
    
    variables : list of str
        A list containing the names of the two variables to be
        correlated. The function will plot the correlation between
        these two variables.

    Returns
    -------
    matplotlib.axes.Axes
        Returns the modified axis object with the correlation plot
        drawn on it.

    Notes
    -----
    - The function computes the Pearson correlation coefficient
      and p-value using scipy.stats.pearsonr and displays these
      metrics on the plot for reference.
    - The linear regression line is added to the scatter plot to
      visually assess the relationship between the variables.
    - It is assumed that the input variables are numerical and
      have a linear relationship.
    """
    # Flatten the dataset and exclude NaNs
    if isinstance(dataset, pd.DataFrame):
        df = dataset[[variables[0], variables[1]]].dropna()
    elif isinstance(dataset, xr.Dataset):
        df = dataset.to_dataframe().reset_index().dropna(subset=variables)[variables]
    else:
        raise TypeError("Dataset must be a pandas.DataFrame or xarray.Dataset")
    if len(variables) != 2:
        raise ValueError("Exactly two variables must be specified for correlation.")

    x = df[variables[0]].values
    y = df[variables[1]].values

    corr_coeff, _ = scipy.stats.pearsonr(x, y)
    slope, intercept, r_value, p_value, std_err = scipy.stats.linregress(x, y)
    reg_line_x = np.array([np.min(x), np.max(x)])
    reg_line_y = intercept + slope * reg_line_x

    default_scatter_kwargs = {'marker': '.', 'alpha':0.6, 's':5}
    scatter_kwargs = {key: value for key, value in kwargs.items() if _is_valid_kwarg(ax.scatter, key)}
    scatter_kwargs = {**default_scatter_kwargs, **scatter_kwargs}
    ax.scatter(x, y,  **scatter_kwargs)

    plot_kwargs = {key: value for key, value in kwargs.items()
                   if _is_valid_kwarg(ax.plot, key)}
    label = 'regression, '
    for element in label_elements:
        if element == 'formula':
            label += f"y={slope:.2f}x+{intercept:.2f}, "
        if element == 'p-value':
            label += f"p-value={p_value:.2e}, "
        if element == 'r-value':
            label += f"r-value={r_value:.2f}, "
        if element == 'pearson':
            label += f"corr-coeff={corr_coeff:.2f}, "
        if element == 'std_err':
            label += f"std_err={std_err:.2e}, "
    label = label[0:-2]
    plot_kwargs['label'] = label

    ax.plot(reg_line_x, reg_line_y, **plot_kwargs)
    ax.set_xlabel(variables[0])
    ax.set_ylabel(variables[1])
    ax.legend()
    return ax



# def ax_correlation_matrix(ax, dataset, annotate_values=True, cmap='coolwarm', add_cbar=True):
#     """
#     Plots a correlation matrix on the provided Matplotlib axis using data from an xarray dataset, automatically
#     handling non-numerical values and flattening the data across its dimensions. Adjusts label formatting based
#     on the presence of long_name attributes. Ensures consistent variable order by sorting.

#     Parameters
#     ----------
#     ax : matplotlib.axes.Axes
#         The Matplotlib axis object where the correlation matrix will be plotted. This function is designed to
#         fully utilize the provided axis for the correlation matrix, making it less suitable for combining with
#         other types of plots on the same axis.
    
#     dataset : xarray.Dataset
#         An xarray dataset containing the variables to be correlated. The function will preprocess the dataset
#         to handle non-numerical values and flatten the data, treating it as one-dimensional, before computing
#         the correlation matrix.

#     annotate_values: bool
#         If True (default) annotates every pixel with its value.

#     cmap : str or matplotlib.colors.Colormap
#         The colormap for the correlation matrix plot. Defaults to 'coolwarm'.

#     Returns
#     -------
#     matplotlib.axes.Axes
#         Returns the modified axis object with the correlation matrix plotted on it.
#     """
#     def get_label(var): return dataset[var].attrs.get('long_name', var) # helper function
        
#     if not isinstance(dataset, xr.Dataset): raise ValueError("The dataset must be an xarray.Dataset")
#     df = dataset.to_dataframe().select_dtypes(include=[np.number]).reset_index(drop=True)
#     if df.empty: raise ValueError("The dataset does not contain any numerical data")

#     df = df.sort_index(axis=1)
#     corr_matrix = df.corr()
#     np.fill_diagonal(corr_matrix.values, np.nan)  # Set the diagonal to NaN

#     if add_cbar:
#         divider = mpl_toolkits.axes_grid1.make_axes_locatable(ax)
#         ax_cb = divider.append_axes("right", size="5%", pad=0.05)

#     cax = ax.matshow(corr_matrix, cmap=cmap, vmin=-1, vmax=1)

#     if add_cbar:
#         cbar = plt.colorbar(cax, cax=ax_cb)
#         ax_cb.yaxis.tick_right()
#         ax_cb.yaxis.set_tick_params(labelright=False)
#         cbar.set_label('Pearson correlation coefficient')

#     labels = [get_label(var) for var in df.columns]
#     using_long_names = any('long_name' in dataset[var].attrs for var in df.columns)

#     # Annotate the matrix with correlation values
#     if annotate_values:
#         for i in range(corr_matrix.shape[0]):
#             for j in range(corr_matrix.shape[1]):
#                 if not np.isnan(corr_matrix.iloc[i, j]):
#                     ax.text(j, i, f'{corr_matrix.iloc[i, j]:.2f}', ha='center', va='center', color='black')
                
#     ax.set_xticks(np.arange(len(labels)))
#     ax.set_yticks(np.arange(len(labels)))
#     ax.set_xticklabels(labels, rotation=45 if using_long_names else 90, ha="right" if using_long_names else "center")
#     ax.set_yticklabels(labels)
#     ax.tick_params(axis='x', which='both', bottom=False, top=False, labelbottom=True, labeltop=False)
#     ax.tick_params(axis='y', which='both', left=False, right=False, labelleft=True)
#     ax.grid(False)
    
#     return ax


def ax_correlation_matrix(ax, dataset, annotate_values=True, cmap='coolwarm', add_cbar=True, variables=None):
    """
    Plots a correlation matrix on the provided Matplotlib axis using data from an xarray dataset, automatically
    handling non-numerical values and flattening the data across its dimensions. Adjusts label formatting based
    on the presence of long_name attributes. Ensures consistent variable order by sorting.

    Parameters
    ----------
    ax : matplotlib.axes.Axes
        The Matplotlib axis object where the correlation matrix will be plotted. This function is designed to
        fully utilize the provided axis for the correlation matrix, making it less suitable for combining with
        other types of plots on the same axis.
    
    dataset : xarray.Dataset
        An xarray dataset containing the variables to be correlated. The function will preprocess the dataset
        to handle non-numerical values and flatten the data, treating it as one-dimensional, before computing
        the correlation matrix.

    annotate_values: bool
        If True (default) annotates every pixel with its value.

    cmap : str or matplotlib.colors.Colormap
        The colormap for the correlation matrix plot. Defaults to 'coolwarm'.

    add_cbar : bool
        Whether to add a colorbar to the plot. Defaults to True.

    variables : list of str, optional
        A list of variable names to include in the correlation matrix. If not provided, all numerical variables
        in the dataset will be used.

    Returns
    -------
    matplotlib.axes.Axes
        Returns the modified axis object with the correlation matrix plotted on it.
    """
    def get_label(var): return dataset[var].attrs.get('long_name', var) # helper function
        
    if not isinstance(dataset, xr.Dataset): raise ValueError("The dataset must be an xarray.Dataset")
    
    if variables is not None:
        dataset = dataset[variables]
    
    df = dataset.to_dataframe().select_dtypes(include=[np.number]).reset_index(drop=True)
    if df.empty: raise ValueError("The dataset does not contain any numerical data")

    df = df.sort_index(axis=1)
    corr_matrix = df.corr()
    np.fill_diagonal(corr_matrix.values, np.nan)  # Set the diagonal to NaN

    if add_cbar:
        divider = mpl_toolkits.axes_grid1.make_axes_locatable(ax)
        ax_cb = divider.append_axes("right", size="5%", pad=0.05)

    cax = ax.matshow(corr_matrix, cmap=cmap, vmin=-1, vmax=1)

    if add_cbar:
        cbar = plt.colorbar(cax, cax=ax_cb)
        ax_cb.yaxis.tick_right()
        ax_cb.yaxis.set_tick_params(labelright=False)
        cbar.set_label('Pearson correlation coefficient')

    labels = [get_label(var) for var in df.columns]
    using_long_names = any('long_name' in dataset[var].attrs for var in df.columns)

    # Annotate the matrix with correlation values
    if annotate_values:
        for i in range(corr_matrix.shape[0]):
            for j in range(corr_matrix.shape[1]):
                if not np.isnan(corr_matrix.iloc[i, j]):
                    ax.text(j, i, f'{corr_matrix.iloc[i, j]:.2f}', ha='center', va='center', color='black')
                
    ax.set_xticks(np.arange(len(labels)))
    ax.set_yticks(np.arange(len(labels)))
    ax.set_xticklabels(labels, rotation=45 if using_long_names else 90, ha="right" if using_long_names else "center")
    ax.set_yticklabels(labels)
    ax.tick_params(axis='x', which='both', bottom=False, top=False, labelbottom=True, labeltop=False)
    ax.tick_params(axis='y', which='both', left=False, right=False, labelleft=True)
    ax.grid(False)
    
    return ax


def ax_histogram(ax, dataset, varname, bins=None, mean=True, *args, **kwargs):
    """
    Plot a histogram and a Kernel Density Estimate (KDE) on a given Axes object.

    Parameters:
    - ax: matplotlib.axes.Axes
        The axes on which to plot the histogram and KDE.
    - dataset: dict-like
        A dataset containing the data to be plotted. The dataset should support
        dictionary-like access to variables.
    - varname: str
        The name of the variable in the dataset to be plotted.
    - bins: int or sequence of scalars, optional
        The number of bins or the bin edges for the histogram. If None, the default
        binning strategy is used.
    - mean: bool, default=True
        If True, a vertical line indicating the mean of the data will be plotted.
    - *args: 
        Additional positional arguments passed to the histogram plotting function.
    - **kwargs: 
        Additional keyword arguments passed to the histogram plotting function.
        Common options include 'label' for the KDE line and 'color' for the histogram
        and KDE line.

    Returns:
    - ax: matplotlib.axes.Axes
        The axes object with the histogram and KDE plotted.

    Raises:
    - ValueError: 
        If the specified variable name is not found in the dataset.

    Notes:
    - The function automatically handles NaN and infinite values by excluding them
      from the plot.
    - The KDE is rescaled to match the maximum height of the histogram for better
      visual comparison.
    - The x-axis label is automatically set based on the variable's attributes in
      the dataset, if available.
    """
    if varname not in dataset:
        raise ValueError(f"Variable '{varname}' not found in the dataset.")
    
    hist_kwargs = kwargs.copy()
    label = hist_kwargs.pop('label', 'KDE')
    color = hist_kwargs.pop('color', 'blue')

    values = np.ravel(dataset[varname])
    values = values[np.isfinite(values) & ~np.isnan(values)]   # Drop NaN and inf
    counts, bins, patches = ax.hist(values, bins=bins, alpha=.3, color=color, **hist_kwargs)
    hist_max = max(counts)
    kde = scipy.stats.gaussian_kde(values)
    x = np.linspace(min(values), max(values), 1000)
    y = kde(x)
    y = y * (hist_max / max(y))  # Rescale KDE to match histogram

    ax.plot(x, y, label=label, color=color)
    if mean: 
        mean_value = np.nanmean(values)
        ax.axvline(np.nanmean(values), color=color, ls='--')
        ax.text(mean_value, ax.get_ylim()[1] * 0.9, f'{mean_value:.2f}', color=color, 
                ha='center', va='center', rotation=90, 
                bbox=dict(facecolor='white', edgecolor='none', pad=2))
    
    
    xlabel = dataset[varname].attrs.get('long_name', varname)
    if 'units' in dataset[varname].attrs:
        xlabel += ' / ' + dataset[varname].attrs['units']
    ax.set_xlabel(xlabel)
    ax.legend()
    return ax


def ax_variable_correlation(ax, dataset, variables,
                            label_elements=['pearson'], kwargs={}):
    """
    Plots the correlation between two variables from a dataset on
    a provided Matplotlib axis. The plot includes a scatter plot
    of the variables, a linear regression line, and displays key
    metrics for assessing the correlation's goodness, such as the
    Pearson correlation coefficient and p-value.

    Parameters
    ----------
    ax : matplotlib.axes.Axes
        The Matplotlib axis object where the correlation plot will
        be drawn. This allows the function to be integrated into
        larger figures or subplot layouts.
    
    dataset : xarray.Dataset or pandas.DataFrame
        The dataset containing the variables to be correlated. It
        must have the two specified variables as columns (if a
        DataFrame) or data variables (if an xarray.Dataset).
    
    variables : list of str
        A list containing the names of the two variables to be
        correlated. The function will plot the correlation between
        these two variables.

    Returns
    -------
    matplotlib.axes.Axes
        Returns the modified axis object with the correlation plot
        drawn on it.
    """
    # Flatten the dataset and exclude NaNs
    if isinstance(dataset, pd.DataFrame):
        df = dataset[[variables[0], variables[1]]].dropna()
    elif isinstance(dataset, xr.Dataset):
        df = dataset.to_dataframe().reset_index().dropna(subset=variables)[variables]
    else:
        raise TypeError("Dataset must be a pandas.DataFrame or xarray.Dataset")
    if len(variables) != 2:
        raise ValueError("Exactly two variables must be specified for correlation.")

    x = df[variables[0]].values
    y = df[variables[1]].values

    # Determine point size if not specified in kwargs
    if 's' not in kwargs:
        num_points = len(x)
        if num_points < 100:
            point_size = 20
        elif num_points < 10000:
            point_size = 10
        else:
            point_size = 5
        kwargs['s'] = point_size

    corr_coeff, _ = scipy.stats.pearsonr(x, y)
    slope, intercept, r_value, p_value, std_err = scipy.stats.linregress(x, y)
    reg_line_x = np.array([np.min(x), np.max(x)])
    reg_line_y = intercept + slope * reg_line_x

    default_scatter_kwargs = {'marker': '.', 'alpha':0.6}
    scatter_kwargs = {key: value for key, value in kwargs.items() if _is_valid_kwarg(ax.scatter, key)}
    scatter_kwargs = {**default_scatter_kwargs, **scatter_kwargs}
    ax.scatter(x, y, **scatter_kwargs)

    plot_kwargs = {key: value for key, value in kwargs.items()
                   if _is_valid_kwarg(ax.plot, key)}
    label = 'regression, '
    for element in label_elements:
        if element == 'formula':
            label += f"y={slope:.2f}x+{intercept:.2f}, "
        if element == 'p-value':
            label += f"p-value={p_value:.2e}, "
        if element == 'r-value':
            label += f"r-value={r_value:.2f}, "
        if element == 'pearson':
            label += f"corr-coeff={corr_coeff:.2f}, "
        if element == 'std_err':
            label += f"std_err={std_err:.2e}, "
    label = label[0:-2]
    plot_kwargs['label'] = label

    ax.plot(reg_line_x, reg_line_y, **plot_kwargs)
    ax.set_xlabel(variables[0])
    ax.set_ylabel(variables[1])
    ax.legend()
    return ax