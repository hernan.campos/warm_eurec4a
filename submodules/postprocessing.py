"""
EUREC4A Postprocessing Module

This module provides a collection of functions and utilities for postprocessing EUREC4A data. It includes functions for data cleaning, filtering, and conversion, as well as tools for working with EUREC4A domain grids.

Functions:- add_arithmetic_variable(dataset, var_name1, var_name2, operation_symbol, new_var_name): Performs a specified arithmetic operation between two variables in a dataset and adds the result as a new variable.
- add_grid(dataset, grid_path, clean, drop_vars, rad2deg): Add grid information to an xarray dataset with EUREC4A-LES data.
- add_threshold_indicator(dataset, threshold, new_variable_name, long_name, boolean, below_threshold): Add a threshold indicator variable to the dataset.
- add_time_difference(time, time_difference): Adds a time difference to the given time.
- cells_in_rectangle(grid, rectangle, varnames): Determine grid cells within a specified rectangle.
- clean_dims(dataset): Clean up dimensions in the dataset by removing irrelevant dimensions.
- clean_surface_data(dataset, rename_vars, dim_order, convert_time): Clean and manipulate a EUREC4A-LES surface data xarray dataset.
- clean_volume_data(dataset, rename_vars, dim_order, convert_time): Clean and manipulate a EUREC4A-LES volume data xarray dataset.
- cloud_scenario_cell_average(dataset, threshold, dim): Calculate the average of a dataset over specified cloud scenarios based on a threshold.
- common_timesteps(datasets, time_coord): Find common timesteps, where the datasets overlap.
- convert_coords_deg2rad(dataset, varnames): Convert latitude and longitude coordinates from degrees to radians in an xarray dataset.
- convert_coords_rad2deg(dataset, varnames): Convert latitude and longitude coordinates from radians to degrees in an xarray dataset.
- convert_units(target_unit, data, var): Converts the units of a variable
- count_nans_along_dims(data_array, dim, relative): Count the number of NaN values along specified dimensions in an xarray.DataArray.
- cut_borders(dataset, borders, varnames): Extract data within a specified border rectangle.
- cut_rectangle(data, rectangle, grid, varnames): Extract data within a specified rectangle.
- cut_time_interval(data, interval): Cuts a time interval from the given data based on the specified interval.
- daily_average(ds, time_dim): Calculate daily averaged values for each variable in the given xarray.Dataset and add a quality control variable.
- default_cutout_rectangle(dom): Get the default cutout rectangle for a specified EUREC4A domain.
- disk_data_from_keywords(directory, keywords, exclude, return_file_list): Load data files from a directory based on specified keywords and return as an xarray dataset.
- drop_nan(x): Remove NaN (Not-a-Number) values from an array.
- eurec4a_grid(domain, ncells): Provide an EUREC4A simulation grid based on domain or the number of cells.
- eureca_domain_rectangles(domains): Get rectangle representations of EUREC4A domains.
- exppath2savename(exp_path, basepath, keywords): Generate a save filename based on an experiment path and optional keywords.
- filter_by_threshold(dataset, threshold_dict, below_threshold, chunk_size): Filter dataset based on variable-specific thresholds.
- filter_common_vars(datasets): Filter datasets to include only common variables.
- find_ranges(bool_array): Find continuous ranges of True values in a boolean array.
- get_grid_metadata(): Retrieve metadata about EUREC4A grid domains.
- get_interval(time, time_difference): Produces a sorted interval based on the given time and time difference.
- get_z_full(z_ifc): Retrieves or calculates full-level heights.
- grid_description(rectangle, shape): Create a description for a regular global grid with the specified shape.
- griddescription2dict(description): Convert a grid description string into a dictionary.
- griddescription2name(description): Generate a name for the grid based on its description.
- guess_domain(dataset): Guess the EUREC4A domain name based on dataset attributes or number of cells.
- icon2datetime(icon_dates): Convert ICON format datetime objects to Python datetime objects.
- integrate_over_layer(data, dimension, lower_bound, upper_bound, new_variable_name): Integrate the data over a specified dimension within a given range.
- local_time(dataset): Specific for time shift -4 (UTC to Barbados local time)
- mean_over_layer(data, dimension, lower_bound, upper_bound, new_variable_name): Calculate the mean of data over a specified dimension within a given range.
- open_mfdataset_with_timedoubles(files): Open a multi-file dataset with duplicate time values and return a cleaned dataset.
- open_timeseries(experiment_path, keywords): Open a timeseries dataset, or create and save one if it doesn't exist.
- random_subsample(dataset, dim_samples): Takes a random subsample along specified dimensions in an xarray.Dataset.
- rectangle(argument): Create a rectangle representation.
- rectangle_latlon_extent(rectangle): Get the coordinates of the corners of a matplotlib.patches.Rectangle in terms of y and x (lat and lon) extents.
- regrid_latlon(dataset, resolution, weights_file, varnames): Regrids the dataset to a new latitude-longitude grid with the specified resolution in degree. 
- regular_subsample(dataset, subsampling_dict): Subsample an xarray.Dataset along specified dimensions.
- remove_dimension(dataset, dim_name): Remove all variables along a specified dimension from an xarray.Dataset and also remove the dimension itself.
- remove_duplicates(data, dim): Removes duplicate entries along a specified dimension of an xarray.Dataset
or xarray.DataArray, keeping only the first occurrence of any duplicates.
- remove_single_value_dimensions(dataset, exclude_dims): Remove single-value dimensions from a dataset, excluding specific dimensions.
- rename_dimensions(dataset, dim_mapping): Rename dimensions of variables in an xarray dataset according to the specified dimension mapping.
- rename_variables(ds, var_prefix, long_name_prefix): Rename variables and their long_name attribute in an xarray.Dataset.
- replace_height_indices_with_values(data, height_names): Replace height indices with corresponding values in the dataset.
- replace_icon2datetime(dataset): Replace 'time' coordinate in a dataset with Python datetime objects.
- save_griddescription(description, directory): Save the grid description to a file.
- save_timeseries(experiment_path, keywords): Process and save a timeseries dataset from an experiment path.
- shift_time(dataset, offset, time_var): Shift the time variable in an xarray.Dataset from UTC to local time using a specified UTC offset.
- split_along_threshold(dataset, threshold, dimname): Splits a dataset into three parts based on a specified threshold and concatenates them along a new dimension.
- subsample_dataset(dataset, subsampling_dict): Alias for regular_subsample
- subset_time_overlap(datasets, dimension): Return datasets containing overlapping timesteps from a list of xarray datasets.
- timestamp(date_time): Returns the date in YYYYMMDD format for the given datetime object.
If no datetime object is provided, it defaults to the current date.
- to_datetime64(time): Converts a datetime string to a numpy.datetime64 object.
- to_timedelta64(time_difference): Converts a time difference string to a numpy.timedelta64 object.


Please refer to the individual function docstrings for detailed descriptions and usage instructions.

Author: Hernan Campos
Email: hernan.campos@mpimet.mpg.de
"""

import os
import numpy as np
import xarray as xr
import pandas as pd
import datetime
import eurec4a
from matplotlib.patches import Rectangle
from collections.abc import Iterable
import warnings
import metpy.units
import operator
import easygems

DEFAULT_VARNAMES = {'latitude':'lat',
                    'longitude':'lon',
                    'cell':'cell'
                   }

def _add_varnames_to_default(custom_varnames=None):
    """
    Updates the default variable names dictionary with custom variable names.

    Args:
        custom_varnames (dict): Dictionary containing custom variable names. Defaults to None.

    Returns:
        dict: Dictionary of variable names. Default values are taken from `DEFAULT_VARNAMES` and replaced if present in `custom_varnames`
    """
    global DEFAULT_VARNAMES
    varnames = DEFAULT_VARNAMES.copy()
    if custom_varnames: # if any given, this will overwrite the default
        for key, value in custom_varnames.items(): varnames[key] = value
    return varnames
    

def drop_nan(x):
    """
    Remove NaN (Not-a-Number) values from an array.

    This function takes an input array 'x' and returns a new array that contains only the non-NaN values from 'x'.

    Args:
        x (array-like): The input array that may contain NaN values.

    Returns:
        array-like: An array containing only the non-NaN values from the input array.

    Example:
        >>> input_array = [1.0, 2.0, float('nan'), 4.0, float('nan')]
        >>> cleaned_array = drop_nan(input_array)
        >>> print(cleaned_array)
        [1. 2. 4.]

    Note:
        - Take from https://stackoverflow.com/a/11620982
    """
    return x[~np.isnan(x)]


def icon2datetime(icon_dates):
    """
    Convert ICON format datetime objects to Python datetime objects.

    This function converts datetime objects in the ICON format to Python datetime objects.
    The input 'icon_dates' can be a single value or a collection.

    Args:
        icon_dates (numeric or collection): A single numeric value or a collection of ICON format datetime values.

    Returns:
        pd.DatetimeIndex or pd.Timestamp: A Pandas DatetimeIndex if 'icon_dates' is a collection,
        or a single Pandas Timestamp if 'icon_dates' is a single value.

    Example:
        >>> icon_date = 20011201.5
        >>> python_date = icon2datetime(icon_date)
        >>> print(python_date)
        Timestamp('2001-12-01 12:00:00')

    Note:
        - The function supports conversion of ICON format datetime values to Python datetime objects.
        - ICON format represents dates as numeric values (e.g., 20011201.5 for December 1, 2001, 12:00:00).
        - This function has been taken from the esm_analysis library: https://github.com/pgierz/esm_analysis
    """
    try: icon_dates = icon_dates.values
    except AttributeError: pass

    try: icon_dates = icon_dates[:]
    except TypeError: icon_dates = np.array([icon_dates])

    def _convert(icon_date):
        frac_day, date = np.modf(icon_date)
        frac_day *= 60 ** 2 * 24
        date = str(int(date))
        date_str = datetime.datetime.strptime(date, '%Y%m%d')
        td = datetime.timedelta(seconds=int(frac_day.round(0)))
        return date_str + td

    conv = np.vectorize(_convert)
    try:
        out = conv(icon_dates)
    except TypeError:
        out = icon_dates
    if len(out) == 1:
        return pd.DatetimeIndex(out)[0]
    return pd.DatetimeIndex(out)


def replace_icon2datetime(dataset):
    """
    Replace 'time' coordinate in a dataset with Python datetime objects.

    This function is a wrapper for the 'icon2datetime' function and is used to replace the 'time' coordinate
    in a given xarray dataset with Python datetime objects.

    Args:
        dataset (xarray.Dataset): An xarray dataset containing ICON format 'time' coordinates.

    Returns:
        xarray.Dataset: A new xarray dataset with 'time' coordinates converted to Python datetime objects.
    """
    return dataset.assign_coords({'time': icon2datetime(dataset.time)})

def common_timesteps(datasets, time_coord='time'):
    """
    Find common timesteps, where the datasets overlap.

    This function takes a list of xarray datasets and identifies the timesteps that are common to all datasets.
    The overlapping timesteps are returned as a list.

    Args:
        datasets (list of xarray.Dataset): A list of xarray datasets with time coordinates.
        time_coord (str, optional): The name of the time coordinate in the datasets. Defaults to 'time'.

    Returns:
        list: A list of overlapping timesteps found in the input datasets.

    Example:
        >>> dataset1 = xr.Dataset({'temperature': [25.3, 26.1]}, coords={'time': ['2023-01-01T00:00:00', '2023-01-01T01:00:00']})
        >>> dataset2 = xr.Dataset({'humidity': [60.2, 58.9]}, coords={'time': ['2023-01-01T00:00:00', '2023-01-01T02:00:00']})
        >>> overlapping_times = common_timesteps([dataset1, dataset2])
        >>> print(overlapping_times)

    Note:
        - The function checks for overlapping timesteps among the provided datasets based on the specified time coordinate.
    """
    common_times = set(datasets[0][time_coord].values)
    for dataset in datasets[1:]:
        common_times.intersection_update(dataset[time_coord].values)
    return list(common_times)

def time_overlap(datasets):
    """
    deprecated
    
    use subset_time_overlap instead.
    """
    warnings.warn("This function has been renamed 'subset_time_overlap'.", DeprecationWarning)
    return subset_time_overlap(datasets)


def subset_time_overlap(datasets, dimension='time'):
    """
    Return datasets containing overlapping timesteps from a list of xarray datasets.

    This function takes a list of xarray datasets and returns a modified list that only contains timesteps
    that overlap across all datasets. Each dataset in the output list is subset to include only the overlapping timesteps.

    Args:
        datasets (list of xarray.Dataset): A list of xarray datasets with time coordinates.
        dimension (str): dimension which overlap is selected. Defaults to 'time'. 

    Returns:
        tuple: A tuple of xarray datasets with overlapping timesteps.
    """
    overlapping_times = common_timesteps(datasets)
    overlapping_datasets = []
    
    if not overlapping_times:
        warnings.warn("No overlapping timesteps found across datasets.")
        return None

    for dataset in datasets:
        try:
            dataset_time = pd.to_datetime(dataset[dimension].values)
        except Exception as e:
            warnings.warn(f"Time conversion error in dataset: {e}")
            continue
        
        _, index = np.unique(dataset_time, return_index=True)  # find unique times and their indices
        dataset = dataset.isel({dimension: index})  # select unique entries only
        dataset_time_unique = pd.to_datetime(dataset[dimension].values)  # convert unique times to datetime
        dataset = dataset.sel({dimension: dataset_time_unique.isin(overlapping_times)})  # select overlapping times
        
        if dataset[dimension].size == 0: warnings.warn("No overlapping timesteps found for one of the datasets.")
        
        overlapping_datasets.append(dataset)  # add to list of overlapping datasets

    return tuple(overlapping_datasets)


def remove_single_value_dimensions(dataset, exclude_dims=None):
    """
    Remove single-value dimensions from a dataset, excluding specific dimensions.

    Parameters:
    - dataset (xarray.Dataset): The dataset from which single-value dimensions are to be removed.
    - exclude_dims (list or None): Dimensions to exclude from removal. Default is None.

    Returns:
    xarray.Dataset: The modified dataset with single-value dimensions removed.
    """
    if exclude_dims is None:
        exclude_dims = []
    dims_to_squeeze = [dim for dim in dataset.dims if dim not in exclude_dims]
    single_value_dims = [dim for dim in dims_to_squeeze if not dataset.sizes[dim] > 1]
    return dataset.squeeze(dim=single_value_dims, drop=True)


def clean_surface_data(dataset, 
                       rename_vars={'ncells':'cell'}, 
                       dim_order=['time', 'cell'],
                       convert_time=True):
    """
    Clean and manipulate a EUREC4A-LES surface data xarray dataset.

    This function performs data cleaning and manipulation operations on a surface data xarray dataset. 
    It is meant to stardardise EUREC4A-LES model output. It renames variables, drops dimensions, reorders dimensions, 
    and convert the 'time' variable to Python datetime objects if needed.

    Args:
        dataset (xarray.Dataset): An xarray dataset containing surface data.
        rename_vars (dict, optional): A dictionary for renaming variables (default is {'ncells':'cell'}).
        dim_order (list, optional): A list specifying the desired dimension order (default is ['time', 'cell']).
        convert_time (bool, optional): Whether to convert the 'time' variable to Python datetime objects (default is True).

    Returns:
        xarray.Dataset: A cleaned and manipulated xarray dataset.
    """
    if rename_vars:
        dataset = dataset.rename(rename_vars)
    dataset = remove_single_value_dimensions(dataset, exclude_dims=['time'])
    if dim_order:
        dataset = dataset[dim_order + list(dataset)]
    if convert_time:
        time = icon2datetime(dataset.time.values)
        if not isinstance(time, Iterable): time = [time]
        dataset = dataset.assign_coords(time=time)
    return dataset


def clean_volume_data(dataset, 
                      rename_vars={'ncells':'cell'}, 
                      dim_order=['time', 'cell', 'height'],
                      convert_time=True):
    """
    Clean and manipulate a EUREC4A-LES volume data xarray dataset.

    This function performs data cleaning and manipulation operations on a volume data xarray dataset. 
    It is meant to stardardise EUREC4A-LES model output. It renames variables, drops dimensions, reorders dimensions, 
    and convert the 'time' variable to Python datetime objects if needed.

    Args:
        dataset (xarray.Dataset): An xarray dataset containing volume data.
        rename_vars (dict, optional): A dictionary for renaming variables (default is {'ncells':'cell'}).
        dim_order (list, optional): A list specifying the desired dimension order (default is ['time', 'cell', 'height', 'bnds']).
        convert_time (bool, optional): Whether to convert the 'time' variable to Python datetime objects (default is True).

    Returns:
        xarray.Dataset: A cleaned and manipulated xarray dataset.
    """
    if rename_vars:  dataset = dataset.rename(rename_vars)
    try:
        dataset = dataset.drop("height_2").rename({"height_2": "height"})
    except KeyError:
        pass
        # print("Column 'height_2' does not exist. Continuing without renaming.")
    if dim_order:    dataset = dataset[dim_order + list(dataset)]
    if convert_time:
        time = icon2datetime(dataset.time.values)
        if not isinstance(time, Iterable): time = [time]
        dataset = dataset.assign_coords(time=time)
    return dataset


def eurec4a_grid(domain='DOM01', ncells=None):
    """
    Provide an EUREC4A simulation grid based on domain or the number of cells.

    If 'ncells' is specified, the function retrieves the grid corresponding to the specified number of cells.
    If 'ncells' is not provided, the function returns the grid associated with the specified domain.
    By default the grid for domain one (`DOM01`) is returned.

    Args:
        domain (str, optional): The domain identifier ('DOM01' or 'DOM02') for the desired simulation grid (default is 'DOM01').
        ncells (int, optional): The number of cells for the desired simulation grid (e.g., 4528560 or 11792076).

    Returns:
        xarray.Dataset: An xarray dataset representing the simulation grid.

    Raises:
        ValueError: If the provided 'ncells' value cannot be associated with a grid, or if an unknown 'domain' is specified.

    Example:
        >>> # Get the simulation grid for 'DOM01'
        >>> grid_dom01 = eurec4a_grid(domain='DOM01')

        >>> # Get the simulation grid for 4528560 cells
        >>> grid_4528560 = eurec4a_grid(ncells=4528560)
    """
    grids = eurec4a.get_intake_catalog().simulations.grids
    if ncells:
        if ncells == 4528560:
            return grids[list(grids)[0]].to_dask()
        if ncells == 11792076:
            return grids[list(grids)[1]].to_dask()
        else:
            raise ValueError("ncells could not be connected with a grid")
    else:
        if domain == 'DOM01':
            return grids[list(grids)[0]].to_dask()
        elif domain == 'DOM02':
            return grids[list(grids)[1]].to_dask()
        else:
            raise ValueError("Unknown domain. Choose between 'DOM01' and 'DOM02'.")

            

def add_grid(dataset, grid_path=None, clean=True, drop_vars=True, rad2deg=True):
    """
    Add grid information to an xarray dataset with EUREC4A-LES data.

    This function adds grid information to data from EUREC4A-LES simulations. The grid information
    is incorporated into the dataset, and optionally, certain variables and dimensions can be cleaned or removed.

    Args:
        dataset (xarray.Dataset): The xarray dataset to which grid information will be added.
        grid_path (str): Path to grid file. Defaults to None. A grid will then be guessed based on the number of cells via `eurec4a_grid`.
        clean (bool, optional): If True, perform data cleaning on the dataset (default is True).
        drop_vars (bool, optional): If True, drop grid-related variables after adding grid information (default is True).
        rad2deg (bool, optional): If True, convert latitude and longitude coordinates to degree (default is True).

    Returns:
        xarray.Dataset: An updated xarray dataset with added grid information.

    Example:
        >>> # Load a dataset
        >>> dataset = load_surface_data("surface_data.nc")
        >>> # Add grid information and perform cleaning
        >>> updated_dataset = add_grid(dataset, clean=True, drop_vars=True)
        >>> print(updated_dataset)
    """
    if clean: drop_vars = True
    if grid_path == None:
        try: ncells = len(dataset.cell)
        except: ncells = len(dataset.ncells)
        grid = eurec4a_grid(ncells=ncells)
    else:
        grid = xr.open_dataset(grid_path)
    grid_vars = list(grid)
    if clean:
        grid = grid.drop_vars(['elat', 'elon', 'vlat', 'vlon'])
        grid = grid.rename({'clon': 'lon', 'clat': 'lat'})
    if rad2deg:
        varnames = {'latitude': 'clat', 'longitude': 'clon'}
        if clean: varnames = {'latitude': 'lat', 'longitude': 'lon'}
        grid = convert_coords_rad2deg(grid, varnames=varnames)
    dataset = xr.merge((grid, dataset))
    if drop_vars:
        dataset = dataset.drop_vars(grid_vars)
    return dataset

def convert_coords_rad2deg(dataset, varnames={'latitude': 'lat', 'longitude': 'lon'}):
    """
    Convert latitude and longitude coordinates from radians to degrees in an xarray dataset.

    Parameters:
    - dataset (xarray.Dataset): The dataset containing latitude and longitude coordinates.
    - coord_names (dict, optional): A dictionary specifying the variable names for latitude and longitude.
        Defaults to {'latitude': 'lat', 'longitude': 'lon'}.
    """
    for coord in [varnames['latitude'], varnames['longitude']]:
        dataset[coord].values = np.rad2deg(dataset[coord].values)
        dataset[coord].attrs['units'] = 'degree'
    return dataset

def convert_coords_deg2rad(dataset, varnames={'latitude': 'lat', 'longitude': 'lon'}):
    """
    Convert latitude and longitude coordinates from degrees to radians in an xarray dataset.

    Parameters:
    - dataset (xarray.Dataset): The dataset containing latitude and longitude coordinates.
    - coord_names (dict, optional): A dictionary specifying the variable names for latitude and longitude.
        Defaults to {'latitude': 'lat', 'longitude': 'lon'}.
    """
    for coord in [varnames['latitude'], varnames['longitude']]:
        dataset[coord].values = np.deg2rad(dataset[coord].values)
        dataset[coord].attrs['units'] = 'radian'
    return dataset


def rectangle(argument):
    """
    Create a rectangle representation.

    This function allows you to create a rectangle representation from different types of input. The representation can be derived 
    from an xarray dataset (grid) or a list of lat-lon coordinates (either as a list, a pair of latitudinal and longitudinal boundaries, or a dictionary with 'lat' and 'lon' keys).

    Args:
        argument (xr.Dataset, list, tuple, dict, or iterable): The input argument to create a rectangle representation.

    Returns:
        tuple: A tuple representing a rectangle, which includes (min_lat, max_lat, min_lon, max_lon).

    Raises:
        ValueError: If the input argument is of an invalid type or has an incorrect length.

    Example:
        >>> # Create a rectangle from an xarray dataset
        >>> grid = eurec4a_grid(domain='DOM01')
        >>> rect = rectangle(grid)

        >>> # Create a rectangle from a list of latitudes and longitudes
        >>> lat_lon_list = [8.0, 9.0, -60.0, -59.0]
        >>> rect = rectangle(lat_lon_list)

        >>> # Create a rectangle from a nested iterable
        >>> lat_lon_iterable = ((8.0, 9.0), (-60.0, -59.0))
        >>> rect = rectangle(lat_lon_iterable)
        
        >>> # Create a rectangle from a dictionary
        >>> limits = {'lat':(0.130, 0.134), 'lon':(-0.820, -0.810)}
        >>> rect = rectangle(limits)

    Note:
        - The rectangle representation is in the form of (min_lat, max_lat, min_lon, max_lon).
    """
    
    if type(argument) == type(xr.Dataset()):
        return _grid2rectangle(argument)
    elif isinstance(argument, dict):
        if 'lat' in argument and 'lon' in argument:
            lat_limits = argument['lat']
            lon_limits = argument['lon']
            if len(lat_limits) == 2 and len(lon_limits) == 2:
                sorted_argument = (lat_limits + lon_limits)
                return _list2rectangle(sorted_argument)
            else:
                raise ValueError('argument length mismatch. Latitude and longitude limits should each have a length of 2.')
        else:
            raise ValueError('argument dictionary must contain "lat" and "lon" keys')
    elif isinstance(argument, Iterable):
        # Check if argument is flat or nested iterable
        if any(isinstance(item, Iterable) for item in argument):
            if len([item for sublist in argument for item in sublist]) == 4:
                sorted_argument = [item for sublist in argument for item in sorted(sublist)]
                return _list2rectangle(sorted_argument)
            else:
                raise ValueError('argument length mismatch. Input should be either (min_lat, max_lat, min_lon, max_lon) or ((min_lat, max_lat), (min_lon, max_lon))')
        else:
            # If argument is flat iterable, assume it's a list of lat-lon coordinates
            if len(argument) == 4:
                sorted_argument = sorted(argument)
                return _list2rectangle(sorted_argument)
            else:
                raise ValueError('argument length mismatch. Input should be a list of length 4 representing (min_lat, max_lat, min_lon, max_lon)')
    else:
        raise ValueError('Input not valid')

        

def _list2rectangle(argument):
    """
    Create a rectangle representation from a list of latitudes and longitudes.

    This function is used internally to create a rectangle representation from a list of four values
    specifying latitudes and longitudes.

    Args:
        argument (list): A list containing (min_lat, max_lat, min_lon, max_lon).

    Returns:
        matplotlib.patches.Rectangle: A rectangle representation.

    Example:
        >>> lat_lon_list = [8.0, 9.0, -60.0, -59.0]
        >>> rect = _list2rectangle(lat_lon_list)
    """
    min_lat, max_lat, min_lon, max_lon = argument
    anker = (min_lon, min_lat)
    extent = (max_lon - min_lon, max_lat - min_lat)
    return Rectangle(anker, *extent)


def _grid2rectangle(grid):
    """
    Create a rectangle representation from an xarray dataset (grid).

    This function is used internally to create a rectangle representation from an xarray dataset
    representing a grid.

    Args:
        grid (xr.Dataset): An xarray dataset representing the grid.

    Returns:
        matplotlib.patches.Rectangle: A rectangle representation.

    Example:
        >>> grid = eurec4a_grid(domain='DOM01')
        >>> rect = _grid2rectangle(grid)
    """
    lonlim = [np.rad2deg(l) for l in (np.min(grid.clon.values), np.max(grid.clon.values))]
    latlim = [np.rad2deg(l) for l in (np.min(grid.clat.values), np.max(grid.clat.values))]
    anker = (lonlim[0], latlim[0])
    extent = (lonlim[1] - lonlim[0], latlim[1] - latlim[0])
    return Rectangle(anker, *extent)


# https://matplotlib.org/stable/api/_as_gen/matplotlib.patches.Rectangle.html
def _rectangle2yspan(rectangle):
    return (rectangle.get_y(), rectangle.get_height() + rectangle.get_y())
def _rectangle2xspan(rectangle):
    return (rectangle.get_x(), rectangle.get_width() + rectangle.get_x())

def rectangle_latlon_extent(rectangle):
    """
    Get the coordinates of the corners of a matplotlib.patches.Rectangle in terms of y and x (lat and lon) extents.

    Parameters:
    rect (matplotlib.patches.Rectangle): The Rectangle object.

    Returns:
    tuple: A tuple containing two tuples of (min_y, max_y) and (min_x, max_x) coordinates.
    """
    return _rectangle2yspan(rectangle), _rectangle2xspan(rectangle)

def cells_in_rectangle(grid, rectangle, varnames=None):
    """
    Determine grid cells within a specified rectangle.

    This function calculates grid cells that fall within a specified rectangle defined by its latitude and longitude spans.

    Args:
        grid (xr.Dataset): The grid dataset for the domain.
        rectangle (matplotlib.patches.Rectangle): A rectangle representation.
        varnames (dict, optional): A dictionary mapping the dimension names. Default see _add_varnames_to_default.

    Returns:
        xarray.DataArray: A boolean array indicating which cells are inside the specified rectangle.

    Example:
        >>> # Get the grid cells within a rectangle
        >>> rectangle = eureca_domain_rectangles(domains=['DOM01'])[0]
        >>> grid = xr.open_zarr("https://example.com/grid_data.zarr")
        >>> inside_cells = cells_in_rectangle(grid, rectangle)
        >>> print(inside_cells)
    """
    varnames = _add_varnames_to_default(varnames)
    minlat, maxlat = _rectangle2yspan(rectangle)
    minlon, maxlon = _rectangle2xspan(rectangle)
    # # if latlon_in_degree:
    # if grid[varnames['latitude']].attrs['units'] == 'radian':
    #     for coord in [varnames['latitude'], varnames['longitude']]: grid[coord].values = np.rad2deg(grid[coord].values)
    is_inside = (
        (grid[varnames['latitude']] > minlat) &
        (grid[varnames['latitude']] < maxlat) &
        (grid[varnames['longitude']] > minlon) &
        (grid[varnames['longitude']] < maxlon)
    ).compute()
    
    # # if latlon_in_degree: change back
    # if grid[varnames['latitude']].attrs['units'] == 'radian':
    #     for coord in [varnames['latitude'], varnames['longitude']]: grid[coord].values = np.deg2rad(grid[coord].values)
    return is_inside


def eureca_domain_rectangles(domains=['DOM01','DOM02']):
    """
    Get rectangle representations of EUREC4A domains.

    This function retrieves rectangle representations of specified EUREC4A domains. The rectangles
    are derived from grid data for the domains.

    Args:
        domains (list, optional): A list of EUREC4A domains to retrieve rectangles for (default is ['DOM01', 'DOM02']).

    Returns:
        list: A list of rectangle representations for the specified domains.

    Example:
        >>> # Get rectangles for specific EUREC4A domains
        >>> rectangles = eureca_domain_rectangles(domains=['DOM01', 'DOM02'])
        >>> print(rectangles)
    """
    rectangles = list()
    for dom in domains:
        grid = xr.open_zarr(f"https://swift.dkrz.de/v1/dkrz_948e7d4bbfbb445fbff5315fc433e36a/grids/EUREC4A_PR1250m_{dom}.zarr")
        rectangles.append(rectangle(grid))
    return rectangles


def cut_rectangle(data, rectangle, grid=False, varnames=None):
    """
    Extract data within a specified rectangle.

    This function extracts data within a specified rectangle defined by its latitude and longitude spans.
    
    Args:
        data (xr.Dataset): The dataset containing the data to be extracted.
        rectangle (matplotlib.patches.Rectangle): A rectangle representation.
        grid (xr.Dataset, optional): The grid dataset for the domain (default is None, which means using the same dataset as data).
        varnames (dict, optional): A dictionary mapping dimension names. For defaults see _add_varnames_to_default.

    Returns:
        xr.Dataset: A subset of the input dataset containing data within the specified rectangle.

    Example:
        >>> # Extract data within a rectangle
        >>> rectangle = default_cutout_rectangle(dom='DOM01')
        >>> data = xr.open_zarr("https://example.com/data.zarr")
        >>> extracted_data = cut_rectangle(data, rectangle)
        >>> print(extracted_data)
    """
    varnames = _add_varnames_to_default(varnames)
    if not grid: grid = data.copy()
    return data.sel({varnames['cell']: cells_in_rectangle(grid, rectangle, varnames)})
                                  
                 
def cut_borders(dataset, borders=False, varnames=None):
    """
    Extract data within a specified border rectangle.

    If no borders are provided, it uses the default rectangle for 'DOM02'.

    Args:
        dataset (xr.Dataset): The dataset containing the data to be extracted.
        borders (matplotlib.patches.Rectangle, optional): The border rectangle to extract data within (default is None, which means using the default rectangle for 'DOM02').
        varnames (dict, optional): A dictionary mapping dimension names. For defaults see _add_varnames_to_default.
        
    Returns:
        xr.Dataset: A subset of the input dataset containing data within the specified border rectangle.

    Example:
        >>> # Extract data within border rectangle
        >>> dataset = xr.open_dataset("data.nc")
        >>> borders = default_cutout_rectangle('DOM02')
        >>> extracted_data = cut_borders(dataset, borders)
        >>> print(extracted_data)
    """
    varnames = _add_varnames_to_default(varnames)
    if not borders: borders = default_cutout_rectangle('DOM02')
    return cut_rectangle(dataset, borders, varnames=varnames)


def default_cutout_rectangle(dom='DOM01'):
    """
    Get the default cutout rectangle for a specified EUREC4A domain.

    This function provides the default rectangle representation for a specified EUREC4A domain.

    Args:
        dom (str, optional): The EUREC4A domain for which to retrieve the default rectangle (default is 'DOM01').

    Returns:
        matplotlib.patches.Rectangle: A rectangle representation.

    Example:
        >>> # Get the default cutout rectangle for a specific EUREC4A domain
        >>> default_rect = default_cutout_rectangle(dom='DOM01')
        >>> print(default_rect)
    """
    if dom == 'DOM01':
        anker = (-59.25, 8.125)
        extent = (13.00, 8.000)
    elif dom == 'DOM02':
        anker = (-59.25, 9.50)
        extent = (11.50, 6.00)
    else:
        raise ValueError('Domain not recognized')
    return Rectangle(anker, *extent)


def guess_domain(dataset):
    """
    Guess the EUREC4A domain name based on dataset attributes or number of cells.

    This function attempts to determine the EUREC4A domain name based on dataset attributes or the number of cells.
    It first checks for a matching UUID attribute in the dataset. If that's not found, it tries to match the number of cells.

    Args:
        dataset (xr.Dataset): The dataset for which to guess the EUREC4A domain.

    Returns:
        str: The guessed EUREC4A domain name.

    Raises:
        ValueError: If no matching domain is found based on UUID or number of cells.

    Example:
        >>> # Guess the EUREC4A domain from a dataset
        >>> dataset = xr.open_dataset("data.nc")
        >>> domain = guess_domain(dataset)
        >>> print(domain)
    """
    grid_metadata = get_grid_metadata()
    try: # if we can get a grid uuid, we will try to match it
        grid_uuid = dataset.attrs['uuidOfHGrid']
        for dom in grid_metadata:
            if dom['grid_uuid'] == grid_uuid:
                return dom['name']
        # only if we have and uuid AND it does not match:
        raise ValueError('No grid with a matching UUID found.')
    except: # if we do not have a uuid, we try guessing via number of cells
        try: # the dimension should have either this name
            cell_dim_name = 'ncells'
            ncells = dataset.dims[cell_dim_name]
            for dom in grid_metadata:
                if dom['ncells'] == ncells:
                    return dom['name']
            raise ValueError('No grid with a matching number of cells found.')
        except:
            try: # or this one
                cell_dim_name = 'cell'
                ncells = dataset.dims[cell_dim_name]
                for dom in grid_metadata:
                    if dom['ncells'] == ncells:
                        return dom['name']
                raise ValueError('Could not determine the number of cells.')
            except:
                raise ValueError('Could not determine the number of cells')

                
def clean_dims(dataset):
    """
    Clean up dimensions in the dataset by removing irrelevant dimensions.

    This function removes dimensions in the dataset that are not 'time' or 'cell'. It helps reduce the dimensions to those that are relevant for analysis.

    Args:
        dataset (xr.Dataset): The dataset to clean up.

    Returns:
        xr.Dataset: A cleaned-up dataset with irrelevant dimensions removed.

    Example:
        >>> # Clean up dimensions in the dataset
        >>> dataset = xr.open_dataset("data.nc")
        >>> cleaned_data = clean_dims(dataset)
        >>> print(cleaned_data)
    """
    try: dataset = dataset.rename({'ncells':'cell'})
    except: pass
    useless_dims = list(dataset.dims)
    useless_dims.remove('time')
    useless_dims.remove('cell')
    return dataset.median(dim=useless_dims)


def exppath2savename(exp_path, basepath='data/', keywords=False):
    """
    Generate a save filename based on an experiment path and optional keywords.

    This function generates a save filename for a timeseries dataset based on an experiment path and optional keywords.

    Args:
        exp_path (str): The experiment path used for generating the filename.
        basepath (str, optional): The base path for saving the file (default is 'data/').
        keywords (list, optional): List of keywords to append to the filename (default is False).

    Returns:
        str: The generated save filename.

    Example:
        >>> # Generate a save filename for an experiment
        >>> exp_path = "experiment_data/"
        >>> save_name = exppath2savename(exp_path, keywords=['keyword1', 'keyword2'])
        >>> print(save_name)
    """
    savename = os.path.join(basepath, f'timeseries_{os.path.basename(exp_path)}.nc')
    if keywords: savename += '_' + '_'.join(keywords)
    return savename


def open_timeseries(experiment_path, keywords=['DOM01', 'surface']):
    """
    Open a timeseries dataset, or create and save one if it doesn't exist.

    This function opens a timeseries dataset if it already exists; otherwise, it creates the dataset by saving data from the specified experiment path.

    Args:
        experiment_path (str): The path to the experiment data.
        keywords (list, optional): List of keywords for generating the save filename (default is ['DOM01', 'surface']).

    Returns:
        xr.Dataset: The timeseries dataset.

    Example:
        >>> # Open an existing timeseries dataset or create and save one if it doesn't exist
        >>> dataset = open_timeseries("experiment_data/", keywords=['keyword1', 'keyword2'])
        >>> print(dataset)
    """
    if not os.path.isfile(exppath2savename(experiment_path)):
        save_timeseries(experiment_path, keywords=keywords)
    return xr.open_dataset(exppath2savename(experiment_path))


def get_grid_metadata():
    """
    Retrieve metadata about EUREC4A grid domains.

    Returns a list of dictionaries, each containing information about a grid domain.
    The dictionary includes the domain name, grid UUID, and the number of cells in the grid.

    Returns:
        list: A list of dictionaries, each representing a grid domain and its metadata.

    Example:
        >>> grid_metadata = get_grid_metadata()
        >>> for domain in grid_metadata:
        >>>     print(f"Domain: {domain['name']}, Grid UUID: {domain['grid_uuid']}, Num of Cells: {domain['ncells']}")
    """
    # taken from eurec4a.get_intake_catalog().simulations.grids
    return [{'name':'DOM01', 'grid_uuid':'6b59890b-99f3-939b-e76a-0a3ad2e43140', 'ncells':4528560},
            {'name':'DOM02', 'grid_uuid':'3c7523ba-f78c-c23e-6654-012805ba3300', 'ncells':11792076},
            {'name':'DOM03', 'grid_uuid':'88fe7c0a-f61d-e582-d5e1-b00866e33000', 'ncells':24469588}]


def disk_data_from_keywords(directory, keywords, exclude=False, return_file_list=False):
    """
    Load data files from a directory based on specified keywords and return as an xarray dataset.

    Args:
        directory (str): The directory path containing the data files.
        keywords (list): List of keywords to filter files by.
        exclude (bool, optional): Whether to exclude certain files based on keywords (default is False).
        return_file_list (bool, optional): Whether to return a list of file paths instead of a dataset (default is False).

    Returns:
        xr.Dataset or list: The dataset containing the data files that match the keywords, or a list of file paths.

    Example:
        >>> data = disk_data_from_keywords("data_directory/", keywords=['keyword1', 'keyword2'])
        >>> print(data)
        
    Note:
        - This is a hard copy of a function in file_handling. It is here to avoid interdependency of my homegrown modules. 
    """
    def match(string, keywords):
        checklist = [k in string for k in keywords]
        return all(checklist)

    files = [f for f in os.listdir(directory) if match(f, keywords)]
    # for debugging purposes you can get the file list instead of the dataset
    if return_file_list: return files
    if exclude: 
        for x in exclude:
            files = [f for f in files if not x in files]
    files = [os.path.join(directory, f) for f in files]
    dataset = xr.open_mfdataset(files)
    return dataset



# Note: The next function seems to be similar to one provided earlier (save_timeseries). Consider whether it's necessary.

def save_timeseries(experiment_path, keywords=['DOM01', 'surface']):
    """
    Process and save a timeseries dataset from an experiment path.

    This function loads data, processes it by replacing time values, cleaning dimensions, adding grid information, 
    cutting borders, and computing the mean. The processed data is then saved as a NetCDF file.

    Args:
        experiment_path (str): The path to the experiment data.
        keywords (list, optional): List of keywords for generating the save filename (default is ['DOM01', 'surface']).

    Example:
        >>> save_timeseries("experiment_data/", keywords=['keyword1', 'keyword2'])
    """
    data = disk_data_from_keywords(experiment_path, keywords)
    data = replace_icon2datetime(data)
    data = clean_dims(data)
    data = add_grid(data)
    data = cut_borders(data)
    data = data.mean(dim='cell')
    data.to_netcdf(exppath2savename(experiment_path))


def open_mfdataset_with_timedoubles(files):
    """
    Open a multi-file dataset with duplicate time values and return a cleaned dataset.

    This function opens a multi-file dataset, identifies duplicate time values, and returns a cleaned dataset with unique time values.

    Args:
        files (list): List of file paths for the multi-file dataset.

    Returns:
        xr.Dataset: The cleaned dataset with unique time values.

    Example:
        >>> files = ["file1.nc", "file2.nc"]
        >>> dataset = open_mfdataset_with_timedoubles(files)
        >>> print(dataset)
    """
    data = xr.open_mfdataset(files, concat_dim='time', combine='nested')
    data = data.isel(time=np.unique(data.time, return_index=True)[1])
    return data


def get_z_full(z_ifc=None): # Copied from model calculations to avoid interdependencies in the module files
    """
    Retrieves or calculates full-level heights.

    Args:
    - z_ifc (numpy.ndarray or None): Array of half-level heights. If None, defaulting to data from eurec4a intake catalog.

    Returns:
    - numpy.ndarray: Full-level heights.
    """
    def interpolate_full_level(half_level):
        return np.asarray([(half_level[i] + half_level[i - 1]) / 2 for i in range(1, len(half_level))])
    
    if z_ifc is None: # default case
        try: 
            cat = eurec4a.get_intake_catalog()['simulations']['grids']
            d = cat['ecf22d17-dcee-1510-a807-11ae4a612be0'].to_dask()
            d = d['z_ifc'].median(dim='cell')
            z_ifc = np.flip(d.values)
        except Exception as e:
            print(f"Failed to retrieve z_ifc data: {e}")
    z_full = interpolate_full_level(z_ifc)
    return z_full


def replace_height_indices_with_values(data, height_names=None):
    """
    Replace height indices with corresponding values in the dataset.

    This function replaces the height indices in the dataset with the corresponding height values.
    It iterates over the specified height names and replaces their indices with the corresponding values.
    The height values are retrieved from a predefined source.

    Parameters:
    - data (xarray.Dataset): The dataset containing the height indices to be replaced.
    - height_names (list, optional): A list of strings specifying the names of height variables to be processed.
      Defaults to every dimesions having 'height' in their name.

    Returns:
    - xarray.Dataset: The dataset with height indices replaced by their corresponding values.
    """
    if height_names == None: height_names = [name for name in data.dims if 'height' in name]
    for height_name in height_names:
        replacements = np.flip(get_z_full()[range(len(data[height_name].values))])
        data = data.assign_coords({height_name: replacements})
        data[height_name].attrs = { 'long_name': 'geometric height at level center, interpolated from half level center', 
                                                    'standard_name': 'geometric_height_at_level_center', 
                                                    'units': 'm'}
    return data


def grid_description(rectangle, shape):
    """
    Create a description for a regular global grid with the specified shape.

    This function generates a description for a regular global grid based on the provided Rectangle object and shape.
    The shape parameter should be a tuple of two integers representing the number of grid cells along the y and x axes, respectively.

    Parameters:
    - rectangle (matplotlib.patches.Rectangle): The Rectangle object defining the spatial extent of the grid.
    - shape (tuple): A tuple of two integers representing the shape of the grid in terms of the number of grid cells
      along the y and x axes, respectively.

    Returns:
    - str: A string containing a description of the grid in a format suitable for configuration files or documentation.

    Raises:
    - ValueError: If the shape parameter is not a tuple of two integers.

    Example:
    min_lat, max_lat, min_lon, max_lon = 13.0, 16.0, -47.5, -50.0
    anker = (min_lon, min_lat)
    extent = (max_lon - min_lon, max_lat - min_lat)
    rect = matplotlib.patches.Rectangle(anker, *extent)
    description  = grid_description(rect, (1,100))
    griddes_file = save_griddescription(description, tempdir)
    """
    if isinstance(shape, tuple) and len(shape) == 2 and all(isinstance(val, int) for val in shape):
        ysize, xsize = shape
    else: raise ValueError("shape must be a pair of integers.")

    lat_ext, lon_ext = rectangle_latlon_extent(rectangle)
    yfirst = lat_ext[0]
    xfirst = lon_ext[0]
    xinc = (lon_ext[1] - lon_ext[0]) / xsize
    yinc = (lat_ext[1] - lat_ext[0]) / ysize
 
    lines = [
        f'gridtype  = lonlat',
        f'gridsize  = {int(xsize * ysize)}',
        f'xsize     = {int(xsize)}',
        f'ysize     = {int(ysize)}',
        f'xfirst    = {xfirst}',
        f'xinc      = {xinc}',
        f'yfirst    = {yfirst}',
        f'yinc      = {yinc}',
        'xname     = lon',
        'xlongname = "longitude"',
        'xunits    = "degrees_east"',
        'yname     = lat',
        'ylongname = "latitude"',
        'yunits    = "degrees_north"',
        '']
    return "\n".join(lines)


def griddescription2dict(description):
    """
    Convert a grid description string into a dictionary.

    This function parses a grid description string and converts it into a dictionary format, 
    where each line of the description represents a key-value pair in the dictionary.

    Parameters:
    - description (str): A string containing the grid description.

    Returns:
    - dict: A dictionary containing the parsed key-value pairs from the grid description string.
    """
    description = description.split('\n')
    dictionary = dict()
    for line in description:
        try:
            key, value = line.split('=')
            dictionary[key.strip()] = value.strip()
        except: pass
    return dictionary


def griddescription2name(description):
    """
    Generate a name for the grid based on its description.

    This function generates a name for the grid based on the provided grid description.
    The generated name includes information about the grid type, size, latitude span, and longitude span.

    Parameters:
    - description (str): A string containing the grid description.

    Returns:
    - str: A formatted name for the grid based on its description.
    """
    des = griddescription2dict(description)
    size    = f"{int(des['xsize'])}x{int(des['ysize'])}"
    latspan = f"{float(des['yfirst']):.1f}to{float(des['yfirst']) + float(des['ysize']) * float(des['yinc']):.1f}N"
    lonspan = f"{float(des['xfirst']):.1f}to{float(des['xfirst']) + float(des['xsize']) * float(des['xinc']):.1f}E"
    return f"grid_{des['gridtype']}_{size}_{latspan}_{lonspan}.txt"


def save_griddescription(description, directory='/scratch/m/m300872/temp/'):
    """
    Save the grid description to a file.

    This function saves the provided grid description to a file with a name generated based on the description.
    The file is saved in the specified directory or the default directory if not provided.

    Parameters:
    - description (str): A string containing the grid description.
    - directory (str, optional): The directory where the file will be saved. Defaults to '/scratch/m/m300872/temp/'.

    Returns:
    - str: The file path of the saved grid description.
    """
    file_name = directory + griddescription2name(description)
    with open(file_name, 'w') as f:
        f.write(description)
    return file_name




def to_datetime64(time):
    """
    Converts a datetime string to a numpy.datetime64 object.

    Parameters:
        time (numpy.datetime64 or str): The time to convert. If it's already a numpy.datetime64 object,
            it's returned as is. If it's a datetime string, it's converted to a numpy.datetime64 object.

    Returns:
        numpy.datetime64: The resulting numpy.datetime64 object.
    """
    if isinstance(time, str):
        return np.datetime64(pd.to_datetime(time))  # pandas is laissez-faire with the input strings
    else: 
        return time

def to_timedelta64(time_difference):
    """
    Converts a time difference string to a numpy.timedelta64 object.

    Parameters:
        time_difference (numpy.timedelta64 or str): The time difference to convert. If it's already
            a numpy.timedelta64 object, it's returned as is. If it's a time difference string,
            it's converted to a numpy.timedelta64 object.

    Returns:
        numpy.timedelta64: The resulting numpy.timedelta64 object.
    """
    if isinstance(time_difference, str):
        return np.timedelta64(pd.to_timedelta(time_difference)) # pandas is laissez-faire with the input strings
    else: 
        return time_difference

def cut_time_interval(data, interval):
    """
    Cuts a time interval from the given data based on the specified interval.

    Parameters:
        data (xarray.Dataset): The dataset containing time series data.
        interval (tuple): A tuple representing the time interval to cut from the data.
            It should be in the format (start_time, end_time), where start_time and end_time
            can be either numpy.datetime64 objects or datetime strings.

    Returns:
        xarray.Dataset: A subset of the input data containing only the time series data within
            the specified time interval.
    """
    start_time, end_time = map(to_datetime64, interval) # Convert start_time and end_time to numpy.datetime64
    is_inside = ((data['time'] > start_time) & (data['time'] < end_time)).compute()
    return data.sel({'time': is_inside})


def add_time_difference(time, time_difference):
    """
    Adds a time difference to the given time.

    Parameters:
        time (numpy.datetime64 or str): The time to which the time difference will be added.
            It can be either a numpy.datetime64 object or a datetime string.
        time_difference (numpy.timedelta64 or str): The time difference to be added to the time.
            It can be either a numpy.timedelta64 object or a time difference string.

    Returns:
        numpy.datetime64: The resulting time after adding the time difference.
    """
    return to_datetime64(time) + to_timedelta64(time_difference)


def get_interval(time, time_difference):
    """
    Produces a sorted interval based on the given time and time difference.

    Parameters:
        time (numpy.datetime64 or str): The reference time.
        time_difference (numpy.timedelta64 or str): The time difference from the reference time.

    Returns:
        tuple: A tuple representing the sorted interval, in the format (start_time, end_time),
        where start_time is the minimum of (time, time + time_difference), and end_time is the maximum.
    """
    # Convert time and time_difference to numpy.datetime64 and numpy.timedelta64 respectively
    time = to_datetime64(time)
    time_difference = to_timedelta64(time_difference)
    # Calculate start_time and end_time
    start_time = min(time, time + time_difference)
    end_time = max(time, time + time_difference)
    return start_time, end_time


def convert_units(target_unit, data, var=None):
    """
    Converts the units of a variable
    
    It accepts an xarray.DataArray or xarray.Dataset object. Makes use of MetPy for conversion

    Args:
        data (xarray.DataArray or xarray.Dataset): Input data array or dataset with units attribute.
        target_unit (str or pint.Unit): Target unit to convert to.
        var_name (str, optional): Name of the variable whose units need to be converted. Required only for xarray.Dataset.

    Returns:
        xarray.DataArray or xarray.Dataset: DataArray or Dataset with units converted to the target unit.
    """
    if isinstance(data, xr.DataArray): # If data is a DataArray, convert units directly
        return _convert_units_data_array(data, target_unit)
    elif isinstance(data, xr.Dataset): # If data is a Dataset, convert units for the specified variable
        return _convert_units_dataset(data, var, target_unit)
    else:
        raise ValueError("Input data must be either an xarray.DataArray or an xarray.Dataset.")

        
def _convert_units_data_array(data_array, target_unit):
    """
    Converts the units of an xarray.DataArray object to a target unit using MetPy.

    Args:
        data_array (xarray.DataArray): Input data array with units attribute.
        target_unit (str or pint.Unit): Target unit to convert to.

    Returns:
        xarray.DataArray: DataArray with units converted to the target unit.
    """
    current_unit = getattr(data_array, 'units', None)
    if current_unit is None: 
        raise ValueError("Input data array does not have units attribute.")
    
    # Update the 'units' attribute in the 'attrs' dictionary
    converted_data_array = data_array.metpy.convert_units(target_unit)
    converted_data_array.attrs['units'] = target_unit
    return converted_data_array


def _convert_units_dataset(dataset, var_name, target_unit):
    """
    Converts the units of a variable in an xarray.Dataset object to a target unit using MetPy.

    Args:
        dataset (xarray.Dataset): Input dataset with units attribute.
        var_name (str): Name of the variable whose units need to be converted.
        target_unit (str or pint.Unit): Target unit to convert to.

    Returns:
        xarray.Dataset: Dataset with units of the specified variable converted to the target unit.
    """
    # Check if the variable has units attribute
    current_unit = getattr(dataset[var_name], 'units', None) 
    if current_unit is None:
        raise ValueError(f"Variable '{var_name}' does not have units attribute.")

    var_data = dataset[var_name]
    converted_var_data = var_data.metpy.convert_units(target_unit)
    converted_var_data.attrs['units'] = target_unit
    dataset[var_name] = converted_var_data
    return dataset


def find_ranges(bool_array):
    """
    Find continuous ranges of True values in a boolean array.

    Parameters:
        bool_array (array-like): Boolean array.

    Returns:
        list of tuples: List of tuples representing continuous ranges of True values.
                        Each tuple contains the start and end index of the range.

    Example:
        >>> bool_array = [False, True, True, False, False, True, True, True, False]
        >>> find_ranges(bool_array)
        [(1, 2), (5, 7)]
    """
    ranges = list()
    start = None
    for i, value in enumerate(np.asarray(bool_array)):
        if value:
            if start is None: start = i
        elif start is not None:
            ranges.append((start, i - 1))
            start = None
    if start is not None:
        ranges.append((start, len(bool_array) - 1))
    return ranges


def rename_dimensions(dataset, dim_mapping):
    """
    Rename dimensions of variables in an xarray dataset according to the specified dimension mapping.

    Parameters:
    - dataset (xarray.Dataset): The input dataset.
    - dim_mapping (dict): A dictionary mapping source dimensions to target dimensions.

    Returns:
    - renamed_ds (xarray.Dataset): The dataset with dimensions renamed according to the mapping.
    """
    dataset_internal = dataset.copy() 
    for var in list(dataset_internal):
        for key in dim_mapping.keys():
            if key in dataset_internal[var].dims:
                target_dims = [dim_mapping[d] if d in dim_mapping.keys() else d for d in dataset_internal[var].dims]
                dataset_internal[var] = target_dims, dataset_internal[var].data
    return dataset_internal.drop_dims(dim_mapping.keys())


def mean_over_layer(data, dimension, lower_bound, upper_bound, new_variable_name='_mean'):
    """
    Calculate the mean of data over a specified dimension within a given range.

    Parameters:
        data (xarray.DataArray or xarray.Dataset): The input data array or dataset.
        dimension (str): The name of the dimension over which to calculate the mean.
        lower_bound (float): The lower bound of the range.
        upper_bound (float): The upper bound of the range.
        new_variable_name (str, optional): The suffix to append to the variable names after averaging.

    Returns:
        xarray.DataArray or xarray.Dataset: The mean of data over the specified dimension within the given range.
    """
    if isinstance(data, xr.DataArray): data = data.to_dataset()

    is_inside = ( (data[dimension] > lower_bound) &
                  (data[dimension] < upper_bound) ).compute()
    
    # Update long_name attribute
    dimension_long_name = data[dimension].attrs.get('long_name', dimension)
    for var_name in data.data_vars:
        data[var_name].attrs['long_name'] = f"{data[var_name].attrs.get('long_name', '')} mean over {dimension_long_name} from {lower_bound} to {upper_bound}"

    # Update variable name
    for var_name in data.data_vars:
        new_var_name = var_name + new_variable_name
        data = data.rename({var_name: new_var_name})
    
    return data.sel({dimension: is_inside}).mean(dim=dimension, keep_attrs=True)


def integrate_over_layer(data, dimension, lower_bound, upper_bound, new_variable_name='_integrated'):
    """
    Integrate the data over a specified dimension within a given range.

    Parameters:
        data (xarray.DataArray or xarray.Dataset): The input data array or dataset.
        dimension (str): The name of the dimension over which to calculate the integration.
        lower_bound (float): The lower bound of the integration range.
        upper_bound (float): The upper bound of the integration range.
        new_variable_name (str, optional): The suffix to append to the variable names after integration.

    Returns:
        xarray.DataArray or xarray.Dataset: The result of integrating the data over the specified dimension within the given range.
    """
    if isinstance(data, xr.DataArray): data = data.to_dataset()

    is_inside = ( (data[dimension] > lower_bound) &
                  (data[dimension] < upper_bound) ).compute()
    
    # Update long_name attribute
    dimension_long_name = data[dimension].attrs.get('long_name', dimension)
    for var_name in data.data_vars:
        data[var_name].attrs['long_name'] = f"{data[var_name].attrs.get('long_name', '')} {dimension_long_name} integrated from {lower_bound} to {upper_bound}"
    # Update variable name
    for var_name in data.data_vars:
        new_var_name = var_name + new_variable_name
        data = data.rename({var_name: new_var_name})
    
    return data.sel({dimension: is_inside}).integrate(coord=dimension)


def filter_by_threshold(dataset, threshold_dict, below_threshold=False, chunk_size=None):
    """
    Filter dataset based on variable-specific thresholds.

    This function filters the input dataset by applying a logical AND operation
    across all specified variables and their corresponding threshold values in
    the threshold_dict. For each variable, the function creates a mask where
    values are compared against the threshold. The final mask is the result of
    combining these individual masks using a logical AND operation.

    Parameters:
    - dataset (xarray.Dataset): Input dataset to be filtered.
    - threshold_dict (dict): Dictionary where keys are variable names and values are
      threshold values. The mask is created based on these thresholds.
    - below_threshold (bool): If True, values below the threshold are kept; if False,
      values above the threshold are kept. Default is False.
    - chunk_size (int or dict, optional): Chunk size to use for computation. If None,
      a heuristic is used to determine chunk size. Default is None.

    Returns:
    - filtered_dataset (xarray.Dataset): Filtered dataset with values not meeting
      the threshold conditions set to NaN. The mask is applied using a logical AND
      across all specified variables.
    """
    mask = xr.ones_like(dataset, dtype=bool)
    for var_name, threshold in threshold_dict.items():
        if below_threshold:
            mask = mask & (dataset[var_name] < threshold)
        else:
            mask = mask & (dataset[var_name] > threshold)
    mask = mask.chunk(chunk_size)
    return dataset.where(mask)

def filter_common_vars(datasets):
    """
    Filter datasets to include only common variables.

    This function takes a list of xarray datasets and filters them to include
    only the variables that are common to all datasets. It finds the intersection
    of variables across all datasets and returns new datasets containing only
    those common variables.

    Parameters:
    - datasets (list of xarray.Dataset): List of datasets to filter.

    Returns:
    - filtered_datasets (list of xarray.Dataset): List of filtered datasets
      containing only the common variables.
    """
    common_vars = set.intersection(*[set(ds.variables) for ds in datasets])
    return [ds[common_vars] for ds in datasets]


def add_threshold_indicator(dataset, threshold, new_variable_name=None, long_name=None, boolean=True, below_threshold=False):
    """
    Add a threshold indicator variable to the dataset.

    This function adds a new threshold indicator variable to the input dataset,
    indicating whether the values of a specified variable exceed a given threshold.
    The indicator variable can be either boolean (True/False) or a float (1/0).

    Parameters:
    - dataset: The dataset to which the threshold indicator will be added. It should be a data structure that supports item assignment and attribute setting.
    - threshold: A dictionary containing exactly one key-value pair, where the key is the variable name in the dataset to be compared, and the value is the threshold value.
    - new_variable_name: (Optional) The name of the new indicator variable to be added to the dataset. If not provided, it defaults to a string formatted as "{variable_name}>{threshold}".
    - long_name: (Optional) A descriptive name for the new indicator variable. If not provided, it defaults to the value of `new_variable_name`.
    - boolean: (Optional) A boolean flag indicating whether the indicator variable should be of boolean type. Defaults to True. If False, the indicator variable will be of type 'float32'.
    - below_threshold: (Optional) A boolean flag indicating whether to mark values below the threshold instead of above. Defaults to False.

    Returns:
    - The modified dataset with the new threshold indicator variable added.

    Raises:
    - TypeError: If the `threshold` is not a dictionary.
    - ValueError: If the `threshold` dictionary does not contain exactly one key-value pair.
    """
    if not isinstance(threshold, dict): raise TypeError("Threshold must be a dictionary with keys and values for filtering.")
    if len(threshold) != 1: raise ValueError("Threshold dictionary must contain exactly one key-value pair.")
    (variable_name, threshold_value), = threshold.items()
    if new_variable_name is None: new_variable_name = f"{variable_name}>{threshold_value}"
    if long_name is None: long_name = new_variable_name

    indicator_variable = dataset[variable_name] > threshold_value
    if not boolean: indicator_variable = indicator_variable.astype('float32')
    if below_threshold: indicator_variable = not indicator_variable

    indicator_variable.attrs['long_name'] = long_name
    indicator_variable.attrs['units'] = ''
    dataset[new_variable_name] = indicator_variable
    return dataset


def reduce_nondimensional(dataset):
    """
    deprecated
    
    use xarray.Dataset.squeeze() instead.
    """
    warnings.warn("This function has been removed, because it is redunandant to xarray.Dataset.squeeze()", DeprecationWarning)
    return dataset.squeeze()


def add_arithmetic_variable(dataset, var_name1, var_name2, operation_symbol, new_var_name=None):
    """
    Performs a specified arithmetic operation between two variables in a dataset and adds the result as a new variable.

    This function supports basic arithmetic operations (addition, subtraction, multiplication, division) between two variables. 
    It handles units using the MetPy library, ensuring that the operation is physically meaningful. The result of the operation 
    is added to the dataset as a new variable with appropriate metadata, including a long name and units derived from the operation.
    
    Parameters:
    - dataset (xarray.Dataset): The dataset containing the variables to be operated on.
    - var_name1 (str): The name of the first variable in the dataset.
    - var_name2 (str): The name of the second variable in the dataset.
    - operation_symbol (str): A string symbol representing the arithmetic operation to perform ('+', '-', '*', '/').
    - new_var_name (str, optional): The name for the new variable resulting from the operation. If None, defaults to a name derived from the operation (e.g., "var1 + var2").

    Raises:
    - ValueError: If either of the variable names are not found in the dataset.
    - ValueError: If the operation symbol is not supported.
    - ValueError: If the units of the variables do not match for addition or subtraction operations.

    Returns:
    - xarray.Dataset: The dataset with the new variable added.
    """
    if var_name1 not in dataset or var_name2 not in dataset:
        raise ValueError(f"Variables {var_name1} and/or {var_name2} not found in the dataset.")
    
    operations_map = {'+': operator.add, '-': operator.sub, '*': operator.mul, '/': operator.truediv}
    if operation_symbol not in operations_map:
        raise ValueError("Unsupported operation symbol. Choose from ['+', '-', '*', '/'].")

    if new_var_name is None:
        new_var_name = f"{var_name1} {operation_symbol} {var_name2}"
        
    
    var1 = dataset[var_name1]
    var2 = dataset[var_name2]
    if operation_symbol == '/': # prevent zero division
        var2 = var2.where(var2 != 0, np.nan)
    
    # var1_unit = metpy.units.units(var1.attrs['units'])
    # var2_unit = metpy.units.units(var2.attrs['units'])
    var1_unit = metpy.units.units(var1.attrs.get('units', None))
    var2_unit = metpy.units.units(var2.attrs.get('units', None))
    
    operation_func = operations_map[operation_symbol]
    
    # Handle units
    if operation_symbol in ['+', '-']:
        if var1_unit != var2_unit:
            raise ValueError(f"Units do not match for addition or subtraction ({{'{var_name1}':'{var1_unit}', '{var_name2}':'{var2_unit}']}}.")
        result_unit = var1_unit
    elif operation_symbol == '*':
        result_unit = var1_unit * var2_unit
    elif operation_symbol == '/':
        result_unit = var1_unit / var2_unit
    
    # Perform the operation
    result_data = operation_func(var1, var2)
    
    dataset[new_var_name] = result_data
    original_attrs = var1.attrs.copy()
    original_attrs['long_name'] = f"{new_var_name} ({var_name1} {operation_symbol} {var_name2})"
    original_attrs['units'] = f"{result_unit.units}"
    dataset[new_var_name].attrs = original_attrs
    
    return dataset


def rename_variables(ds: xr.Dataset, var_prefix: str, long_name_prefix: str) -> xr.Dataset:
    """
    Rename variables and their long_name attribute in an xarray.Dataset.

    Parameters:
    - ds: The xarray.Dataset whose variables and long_name attributes need to be renamed.
    - var_prefix: The prefix to add to the variable names.
    - long_name_prefix: The prefix to add to the long_name attribute of the variables.

    Returns:
    - A new xarray.Dataset with the renamed variables and updated long_name attributes.
    """
    ds_renamed = ds.copy()
    for var in ds.variables:
        if var not in ds.coords:
            ds_renamed[var].attrs['long_name'] = f"{long_name_prefix}{ds[var].attrs['long_name']}"
            ds_renamed = ds_renamed.rename({var: f"{var_prefix}{var}"})
    return ds_renamed


def remove_duplicates(data, dim='time'):
    """
    Removes duplicate entries along a specified dimension of an xarray.Dataset
    or xarray.DataArray, keeping only the first occurrence of any duplicates.

    Parameters:
    - data (xarray.Dataset or xarray.DataArray): The input data.
    - dim (str, optional): The dimension along which to check for and remove duplicates. Defaults to 'time'.

    Returns:
    - xarray.Dataset or xarray.DataArray: The input data with duplicates removed along the specified dimension.
    """
    if dim not in data.dims: raise ValueError(f"Dimension '{dim}' not found in the input data.")

    _, index = np.unique(data[dim], return_index=True)
    data = data.isel({dim: index})
    data.sortby('time')
    return data

def subsample_dataset(dataset, subsampling_dict={'cell':16}):
    """
    Alias for regular_subsample
    """
    return regular_subsample(dataset, subsampling_dict)
    
def regular_subsample(dataset, subsampling_dict={'cell':16}):
    """
    Subsample an xarray.Dataset along specified dimensions.

    Args:
    - dataset (xarray.Dataset): The original dataset to be subsampled.
    - subsampling_dict (dict): A dictionary describing the subsampling along dimensions.
                               Keys are dimension names, and values are integers representing
                               the step size for subsampling along that dimension.

    Returns:
    - subsampled_dataset (xarray.Dataset): The subsampled dataset. defaults to {'cell':16}

    Example:
    If subsampling_dict = {'time': 5}, every 5th data point will be taken along the time dimension,
    discarding the rest. Other dimensions will remain unchanged.
    
    Note:
    Loosely based on https://easy.gems.dkrz.de/Processing/playing_with_triangles/subsampling_and_averaging.html
    """
    subsampled_dataset = dataset.copy()

    for dim, step in subsampling_dict.items():
        if dim in dataset.dims:
            indices = slice(None, None, step)
            subsampled_dataset = subsampled_dataset.isel({dim: indices})

    return subsampled_dataset

def random_subsample(dataset, dim_samples):
    """
    Takes a random subsample along specified dimensions in an xarray.Dataset.

    Parameters:
    - dataset (xarray.Dataset): The input dataset.
    - dim_samples (dict): A dictionary where keys are dimension names and values are the number of random samples to select.

    Returns:
    - xarray.Dataset: A new dataset containing only the randomly selected subsamples along the specified dimensions.
    """
    for dim, n_samples in dim_samples.items():
        if n_samples > dataset[dim].size:
            raise ValueError(f"n_samples ({n_samples}) for '{dim}' is greater than the size of the dimension ({dataset[dim].size}).")
        random_selection = np.random.choice(dataset[dim], size=n_samples, replace=False)
        dataset = dataset.sel({dim: random_selection})
    return dataset

def timestamp(date_time=None):
    """
    Returns the date in YYYYMMDD format for the given datetime object.
    If no datetime object is provided, it defaults to the current date.

    Args:
        date_time (datetime, optional): The datetime object to format. Defaults to None.

    Returns:
        str: The date formatted as YYYYMMDD.
    """
    if date_time is None:
        date_time = datetime.datetime.now()
    return date_time.strftime('%Y%m%d')
    
def _create_lat_lon_grid(grid, nx=None, ny=None, resolution=None, varnames=None):
    """
    Creates a latitude and longitude grid based on the provided grid and resolution.

    Args:
        grid (xarray.Dataset): The input dataset containing latitude and longitude variables.
        nx (int, optional): Number of points in the x-direction. Defaults to None.
        ny (int, optional): Number of points in the y-direction. Defaults to None.
        resolution (float, optional): Resolution given as a fraction of a degree. Defaults to None.
        varnames (dict, optional): Dictionary mapping variable names in the dataset. Defaults to package defaults, see _add_varnames_to_default

    Returns:
        tuple: A tuple containing the longitude and latitude grids.
    """
    varnames = _add_varnames_to_default(varnames)
    lon_min, lon_max = np.min(grid[varnames['longitude']].values), np.max(grid[varnames['longitude']].values)
    lat_min, lat_max = np.min(grid[varnames['latitude']].values), np.max(grid[varnames['latitude']].values)
    
    if resolution is not None:
        nx = int((lon_max - lon_min) / resolution)
        ny = int((lat_max - lat_min) / resolution)

    lon, lat = np.meshgrid(
        np.linspace(lon_min, lon_max, nx), 
        np.linspace(lat_min, lat_max, ny)
    )
    return lon, lat
    
def _compute_remap_weights(grid, lon, lat, weights_file=None, varnames=None):
    """
    Computes remapping weights for the given grid and saves them to a file.

    Args:
        grid (xarray.Dataset): The input dataset containing latitude and longitude variables.
        lon (numpy.ndarray): The longitude grid.
        lat (numpy.ndarray): The latitude grid.
        weights_file (str, optional): Path to the file where weights will be saved. Defaults to None.
        varnames (dict, optional): Dictionary mapping variable names in the dataset. Defaults see _add_varnames_to_default.

    Returns:
        xarray.Dataset: The dataset containing the remapping weights.
    """
    varnames = _add_varnames_to_default(varnames)
    if weights_file ==  None:
        weights_file = f"/scratch/m/m300872/weight_files/weights_{lon.shape[0]}x{lon.shape[1]}_{timestamp()}.nc"
    
    if not os.path.exists(weights_file):
        print(f'calculating weights, saving to {os.path.basename(weights_file)}')
        weights = easygems.remap.compute_weights_delaunay((grid.lon, grid.lat), (lon.ravel(), lat.ravel()))
        weights.to_netcdf(weights_file, mode="w")
    return xr.open_dataset(weights_file)

def _remap_delaunay(ds, weights, lon, lat, varnames=None):
    """
    Remaps the dataset using Delaunay Triangulation.

    Args:
        ds (xarray.Dataset): The input dataset to be remapped.
        weights (xarray.Dataset): The dataset containing remapping weights.
        lon (numpy.ndarray): The longitude grid.
        lat (numpy.ndarray): The latitude grid.
        varnames (dict, optional): Dictionary mapping variable name in the dataset. Defaults to _add_varnames_to_default.

    Returns:
        xarray.Dataset: The remapped dataset.
    """
    varnames = _add_varnames_to_default(varnames)
    ds_remap = xr.apply_ufunc(
        easygems.remap.apply_weights,
        ds,
        kwargs=weights,
        input_core_dims=[[varnames["cell"]]],
        output_core_dims=[["xy"]],
        output_dtypes=["f4"],
        vectorize=True,
        dask="parallelized",
        dask_gufunc_kwargs={
            "output_sizes": {"xy": lon.size},
        },
    ).assign(
        xy=pd.MultiIndex.from_product(
            (np.degrees(lat[:, 0]), np.degrees(lon[0, :])), 
            names=("lat", "lon"),
        ),
    ).unstack("xy")
    return ds_remap

def regrid_latlon(dataset, resolution=0.5, weights_file=None, varnames=None):
    """
    Regrids the dataset to a new latitude-longitude grid with the specified resolution in degree. 
    
    Makes use of the easygems package.

    Args:
        ds (xarray.Dataset): The input dataset to be regridded.
        resolution (float, optional): Resolution given as a fraction of a degree. Defaults to 0.5.
        varnames (dict, optional): Dictionary mapping variable name in the dataset. Defaults to _add_varnames_to_default.

    Returns:
        xarray.Dataset: The regridded dataset.
    """
    varnames = _add_varnames_to_default(varnames)
    if weights_file == None: 
        warnings.warn("No path given for `weights_file`. The default is system specific (to dkrz.levante).", UserWarning)
    lon, lat = _create_lat_lon_grid(dataset, resolution=resolution, varnames=varnames)
    weights = _compute_remap_weights(dataset, lon, lat, weights_file=weights_file, varnames=varnames)
    dataset = _remap_delaunay(dataset, weights, lon, lat, varnames=varnames)
    return dataset


def remove_dimension(dataset: xr.Dataset, dim_name: str) -> xr.Dataset:
    """
    Remove all variables along a specified dimension from an xarray.Dataset and also remove the dimension itself.

    Parameters:
    ds (xarray.Dataset): The input xarray.Dataset from which variables and the dimension will be removed.
    dim_name (str): The name of the dimension to be removed along with all variables associated with it.

    Returns:
    xarray.Dataset: A new xarray.Dataset with the specified dimension and its associated variables removed.
    """
    vars_to_remove = [var for var in dataset.data_vars if dim_name in dataset[var].dims]
    dataset = dataset.drop_vars(vars_to_remove)
    dataset = dataset.drop_dims(dim_name)
    return dataset


# def split_and_mean_by_threshold(dataset, threshold, dim='cell', equal_in_above=True):
#     """
#     Splits the dataset based on specified thresholds for each variable and computes the mean for each partition.

#     Parameters:
#     - dataset: The input data array or dataset containing the variables to be split.
#     - threshold: A dictionary where keys are variable names and values are the threshold values for splitting.
#     - dim: The dimension along which to compute the mean. Default is 'cell'.
#     - equal_in_above: A boolean indicating whether the threshold is inclusive in the upper partition. Default is True.

#     Returns:
#     - An xarray object containing the concatenated means of the partitions for each variable.
#     """
#     if len(threshold) > 1: raise ValueError(f"Processing more than one threshold not implemented.")
#     for var, threshold in conditions.items():
#         if var not in dataset:
#             raise ValueError(f"Variable '{var}' not found in the dataset.")
#         if not isinstance(threshold, (int, float)):
#             raise ValueError(f"Threshold for '{var}' must be a number.")
    
#     mean_datasets = []
#     for var, threshold in conditions.items():
#         dimname = f"{var}{'>=' if equal_in_above else '>'}{threshold}"
#         if equal_in_above:
#             below_indexer = (dataset[var] < threshold).compute()
#             above_indexer = (dataset[var] >= threshold).compute()
#         else:
#             below_indexer = (dataset[var] <= threshold).compute()
#             above_indexer = (dataset[var] > threshold).compute()

#         below_threshold = dataset.where(below_indexer, drop=True)
#         above_threshold = dataset.where(above_indexer, drop=True)
#         mean_below = below_threshold.mean(dim=dim, keep_attrs=True)
#         mean_above = above_threshold.mean(dim=dim, keep_attrs=True)
#         mean_below[var] = f"<{'=' if not equal_in_above else ''} {threshold}"
#         mean_above[var] = f">{'=' if equal_in_above else ''} {threshold}"

#         mean_below = mean_below.expand_dims({dimname:(0,)})
#         mean_above = mean_above.expand_dims({dimname:(1,)})
#         mean_combined = xr.concat([mean_below, mean_above], dim=dimname)
#         mean_datasets.append(mean_combined)

#     return xr.concat(mean_datasets, dim=dimname)

def split_along_threshold(dataset, threshold, dimname=None):
    """
    Splits a dataset into three parts based on a specified threshold and concatenates them along a new dimension.

    Parameters:
    - dataset: The dataset to be split. It must support the `expand_dims` and `dims` methods.
    - threshold (dict): A dictionary containing exactly one key-value pair used for filtering. The key represents the dimension or variable, and the value is the threshold value.
    - dimname (str, optional): The name of the new dimension along which the datasets will be concatenated. If not provided, it defaults to a string formatted as "key>value" from the threshold dictionary.

    Returns:
    - An xarray dataset with three parts concatenated along the specified dimension:
      1. The original dataset with expanded dimensions.
      2. The dataset filtered to include values below the threshold.
      3. The dataset filtered to include values above the threshold.

    Raises:
    - TypeError: If `threshold` is not a dictionary.
    - ValueError: If `threshold` does not contain exactly one key-value pair.

    Example:
    >>> split_along_threshold(my_dataset, {'temperature': 300})
    """
    if not isinstance(threshold, dict): raise TypeError("Threshold must be a dictionary with keys and values for filtering.")
    if len(threshold) != 1: raise ValueError("Threshold dictionary must contain exactly one key-value pair.")
    if dimname == None: dimname = [f"{key}>{value}" for key, value in threshold.items()][0]
        
    unfiltered = dataset.expand_dims({dimname:(None,)})
    (var, threshold_value), = threshold.items()
    below_indexer = (dataset[var] < threshold_value).compute()
    above_indexer = (dataset[var] >= threshold_value).compute()
    below_threshold = dataset.where(below_indexer)
    above_threshold = dataset.where(above_indexer)
    below_threshold = below_threshold.expand_dims({dimname:(False,)})
    above_threshold = above_threshold.expand_dims({dimname:(True, )})
    
    dataset = xr.concat([unfiltered, below_threshold, above_threshold], dim=dimname)
    return dataset
    

def cloud_scenario_cell_average(dataset, threshold={'clct': 50}, dim='cell'):
    """
    Calculate the average of a dataset over specified cloud scenarios based on a threshold.

    This function computes the mean values of a given dataset along a specified dimension,
    categorizing the data into three cloud scenarios: 'clear_sky', 'cloudy', and 'all_sky'.
    The categorization is based on whether the values of a specified variable in the dataset
    are below or above a given threshold.

    Parameters:
    - dataset (xarray.Dataset): The input dataset containing the variable to be analyzed.
    - threshold (dict, optional): A dictionary specifying the variable and its threshold value
      for categorization. Default is {'clct': 50}.
    - dim (str, optional): The dimension along which to compute the mean. Default is 'cell'.

    Returns:
    - xarray.Dataset: A dataset containing the mean values for each cloud scenario ('clear_sky',
      'cloudy', 'all_sky') along the specified dimension.

    Raises:
    - ValueError: If more than one threshold is provided, if the specified variable is not found
      in the dataset, or if the threshold value is not a number.
    """
    if len(threshold) > 1: raise ValueError(f"Processing more than one threshold not implemented.")

    dimname = 'cloud_scenario'
    mean_datasets = []
    (var, threshold_value), = threshold.items()
    if var not in dataset: raise ValueError(f"Variable '{var}' not found in the dataset.")
    if not isinstance(threshold_value, (int, float)): raise ValueError(f"Threshold for '{var}' must be a number.")

    below_indexer = (dataset[var] < threshold_value).compute()
    above_indexer = (dataset[var] >= threshold_value).compute()
    below_threshold = dataset.where(below_indexer, drop=True)
    above_threshold = dataset.where(above_indexer, drop=True)

    mean_below = below_threshold.mean(dim=dim, keep_attrs=True)
    mean_above = above_threshold.mean(dim=dim, keep_attrs=True)
    mean_overall = dataset.mean(dim=dim, keep_attrs=True)
    
    mean_below = mean_below.expand_dims({dimname:('clear_sky',)})
    mean_above = mean_above.expand_dims({dimname:('cloudy',)})
    mean_overall = mean_overall.expand_dims({dimname:('all_sky',)})
    mean_combined = xr.concat([mean_below, mean_above, mean_overall], dim=dimname)
    return mean_combined
    

def shift_time(dataset: xr.Dataset, offset: int = 0, time_var: str = 'time') -> xr.Dataset:
    """
    Shift the time variable in an xarray.Dataset from UTC to local time using a specified UTC offset.

    Parameters:
    - dataset (xarray.Dataset): The dataset containing the time variable to be shifted.
    - offset (int, optional): The time difference from UTC in hours. Default is 0.
    - time_var (str, optional): The name of the time variable within the dataset. Default is 'time'.

    Returns:
    - xarray.Dataset: A new dataset with the time variable shifted by the specified UTC offset.
    """
    if time_var not in dataset: raise ValueError(f"The specified time variable '{time_var}' is not present in the dataset.")

    shifted_time = pd.to_datetime(dataset[time_var].values) + pd.Timedelta(hours=offset)
    new_dataset = dataset.copy()
    new_dataset[time_var] = ('time', shifted_time)
    return new_dataset

def local_time(dataset):
    """
    Specific for time shift -4 (UTC to Barbados local time)
    """
    return shift_time(dataset, -4)



def daily_average(ds: xr.Dataset, time_dim: str = 'time') -> xr.Dataset:
    """
    Calculate daily averaged values for each variable in the given xarray.Dataset and add a quality control variable.

    This function computes the daily mean for each variable in the dataset and adds a new variable named 
    'samples_per_day' that indicates the number of data points included in each daily average. This is useful 
    for quality control to identify if the daily mean is out of the expected range due to missing data.

    Parameters:
    ds (xarray.Dataset): The input dataset containing time-indexed data.
    time_dim (str): The name of the time dimension in the dataset. Defaults to 'time'.

    Returns:
    xarray.Dataset: A new dataset with daily averaged values and an additional variable 'samples_per_day' 
    indicating the number of data points used in each daily average.
    """
    if time_dim not in ds.dims: raise ValueError(f"The dataset must have a '{time_dim}' dimension.")
    attributes = {'short_name': 'samples_per_day', 'long_name': 'Number of Samples Per Day', 'units': 'unitless'}
    
    daily_mean = ds.resample({time_dim: '1D'}).mean()
    samples_per_day = ds.resample({time_dim: '1D'}).count()
    samples_per_day = samples_per_day.to_array().isel(variable=0) # assuming all variables have the same count

    daily_mean['samples_per_day'] = samples_per_day
    daily_mean['samples_per_day'].attrs = attributes
    return daily_mean

def count_nans_along_dims(data_array, dim, relative=False):
    """
    Count the number of NaN values along specified dimensions in an xarray.DataArray.

    Parameters:
    - data_array (xarray.DataArray): The input data array containing the data.
    - dim (list of str): A list of dimension names along which to count NaN values.
    - relative (bool): If True, return the relative abundance of NaN values instead of the absolute count. Default is False.

    Returns:
    - xarray.DataArray: A new DataArray with the specified dimensions reduced, containing either the count or the relative abundance of NaN values along those dimensions.

    Notes:
    - The `isnull()` method is used to identify NaN (Not a Number) values in the DataArray. It considers both `np.nan` and `None` as NaN values.
    """
    nan_count = data_array.isnull().sum(dim=dim)
    if relative:
        total_count = data_array.count(dim=dim) + nan_count
        return nan_count / total_count
    return nan_count


# def split_and_mean_by_threshold(dataset, threshold, dim='cell', equal_in_above=True):
#     """
#     Splits the dataset based on specified thresholds for each variable and computes the mean for each partition.

#     Parameters:
#     - dataset: The input data array or dataset containing the variables to be split.
#     - threshold: A dictionary where key is the variable name and value is the threshold for splitting.
#     - dim: The dimension along which to compute the mean. Default is 'cell'.
#     - equal_in_above: A boolean indicating whether the threshold is inclusive in the upper partition. Default is True.

#     Returns:
#     - An xarray object containing the concatenated means of the partitions for each variable.
#     """
#     if len(threshold) > 1: raise ValueError(f"Processing more than one threshold not implemented.")
    
    
#     mean_datasets = []
#     for var, threshold_value in threshold.items():
#         if var not in dataset: raise ValueError(f"Variable '{var}' not found in the dataset.")
#         if not isinstance(threshold_value, (int, float)): raise ValueError(f"Threshold for '{var}' must be a number.")
#         dimname = f'{var}{">=" if equal_in_above else ">"}{threshold_value}'
        
#         if equal_in_above:
#             below_indexer = (dataset[var] < threshold_value).compute()
#             above_indexer = (dataset[var] >= threshold_value).compute()
#         else:
#             below_indexer = (dataset[var] <= threshold_value).compute()
#             above_indexer = (dataset[var] > threshold_value).compute()

#         below_threshold = dataset.where(below_indexer, drop=True)
#         above_threshold = dataset.where(above_indexer, drop=True)

#         if below_threshold[var].size > 0:
#             mean_below = below_threshold.mean(dim=dim, keep_attrs=True)
#         else:
#             mean_below = xr.full_like(dataset.isel({dim: 0}), fill_value=np.nan)

#         if above_threshold[var].size > 0:
#             mean_above = above_threshold.mean(dim=dim, keep_attrs=True)
#         else:
#             mean_above = xr.full_like(dataset.isel({dim: 0}), fill_value=np.nan)

#         mean_below = mean_below.expand_dims({dimname:(0,)})
#         mean_above = mean_above.expand_dims({dimname:(0,)})
#         mean_combined = xr.concat([mean_below, mean_above], dim=dimname)
#         mean_datasets.append(mean_combined)

#     return xr.concat(mean_datasets, dim=dimname)

