# warm_EUREC4A

path toward the pseudo global warming Eurec4a LES setup.

## Environment 

Install via: `conda env create -f environment.yml`. This environment does not include `konrad`, because konrad can not be installed on macOS. So you might have to add it for specific notebooks.
